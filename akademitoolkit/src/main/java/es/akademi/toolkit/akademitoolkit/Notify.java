package es.akademi.toolkit.akademitoolkit;
import java.awt.AWTException;
import java.awt.Image;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.TrayIcon.MessageType;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
/*
import java.io.InputStream;
import com.github.plushaze.traynotification.animations.Animations;
import com.github.plushaze.traynotification.notification.Notifications;
import com.github.plushaze.traynotification.notification.TrayNotification;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.image.Image;
import javafx.scene.paint.Paint;
import javafx.util.Duration;
 */
public class Notify {
	public Notify() {
	}

	public void createNotify(String title, String msg) {
		Thread t = new Thread (new Runnable() {

			@Override
			public void run() {
				SystemTray tray = SystemTray.getSystemTray();

				//If the icon is a file
				//java.awt.Image image = Toolkit.getDefaultToolkit().createImage("icon.png");
				//Alternative (if the icon is on the classpath):
				BufferedImage trayIconImage = null;
				try {
					trayIconImage = ImageIO.read(getClass().getResource("/images/PanteraLogo2.png"));
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				int trayIconWidth = new TrayIcon(trayIconImage).getSize().width;
				TrayIcon trayIcon = new TrayIcon(trayIconImage.getScaledInstance(trayIconWidth, 100, Image.SCALE_SMOOTH));

				//Let the system resize the image if needed
				trayIcon.setImageAutoSize(true);
				//Set tooltip text for the tray icon
				trayIcon.setToolTip(msg);
				try {
					tray.add(trayIcon);
				} catch (AWTException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				trayIcon.displayMessage(title, msg, MessageType.NONE);
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				tray.remove(trayIcon);
			}
		});
		t.start();

	}

	public void createErrorNotify(String title, String msg) {
		Thread t = new Thread (new Runnable() {

			@Override
			public void run() {
				SystemTray tray = SystemTray.getSystemTray();

				//If the icon is a file
				//java.awt.Image image = Toolkit.getDefaultToolkit().createImage("icon.png");
				//Alternative (if the icon is on the classpath):
				BufferedImage trayIconImage = null;
				try {
					trayIconImage = ImageIO.read(getClass().getResource("/images/PanteraLogo2.png"));
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				int trayIconWidth = new TrayIcon(trayIconImage).getSize().width;
				TrayIcon trayIcon = new TrayIcon(trayIconImage.getScaledInstance(trayIconWidth, 100, Image.SCALE_SMOOTH));

				//Let the system resize the image if needed
				trayIcon.setImageAutoSize(true);
				//Set tooltip text for the tray icon
				trayIcon.setToolTip(msg);
				try {
					tray.add(trayIcon);
				} catch (AWTException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				trayIcon.displayMessage(title, msg, MessageType.NONE);
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				tray.remove(trayIcon);
			}
		});
		t.start();

	}

	public void createWarningNotify(String title, String msg) {
		Thread t = new Thread (new Runnable() {

			@Override
			public void run() {
				SystemTray tray = SystemTray.getSystemTray();

				//If the icon is a file
				//java.awt.Image image = Toolkit.getDefaultToolkit().createImage("icon.png");
				//Alternative (if the icon is on the classpath):
				BufferedImage trayIconImage = null;
				try {
					trayIconImage = ImageIO.read(getClass().getResource("/images/PanteraLogo2.png"));
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				int trayIconWidth = new TrayIcon(trayIconImage).getSize().width;
				TrayIcon trayIcon = new TrayIcon(trayIconImage.getScaledInstance(trayIconWidth, 100, Image.SCALE_SMOOTH));

				//Let the system resize the image if needed
				trayIcon.setImageAutoSize(true);
				//Set tooltip text for the tray icon
				trayIcon.setToolTip(msg);
				try {
					tray.add(trayIcon);
				} catch (AWTException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				trayIcon.displayMessage(title, msg, MessageType.NONE);
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				tray.remove(trayIcon);
			}
		});
		t.start();
	}
}
