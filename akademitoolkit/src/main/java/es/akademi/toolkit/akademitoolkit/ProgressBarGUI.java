package es.akademi.toolkit.akademitoolkit;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.LayoutStyle.ComponentPlacement;

public class ProgressBarGUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	JLabel label;
	JProgressBar progressBar;

	/**
	 * Create the frame.
	 */
	public ProgressBarGUI(String name) {
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setSize(300, 100);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setName(name);
		setTitle(name);
		setLocationRelativeTo(null);
		label = new JLabel("");
		label.setFont(new Font("Arial Black", Font.PLAIN, 21));
		label.setHorizontalAlignment(SwingConstants.CENTER);

		progressBar = new JProgressBar();
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
				gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addComponent(label, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 1034, Short.MAX_VALUE)
								.addComponent(progressBar, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 1034, Short.MAX_VALUE))
						.addContainerGap())
				);
		gl_contentPane.setVerticalGroup(
				gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
						.addContainerGap(20, Short.MAX_VALUE)
						.addComponent(label, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(progressBar, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
				);
		contentPane.setLayout(gl_contentPane);
		progressBar.setStringPainted(true);
		pack();
		setLocationByPlatform(true);
		setVisible(true);
	}
	
	public void changeTitle(String name) {
		this.setTitle(name);
		this.setName(name);
	}
}

