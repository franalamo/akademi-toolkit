package es.akademi.toolkit.akademitoolkit;
import java.awt.Component;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.akademi.toolkit.akademitoolkit.Parallel.Operation;
import uk.org.lidalia.sysoutslf4j.context.SysOutOverSLF4J;

import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;

import java.awt.Toolkit;
import java.awt.TrayIcon;

import javax.swing.JScrollPane;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;

import javax.swing.JPasswordField;
import javax.swing.JCheckBox;
import java.awt.Font;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;

import javax.swing.JComboBox;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import java.awt.AWTException;
import java.awt.Color;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.JTextPane;
import javax.swing.JProgressBar;
import java.awt.SystemColor;

public class Interfaz{
	private Interfaz interfaz = this;
	static boolean instalandoMods = false;
	static boolean descargandoMods = false;
	static JFrame frmArmaAk;
	private static JTextField steamCMDTF;
	static JTextField txtSteamcmdStatus;
	static JTextField serverName;
	static JTextField serverDir;
	private static Properties prop = new Properties();
	private static InputStream input = null;
	private static OutputStream output = null;	
	//LISTA SERVIDOR
	static DefaultListModel<JCheckBox> modelServidor = new DefaultListModel<JCheckBox>();
	private static JCheckBoxListSingle listServidor = new JCheckBoxListSingle(modelServidor);
	private JScrollPane scrollServidor = new JScrollPane(listServidor);
	//LISTA MOD
	static DefaultListModel<JCheckBox> modelMod = new DefaultListModel<JCheckBox>();
	private static JCheckBoxListSingle listMod = new JCheckBoxListSingle(modelMod);
	private JScrollPane scrollMod = new JScrollPane(listMod);

	//LISTA MISIONES
	static DefaultListModel<JCheckBox> modelMision = new DefaultListModel<JCheckBox>();
	private static JCheckBoxListSingle listMision = new JCheckBoxListSingle(modelMision);
	private JScrollPane scrollMision = new JScrollPane(listMision);

	//PERFIL
	//LISTA MODS
	static DefaultListModel<JCheckBox> modelModPerfil = new DefaultListModel<JCheckBox>();
	JCheckBoxListMultiple listModsPerfil = new JCheckBoxListMultiple(modelModPerfil);
	//LISTA MISIONES
	static DefaultListModel<JCheckBox> modelMisionPerfil = new DefaultListModel<JCheckBox>();
	JCheckBoxListSingle listMisionesPerfil = new JCheckBoxListSingle(modelMisionPerfil);

	//PERFILES
	static DefaultListModel<JCheckBox> modelPerfiles = new DefaultListModel<JCheckBox>();
	JCheckBoxListSingle listPerfiles = new JCheckBoxListSingle(modelPerfiles);
	//VARIABLES PUBLICAS / SERVIDOR
	JPanel pnlServidores;
	static JButton btnAgregarServidor;
	static JButton btnDescargarServidor;
	JCheckBox chckbxAutoinit;
	JCheckBox chckbxHugepages;
	JCheckBox chckbxNologs;
	JCheckBox chckbxNewEnableHT;
	JCheckBox chckbxFilepatching;
	JCheckBox chckbxNocb;
	static JButton btnAgregarMod;
	static JButton btnDescargarMod;
	static JProgressBar downloadPB;
	static JLabel downloadLabel;

	//Variables de aplicacion
	static String steamCMD = "";
	static String user = "";
	static String password = "";
	static String securityKey = "";
	static String version = "0.0.1";
	static String deviceId = "";
	static String deviceToken = "";
	static String otarestart = "false";
	static String token = "";
	static ArrayList<Servidor> servidores = new ArrayList<Servidor>() {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public boolean add(Servidor mt) {
			super.add(mt);
			Collections.sort(servidores, new Comparator<Servidor>() {

				@Override
				public int compare(Servidor o1, Servidor o2) {
					return o1.getNombreServidor().toLowerCase().compareTo(o2.getNombreServidor().toLowerCase());
				}
			});
			return true;
		}
	};
	static ArrayList<Mod> mods = new ArrayList<Mod>(){
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public boolean add(Mod mt) {
			super.add(mt);
			Collections.sort(mods, new Comparator<Mod>() {

				@Override
				public int compare(Mod o1, Mod o2) {
					return o1.getNombreMod().toLowerCase().compareTo(o2.getNombreMod().toLowerCase());
				}
			});
			return true;
		}
	};
	static ArrayList<Mision> misiones = new ArrayList<Mision>(){
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public boolean add(Mision mt) {
			super.add(mt);
			Collections.sort(misiones, new Comparator<Mision>() {

				@Override
				public int compare(Mision o1, Mision o2) {
					return o1.getNombreMision().toLowerCase().compareTo(o2.getNombreMision().toLowerCase());
				}
			});
			return true;
		}
	};
	private static ArrayList<Perfil> perfiles = new ArrayList<Perfil>(){
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public boolean add(Perfil mt) {
			super.add(mt);
			Collections.sort(perfiles, new Comparator<Perfil>() {

				@Override
				public int compare(Perfil o1, Perfil o2) {
					return o1.getNombrePerfil().toLowerCase().compareTo(o2.getNombrePerfil().toLowerCase());
				}
			});
			return true;
		}
	};
	static JTextField inputNombreMod;
	static JTextField inputDirMod;
	private JPasswordField serverPassword;
	private JTextField serverPublicName;
	static JTextField dirKeyMod;
	private static JTextField inputUser;
	private static JPasswordField inputPassword;
	private static JPasswordField inputKey;
	static LogGUI logGUI;
	private JTextField serverPuerto;
	static JTextField inputIdMod;
	private JTextField inputDirMision;
	private JTextField inputNombreMision;
	private JTextField inputPerfilName;
	private JTextField inputPackMods;
	private JTextField inputPackMisiones;
	static JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
	TrayIcon trayIcon;
	SystemTray tray;
	static Notify notify = new Notify();
	static String osName = System.getProperty("os.name");
	static boolean deviceEncontrado = false;
	Logger logger = LoggerFactory.getLogger(Interfaz.class);
	static String instalacionCompletada = "false";
	static int allFileNumber; 
	static int progressFile = 0;
	static String defaultFolderMods = "";

	//Exceptions
	@SuppressWarnings("serial")
	public static class ErrorDirectorioSteamCMD extends Exception{}
	@SuppressWarnings("serial")
	public static class ErrorDirectorio extends Exception{}
	@SuppressWarnings("serial")
	public static class ErrorNombre extends Exception{}
	@SuppressWarnings("serial")
	public static class ErrorPuerto extends Exception{}



	/**
	 * Launch the application.
	 * @throws IOException 
	 */
	public static void main(String[] args) {
		SysOutOverSLF4J.sendSystemOutAndErrToSLF4J();
		try {
			input = new FileInputStream("config.properties");
		} catch (FileNotFoundException e1) {
			new File("config.properties");
		}
		if (input != null) {
			try {
				prop.load(input);
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (prop.getProperty("deviceId") != null) {
				if (!prop.getProperty("deviceId").equals("")) {
					deviceId = prop.getProperty("deviceId");
					deviceEncontrado = true;
				}
			}
			if (prop.getProperty("deviceToken") != null) {
				if (!prop.getProperty("deviceToken").equals("")) {
					deviceToken = prop.getProperty("deviceToken");
					deviceEncontrado = true;
				}
			}

			if (prop.getProperty("otaupdate") != null) {
				if (!prop.getProperty("otaupdate").equals("")) {
					otarestart = prop.getProperty("otaupdate");
				}
			}

			if (prop.getProperty("version") != null) {
				if (!prop.getProperty("version").equals("")) {
					version = prop.getProperty("version");
				}
			}

			if (prop.getProperty("token") != null) {
				if (!prop.getProperty("token").equals("")) {
					token = prop.getProperty("token");
				}
			}
			if (prop.getProperty("instalacion") != null) {
				if (!prop.getProperty("instalacion").equals("")) {
					instalacionCompletada= prop.getProperty("instalacion");
				}
			}

			try {
				input.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (instalacionCompletada.equals("false")) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					instalacion();
				}
			}).start();
		}
		while(!instalacionCompletada.equals("true")) {
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		//Borramos carpeta de firmwarefolder en caso de haberse actualizado
		File firmwarefolder = new File(System.getProperty("user.dir")+File.separator+"firmwarefolder");
		if (firmwarefolder.exists()) {
			if (firmwarefolder.isDirectory()) {
				try {
					FileUtils.deleteDirectory(firmwarefolder);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		/**/
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
						| UnsupportedLookAndFeelException e3) {
					e3.printStackTrace();
				}
				try {
					new Interfaz();
					logGUI = new LogGUI();
					LogGUI.frmLog.setVisible(true);
					Interfaz.frmArmaAk.setVisible(true);		
					System.out.println("BIENVENIDO A TOOLKIT AK. ESTA ES UNA APLICACION CREADA\nPARA AYUDAR A ADMINISTRAR LOS SERVIDORES.\n"
							+ "SI TIENES ALGUNA PREGUNTA SOBRE ESTA APLICACION:\n"
							+ "AKADEMI.ES\n"
							+ "TODOS LOS DATOS SOLO SE ALMACENAN EN UN FICHERO\n"
							+ "LLAMADO 'CONFIG.PROPERTIES'\n");
					ThreadSteamCMDMod mr = new ThreadSteamCMDMod(mods);
					Thread t = new Thread(mr);
					t.start();
					Thread tsave= new Thread(new Runnable() {

						@Override
						public void run() {
							while (true) {
								try {
									Thread.sleep(1*60*1000);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								saveInfoToProperties();
								try {
									Thread.sleep(5*60*1000);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}

						}
					});
					tsave.start();
				} catch (Exception e) {
					mensajeError("","A OCURRIDO UN ERROR (LOG)");
					e.printStackTrace();
				}
			}
		});
		mensajeInformacion("AKToolKit", "Versión: "+version);
	}


	/**
	 * Create the application.
	 */
	public Interfaz() {	
		initialize();
	}

	public static void instalacion() {
		String dir = System.getProperty("user.dir");
		try {
			Files.copy(Paths.get(getResourceAsFile("DeOgg.1.00.5.24.Installer.exe", dir+File.separator, "DeOgg.1.00.5.24.Installer.exe").getPath()), Paths.get(dir+File.separator+"DeOgg.1.00.5.24.Installer.exe"), StandardCopyOption.REPLACE_EXISTING);
			Files.copy(Paths.get(getResourceAsFile("DePbo.6.44.0.16.Installer.exe", dir+File.separator, "DePbo.6.44.0.16.Installer.exe").getPath()), Paths.get(dir+File.separator+"DePbo.6.44.0.16.Installer.exe"), StandardCopyOption.REPLACE_EXISTING);
			Files.copy(Paths.get(getResourceAsFile("ExtractPbo.2.16.6.14.Installer.exe", dir+File.separator, "ExtractPbo.2.16.6.14.Installer.exe").getPath()), Paths.get(dir+File.separator+"ExtractPbo.2.16.6.14.Installer.exe"), StandardCopyOption.REPLACE_EXISTING);
			Files.copy(Paths.get(getResourceAsFile("vcredist_x64.exe", dir+File.separator, "vcredist_x64.exe").getPath()), Paths.get(dir+File.separator+"vcredist_x64.exe"), StandardCopyOption.REPLACE_EXISTING);
			Files.copy(Paths.get(getResourceAsFile("dxwebsetup.exe", dir+File.separator, "dxwebsetup.exe").getPath()), Paths.get(dir+File.separator+"dxwebsetup.exe"), StandardCopyOption.REPLACE_EXISTING);
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mensajeInformacion("INSTALA LAS HERRAMIENTAS");
		Process p = null;
		try {
			System.out.println("cmd /c \"" + dir + File.separator + "DeOgg.1.00.5.24.Installer.exe\"");
			p = Runtime.getRuntime().exec("cmd /c \"" + dir + File.separator + "DeOgg.1.00.5.24.Installer.exe\"");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			p.waitFor();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mensajeInformacion("1/5 HERRAMIENTAS");
		try {
			p = Runtime.getRuntime().exec("cmd /wait /c \"" + dir + File.separator + "DePbo.6.44.0.16.Installer.exe\"");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			p.waitFor();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mensajeInformacion("2/5 HERRAMIENTAS");
		try {
			p = Runtime.getRuntime().exec("cmd /wait /c \"" + dir + File.separator + "ExtractPbo.2.16.6.14.Installer.exe\"");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			p.waitFor();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean is64bit = false;
		if (System.getProperty("os.name").contains("Windows")) {
			is64bit = (System.getenv("ProgramFiles(x86)") != null);
		} else {
			is64bit = (System.getProperty("os.arch").indexOf("64") != -1);
		}
		String file = "";
		if (is64bit) {
			file = "vcredist_x64.exe";
		}else {
			file = "vcredist_x86.exe";
		}
		mensajeInformacion("3/5 HERRAMIENTAS");
		try {
			p = Runtime.getRuntime().exec("cmd /wait /c \"" + dir + File.separator +file+"\"");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			p.waitFor();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mensajeInformacion("4/5 HERRAMIENTAS");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			p = Runtime.getRuntime().exec("cmd /wait /c \"" + dir + File.separator +"dxwebsetup.exe\"");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			p.waitFor();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mensajeInformacion("5/5 HERRAMIENTAS");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		new File(dir+File.separator+"DeOgg.1.00.5.24.Installer.exe").delete();
		new File(dir+File.separator+"DePbo.6.44.0.16.Installer.exe").delete();
		new File(dir+File.separator+"ExtractPbo.2.16.6.14.Installer.exe").delete();
		new File(dir+File.separator+file).delete();
		new File(dir+File.separator+"dxwebsetup.exe").delete();
		instalacionCompletada = "true";
	}


	/**
	 * Funcion que instala SteamCMD en el directorio que elija el usuario
	 * @param dir String Directorio donde se instalara SteamCMD
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	private void descargarSteamCMD(String dir)throws ErrorDirectorioSteamCMD, IOException{
		if (dir.equals("")) {
			throw new ErrorDirectorioSteamCMD();
		}
		//Descargo steam_cmd.exe desde 
		if (isWindows()) {
			//Movemos el archivo al directorio indicado
			Files.copy(Paths.get(getResourceAsFile("steamcmd.exe", dir+File.separator, "steamcmd.exe").getPath()), Paths.get(dir+File.separator+"steamcmd.exe"), StandardCopyOption.REPLACE_EXISTING);
			mensajeInformacion("Moviendo SteamCMD al directorio "+dir,"Moviendo SteamCMD al directorio "+dir);
			//Cambiamos el estado
			txtSteamcmdStatus.setText("STEAMCMD ENCONTRADO");
			steamCMD = dir;
			execSteamCMD(new String[] {},"");
			mensajeInformacion("SteamCMD instalado correctamente en el directorio "+dir,"SteamCMD instalado correctamente en el directorio "+dir);
			//Indicamos que se a creado correctamente la pantalla
		}
		if (isLinux()) {
			File zipFileSrc = getResourceAsFile("steamcmd.zip",System.getProperty("user.dir"),"steamcmd.zip");
			extract(zipFileSrc,new File(dir));
			zipFileSrc.delete();
			/*
			File folderSrc = new File(Interfaz.class.getClassLoader().getResource("steamcmdlinux/linux32").getFile());
			File folderOut = new File(dir);
			FileUtils.copyDirectoryToDirectory(folderSrc, folderOut);
			Files.copy(Paths.get(Interfaz.class.getClassLoader().getResource("steamcmdlinux/steamcmd.sh").getPath()), Paths.get(dir+"/steamcmd.sh"), StandardCopyOption.REPLACE_EXISTING);
			 */
			mensajeInformacion("Moviendo SteamCMD al directorio "+dir,"Moviendo SteamCMD al directorio "+dir);
			//Cambiamos el estado
			steamCMD = dir;
			File[] steamcmd = buscarSteamCMD();
			if (steamcmd != null && steamcmd.length > 0) {
				txtSteamcmdStatus.setText("STEAMCMD ENCONTRADO");
				execSteamCMD(new String[] {},"");
				//mensajeInformacion("SteamCMD instalado correctamente en el directorio "+dir,"SteamCMD instalado correctamente en el directorio "+dir);
			}else {
				notify.createErrorNotify("ERROR AL ENCONTRAR STEAMCMD", "STEAMCMD NO ENCONTRADO");
				txtSteamcmdStatus.setText("STEAMCMD NO ENCONTRADO");
				steamCMD = "";
				//descargarSteamCMD(dir); //Reintentamos
			}
			//Indicamos que se a creado correctamente la pantalla
		}

	}

	public static boolean isWindows() {
		return osName.toLowerCase().contains("windows");
	}

	public static boolean isLinux() {
		return osName.toLowerCase().contains("linux");
	}


	private static int obtenerUltimaActualizacion (Mod mod) {
		HttpClient httpclient = HttpClients.createDefault();
		HttpPost httppost = new HttpPost("https://api.steampowered.com/ISteamRemoteStorage/GetPublishedFileDetails/v1/");
		int stampTime = 0;
		// Request parameters and other properties.
		List<NameValuePair> params = new ArrayList<NameValuePair>(2);
		params.add(new BasicNameValuePair("key", "7D03BDF3CA93ACC07F53E386B9FBF8CE"));
		params.add(new BasicNameValuePair("itemcount", "1"));
		params.add(new BasicNameValuePair("publishedfileids[0]", ""+mod.getId()));

		try {
			httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		//Execute and get the response.
		HttpResponse response = null;
		try {
			response = httpclient.execute(httppost);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		HttpEntity entity = response.getEntity();

		if (entity != null) {
			InputStream instream = null;
			try {
				instream = entity.getContent();
			} catch (UnsupportedOperationException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(instream));
				String temp;
				try {
					while ((temp = in.readLine()) != null){
						try {
							stampTime =  Integer.parseInt(temp.substring(temp.indexOf("time_updated")+14, temp.indexOf(",",temp.indexOf("time_updated"))));	
						} catch (Exception e) {
						}	
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			} finally {
				try {
					instream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return stampTime;

	}
	public static class ThreadSteamCMDMod implements Runnable { 
		private ArrayList<Mod> modsUpdate;

		public ThreadSteamCMDMod (ArrayList<Mod> modsUpdate) {
			this.modsUpdate = modsUpdate;
		}
		@Override
		public void run() {
			synchronized (this) {
				try {
					this.wait(10000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			while (true) {
				if (!estadoSteam()) {
					mensajeError("STEAMCMD NO ENCONTRADO");
					synchronized (this) {
						try {
							this.wait(5000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					mensajeInformacion("Comprobacion de mods Finalizada","Se comprobaran los mods dentro de 3 horas");
					synchronized (this) {
						try {
							this.wait(3600000 * 3);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}

				}else {
					try {
						for (Mod mod : modsUpdate) {
							int getStampTime = mod.getTimeUpdated();
							int stampTimeCheck = obtenerUltimaActualizacion(mod);
							if (stampTimeCheck > getStampTime) {
								mensajeInformacion("Actualizando " +mod.getNombreMod());
								if (Interfaz.getTxtSteamcmdStatus().getText().equals("STEAMCMD ENCONTRADO")) {

									mensajeInformacion("========================STEAMCMD=================================");
									Process p = null;
									try {
										p = Runtime.getRuntime().exec("cmd /c \"\"" + Interfaz.steamCMD + "\\steamcmd.exe\""+" +login \""+Interfaz.user+"\" \""+Interfaz.password+"\" \""+Interfaz.securityKey+"\" "+" +workshop_download_item 107410 "+ mod.getId()+" validate"+" +quit\"");	
									} catch (IOException e1) {
										mensajeInformacion("Error al abrir steamCMD (Visita la pestaña STEAMCMD)");
									}
									InputStream s = p.getInputStream();
									BufferedReader in = new BufferedReader(new InputStreamReader(s));
									String temp;
									try {
										while ((temp = in.readLine()) != null){
											downloadLabel.setText(temp);
											if (temp.equals("Waiting for user info...OK")) {
												downloadLabel.setText("Downloading item "+ mod.getId() +" ...");
											}
											if (temp.contains("ERROR! Timeout downloading item")) {
												downloadLabel.setText("Descarga Fallida");
												p.destroy();
												return;
											}
											if (temp.contains("Steam Guard")) {
												downloadLabel.setText("Añade tu Steam Guard en la pestaña STEAMCMD");
												p.destroy();
												return;
											}
										}
									} catch (IOException e) {
										e.printStackTrace();
									}
									if (p.exitValue() == 5) {
										mensajeError("USUARIO O CONTRASEÑA INCORRECTA");
									}
									if (p.exitValue() == 84) {
										mensajeError("CUENTA BLOQUEADA TEMPORALMENTE");
									}
									try {
										s.close();
									} catch (IOException e1) {
										e1.printStackTrace();
									}
									p.destroy();

									mensajeInformacion("MOVIENDO MOD");
									File srcDir = new File(Interfaz.steamCMD+File.separator+"steamapps"+File.separator+"workshop"+File.separator+"content"+File.separator+"107410"+File.separator+mod.getId());
									srcDir.renameTo(new File(Interfaz.steamCMD+File.separator+"steamapps"+File.separator+"workshop"+File.separator+"content"+File.separator+"107410"+File.separator+mod.getNombreMod()));
									srcDir = new File(Interfaz.steamCMD+File.separator+"steamapps"+File.separator+"workshop"+File.separator+"content"+File.separator+"107410"+File.separator+mod.getNombreMod());
									try {
										FileUtils.copyDirectory(srcDir, new File(mod.getDirMod()));
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									srcDir.delete();
									extraerPaquetesMod(new File(mod.getDirMod()));
									mod.setTimeUpdated(stampTimeCheck);
									ArrayList<String> bikeys = new ArrayList<String>();
									searchBikeyLastModify(bikeys,new File(mod.getDirMod()));
									mod.setDirKey(bikeys);
								}else {
									mensajeError("STEAMCMD NO ENCONTRADO");
								}
							}
							synchronized (this) {
								try {
									this.wait(5000);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							}
						}
					}catch (Exception e) {
						ThreadSteamCMDMod mr = new ThreadSteamCMDMod(mods);
						Thread t = new Thread(mr);
						t.start();
					}
				}
				mensajeInformacion("Comprobacion de mods Finalizada","Se comprobaran los mods dentro de 3 horas");
				synchronized (this) {
					try {
						this.wait(3600000 * 3);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}


	private static void extraerPaquetesMod(File file) {
		if (file.isDirectory()) {
			File[] arr = file.listFiles();
			for (File f : arr) {
				extraerPaquetesMod(f);
			}
		} else {
			if (file.getName().endsWith(".pbo")) {
				extraerModPBO(file);
			}
		}
	}
	private static boolean extraerModPBO(File f) {
		if (f== null) {
			notify.createErrorNotify("Mod no encontrado","Mod no encontrado");
			return false;
		}
		String directorioPbo = f.getParent();
		String path = System.getProperty("user.dir")+File.separator;
		if (isLinux()) {
			moveDePBOLinux(path);
		}
		//Existe el archivo
		Process r = null;
		if (isWindows()) {
			try {
				r = Runtime.getRuntime().exec("cmd /c cd /d \"" + directorioPbo +"\" && ExtractPboDos.exe -P " + f.getName());	
			} catch (IOException e1) {
				mensajeError("Error","Error al extraer pbo");
			}
			try {
				r.waitFor();
			} catch (InterruptedException e1) {
				mensajeInformacion("Error al esperar");
			}
		}
		if (isLinux()) {
			try {
				r = Runtime.getRuntime().exec(new String[] {"/bin/sh","-c","./DePBO-linux/execMission.sh "+path+" "+directorioPbo+" "+f.getName()});
			} catch (IOException e1) {
				mensajeError("Error","Error al extraer pbo");
			}
			try {
				r.waitFor();
			} catch (InterruptedException e1) {
				mensajeInformacion("Error al esperar");
			}
		}
		return true;
	}

	private static void moveDePBOLinux(String path) {
		File folderSrc = new File(Interfaz.class.getClassLoader().getResource("DePBO-linux").getFile());
		File folderOut = new File(path);
		try {
			FileUtils.copyDirectoryToDirectory(folderSrc, folderOut);
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			Runtime.getRuntime().exec("sudo chmod +x "+path+"/DePBO-linux/bin/ExtractPboDos");
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			Runtime.getRuntime().exec("sudo chmod +x "+path+"/DePBO-linux/execMission.sh");

		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			Runtime.getRuntime().exec("sudo chmod +x "+path+"/DePBO-linux/execInstallSteamCMD.sh");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/*********************MENSAJES MODALES************************/
	public static void mensajeInformacion(String title,String msg) {
		notify.createNotify(title,msg);
	}

	public static void mensajeAdvertencia(String title,String msg) {
		notify.createWarningNotify(title,msg);
	}

	public static void mensajeError(String title,String msg) {
		notify.createErrorNotify(title,msg);
	}

	public static void mensajeInformacion(String title) {
		notify.createNotify(title,"");
	}

	public static void mensajeAdvertencia(String title) {
		notify.createWarningNotify(title,"");
	}

	public static void mensajeError(String title) {
		notify.createErrorNotify(title,"");
	}

	/********************************************************************/

	public static File getResourceAsFile(String resourcePath,String path,String name) {
		try {
			InputStream in = ClassLoader.getSystemClassLoader().getResourceAsStream(resourcePath);
			if (in == null) {
				return null;
			}

			File tempFile = (new File(path + name));

			try (FileOutputStream out = new FileOutputStream(tempFile)) {
				//copy stream
				byte[] buffer = new byte[1024];
				int bytesRead;
				while ((bytesRead = in.read(buffer)) != -1) {
					out.write(buffer, 0, bytesRead);
				}
			}
			return tempFile;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}


	static void hide(File src) throws InterruptedException, IOException {
		// win32 command line variant
		if (isWindows()) {
			Process p = Runtime.getRuntime().exec("attrib +h " + src.getPath());
			p.waitFor(); // p.waitFor() important, so that the file really appears as hidden immediately after function exit.
			p.destroy();
		}
		if (isLinux()) {/* LOS DE LINUX QUE SE *****. ES SUPER COMPLICADO ESCONDER UN ARCHIVO EN LINUX :(
			mensajeInformacion("sudo mv " + src.getPath() + " " +src.getParentFile().getPath()+"."+src.getName());
			Process p = Runtime.getRuntime().exec("sudo mv " + src.getPath() + " " +src.getParentFile().getPath()+"."+src.getName());
			p.waitFor(); // p.waitFor() important, so that the file really appears as hidden immediately after function exit.
			p.destroy();
		 */
		}

	}

	static void nohide(File src) throws InterruptedException, IOException {
		// win32 command line variant
		if (isWindows()) {
			Process p = Runtime.getRuntime().exec("attrib -h " + src.getPath());
			p.waitFor(); // p.waitFor() important, so that the file really appears as hidden immediately after function exit.
			p.destroy();
		}
		if (isLinux()) {
			/*
			mensajeInformacion("sudo mv " + src.getPath() + " " +src.getParentFile().getPath()+src.getName().substring(1, src.getName().length()));
			Process p = Runtime.getRuntime().exec("sudo mv " + src.getPath() + " " +src.getParentFile().getPath()+src.getName().substring(1, src.getName().length()));
			p.waitFor(); // p.waitFor() important, so that the file really appears as hidden immediately after function exit.
			p.destroy();
			 */
		}
	}
	private static final int  BUFFER_SIZE = 4096;
	static JTextField defaultModsFolder;
	private static void extractFile(ZipInputStream in, File outdir, String name) throws IOException
	{
		byte[] buffer = new byte[BUFFER_SIZE];
		BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(new File(outdir,name)));
		int count = -1;
		while ((count = in.read(buffer)) != -1)
			out.write(buffer, 0, count);
		out.close();
	}

	private static void mkdirs(File outdir,String path)
	{
		File d = new File(outdir, path);
		if( !d.exists() )
			d.mkdirs();
	}

	private static String dirpart(String name)
	{
		int s = name.lastIndexOf( File.separatorChar );
		return s == -1 ? null : name.substring( 0, s );
	}

	/***
	 * Extract zipfile to outdir with complete directory structure
	 * @param zipfile Input .zip file
	 * @param outdir Output directory
	 */
	public static void extract(File zipfile, File outdir)
	{
		try
		{
			ZipInputStream zin = new ZipInputStream(new FileInputStream(zipfile));
			ZipEntry entry;
			String name, dir;
			while ((entry = zin.getNextEntry()) != null)
			{
				name = entry.getName();
				if( entry.isDirectory() )
				{
					mkdirs(outdir,name);
					continue;
				}
				/* this part is necessary because file entry can come before
				 * directory entry where is file located
				 * i.e.:
				 *   /foo/foo.txt
				 *   /foo/
				 */
				dir = dirpart(name);
				if( dir != null )
					mkdirs(outdir,dir);

				extractFile(zin, outdir, name);
			}
			zin.close();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}


	/*************************************************************/

	@SuppressWarnings("unused")
	private boolean extraerMisionPBO(Mision m) {
		File f = new File(m.getDirMision());
		if (f.getPath().contains("%20")) {
			System.out.println("CAMBIANDO " + f.getPath() + " a " + m.getDirMision().replaceAll("%20", "") );
			f.renameTo(new File(m.getDirMision().replaceAll("%20", "")));
			f = new File(f.getPath().replaceAll("%20", ""));
			m.setDirMision(f.getAbsolutePath());
		}
		if (f== null) {
			mensajeError("Mision "+m.getNombreMision() +" no encontrado");
			return false;
		}
		String directorioPbo = f.getParent();
		String path = System.getProperty("user.dir")+File.separator;
		if (isLinux()) {
			File zipFileSrc = getResourceAsFile("DePBO-linux.zip",path,"DePBO-linux.zip");
			extract(zipFileSrc,new File(System.getProperty("user.dir")));
			zipFileSrc.delete();
			try {
				Process s = Runtime.getRuntime().exec("sudo chmod +x "+path+"/DePBO-linux/bin/ExtractPboDos");
				try {
					s.waitFor();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				Process s = Runtime.getRuntime().exec("sudo chmod +x "+path+"/DePBO-linux/exec.sh");
				try {
					s.waitFor();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		//Existe el archivo
		Process r = null;
		if (isWindows()) {
			try {
				r = Runtime.getRuntime().exec("cmd /c cd /d \"" + directorioPbo +"\" && ExtractPboDos.exe -P " + f.getName());	
			} catch (IOException e1) {
				mensajeError("Error","Error al extraer pbo");
			}
			try {
				r.waitFor();
			} catch (InterruptedException e1) {
				mensajeInformacion("Error al esperar");
			}
		}
		if (isLinux()) {
			try {
				r = Runtime.getRuntime().exec(new String[] {"/bin/sh","-c","./DePBO-linux/execMission.sh "+path+" "+directorioPbo+" "+f.getName()});
			} catch (IOException e1) {
				mensajeError("Error","Error al extraer pbo");
			}
		}
		//LEEMOS EL MISSION.SQM
		File missionSQM = new File(directorioPbo+File.separator+f.getName().substring(0, f.getName().length()-4)+File.separator+"mission.sqm");
		FileReader fileReader = null;
		try {
			fileReader = new FileReader(missionSQM);
		} catch (FileNotFoundException e) {
			System.out.println("Error al leer archivo "+missionSQM.getPath());
			return false;
		}

		BufferedReader bufferedReader = null;
		try {
			bufferedReader = 
					new BufferedReader(fileReader);
			String line2 = null;
			boolean modulosBasicosEncontrados = false;
			boolean modulosAvanzadosEncontrados = false;
			while((line2 = bufferedReader.readLine()) != null || (modulosBasicosEncontrados && modulosAvanzadosEncontrados)) {
				if (line2.contains("addons[]")) {
					if (line2.equals("addons[]=")) {
						bufferedReader.readLine(); // -> {
						line2 = "addons[] = {";
						String line3;
						boolean finAddons = false;
						while (!finAddons) {
							line3 = bufferedReader.readLine();
							if (line3.equals("};")) {
								finAddons = true;
							}else {
								line2 = line2 + line3.replaceAll("\\s+","");
							}

						}
						line2 = line2 + "};";
					}
					String modulos = line2.substring(12,line2.indexOf(";")-1);
					String[] subModulos = modulos.split(",");
					for (int i = 0; i < subModulos.length; i++) {
						String nombreModulo = subModulos[i].substring(1,subModulos[i].indexOf("\"",1));
						if (!nombreModulo.startsWith("A3")) {
							m.addElement(nombreModulo);

						}
					}
					modulosBasicosEncontrados = true;
				}
				if (line2.equals("property = \"cba_settings_hash\";")) {
					m.addElement("cba_main");
					modulosAvanzadosEncontrados = true;
				}
			}
		} catch (NumberFormatException | IOException | NullPointerException e ) {
			e.printStackTrace();
			mensajeInformacion("Error al leer archivo "+missionSQM.getPath());
			try {
				bufferedReader.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			return false;
		}   
		// Always close files.
		try {
			bufferedReader.close();
		} catch (IOException e) {
			e.printStackTrace();
			mensajeError("Error al leer archivo "+missionSQM.getPath());
			return false;
		}
		File f2 = new File(f.getPath().substring(0,m.getDirMision().indexOf(".pbo")));
		try {
			FileUtils.deleteDirectory(f2);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

	@SuppressWarnings("unlikely-arg-type")
	public static boolean estadoSteam () {
		String string = "";
		if (steamCMDTF.equals(string) || inputUser.equals(string) || inputPassword.getPassword().equals(string) || inputKey.getPassword().equals(string) || txtSteamcmdStatus.getText().equals("STEAMCMD NO ENCONTRADO")) {
			return false;
		}
		return true;


	}

	public static String servidoresTxt = "";
	public static String modsTxt = "";
	public static String misionesTxt = "";
	public static String perfilesTxt = "";
	public static Integer counterServidores = 0;
	public static Integer counterMods = 0;
	public static Integer counterMisiones = 0;
	public static Integer counterPerfiles = 0;
	private static void saveInfoToProperties() {
		try {
			File f2 = new File("config.properties");
			try {
				Interfaz.nohide(f2);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			output = new FileOutputStream("config.properties");
			prop.setProperty("dirSteamCMD", steamCMDTF.getText());
			prop.setProperty("user", inputUser.getText());
			/***PASSWORD***/
			prop.setProperty("password", new String(inputPassword.getPassword()));
			/***KEY***/
			prop.setProperty("securityKey", new String(inputKey.getPassword()));
			//SERVIDORES
			if (servidores.size() < 1) {
				prop.setProperty("servidores", "");
			}else {
				for (int i = 0; i < servidores.size(); i++) {
					Servidor servidor = servidores.get(i);
					if (i == servidores.size() - 1) {
						servidoresTxt = servidoresTxt + "[" + servidor.getNombreServidor() + ";" + servidor.getDirServidor() + ";" + servidor.getContrasena() + ";" + servidor.getNombreServidorMision() + ";" + servidor.getPuerto()+ ";" + servidor.isAutoinit()+
								";" + servidor.isHugepages()+";" + servidor.isNologs()+";" + servidor.isEnableHT()+";" + servidor.isFilePatching()+";" + servidor.isNoCB()+";"
								+servidor.getMaxBandWidth()+";"+servidor.getMinbandwidth()+";"+servidor.getMaxmsgsend()+";"+servidor.getMaxSizeGuaranteed()+";"+servidor.getMaxSizeNonguaranteed()+";"+servidor.getMinErrorToSend()+";"+servidor.getMinErrorToSendNear()+";"
								+servidor.getPasswordAdmin()+";"+servidor.getServerCommandPassword()+";"+servidor.getLogFile()+";"+servidor.getMotd()+";"+servidor.getMotdInterval()+";"+servidor.getMaxPlayers()+";"+servidor.getKickduplicate()+";"
								+servidor.getVerifySignatures()+";"+servidor.getAllowedFilePatching()+";"+servidor.getRequiredSecureId()+";"+servidor.getVoteMissionPlayers()+";"+servidor.getVoteThreshold()+";"+servidor.getAllowedVoteCmds()+";"+servidor.getDisableVoN()+";"
								+servidor.getVonCodecQuality()+";"+servidor.getPersistent()+";"+servidor.getTimeStampFormat()+";"+servidor.getBattlEye()+";"+servidor.getHeadlessClients()+";"+servidor.getDoubleIdDetected()+";"+servidor.getOnUserConnected()+";"
								+servidor.getOnUserDisconnected()+";"+servidor.getOnHackedData()+";"+servidor.getOnDifferentData()+";"+servidor.getOnUnsignedData()+";"+servidor.getRegularCheck()+"]";
					}else {
						servidoresTxt = servidoresTxt + "[" + servidor.getNombreServidor() + ";" + servidor.getDirServidor() + ";" + servidor.getContrasena() + ";" + servidor.getNombreServidorMision() + ";" + servidor.getPuerto()+ ";" + servidor.isAutoinit()+
								";" + servidor.isHugepages()+";" + servidor.isNologs()+";" + servidor.isEnableHT()+";" + servidor.isFilePatching()+";" + servidor.isNoCB()+";"
								+servidor.getMaxBandWidth()+";"+servidor.getMinbandwidth()+";"+servidor.getMaxmsgsend()+";"+servidor.getMaxSizeGuaranteed()+";"+servidor.getMaxSizeNonguaranteed()+";"+servidor.getMinErrorToSend()+";"+servidor.getMinErrorToSendNear()+";"
								+servidor.getPasswordAdmin()+";"+servidor.getServerCommandPassword()+";"+servidor.getLogFile()+";"+servidor.getMotd()+";"+servidor.getMotdInterval()+";"+servidor.getMaxPlayers()+";"+servidor.getKickduplicate()+";"
								+servidor.getVerifySignatures()+";"+servidor.getAllowedFilePatching()+";"+servidor.getRequiredSecureId()+";"+servidor.getVoteMissionPlayers()+";"+servidor.getVoteThreshold()+";"+servidor.getAllowedVoteCmds()+";"+servidor.getDisableVoN()+";"
								+servidor.getVonCodecQuality()+";"+servidor.getPersistent()+";"+servidor.getTimeStampFormat()+";"+servidor.getBattlEye()+";"+servidor.getHeadlessClients()+";"+servidor.getDoubleIdDetected()+";"+servidor.getOnUserConnected()+";"
								+servidor.getOnUserDisconnected()+";"+servidor.getOnHackedData()+";"+servidor.getOnDifferentData()+";"+servidor.getOnUnsignedData()+";"+servidor.getRegularCheck()+"]#";
					}			
			}
				counterServidores = 0;
				prop.setProperty("servidores", servidoresTxt);
			}
			//MODS
			if (mods.size() < 1) {
				prop.setProperty("mods", "");
			}else {
				for (int i = 0; i < mods.size(); i++) {
					Mod mod = mods.get(i);
					if (i == mods.size() - 1) {
						modsTxt = modsTxt + "[" + mod.getNombreMod() + ";" + mod.getDirMod() + ";" + mod.escribirKeys() + ";" + mod.getId() + ";" + mod.escribirPaquetes() +  "]";
					}else {
						modsTxt = modsTxt + "[" + mod.getNombreMod() + ";" + mod.getDirMod() + ";" + mod.escribirKeys() + ";" + mod.getId() + ";" + mod.escribirPaquetes() +  "]#";
					}
				}
				prop.setProperty("mods", modsTxt);
			}
			//MISIONES
			if (misiones.size() < 1) {
				prop.setProperty("misiones", "");
			}else {

				for (int i = 0; i < misiones.size(); i++) {
					Mision m = misiones.get(i);
					if (i == misiones.size() - 1) {
						misionesTxt = misionesTxt + "[" + m.getNombreMision() + ";" + m.getDirMision() + "]";
					}else {
						misionesTxt = misionesTxt + "[" + m.getNombreMision() + ";" + m.getDirMision() + "]#";
					}

				}
				prop.setProperty("misiones", misionesTxt);
			}
			//PERFILES
			if (perfiles.size() < 1) {
				prop.setProperty("perfiles", "");
			}else {
				//[nombrePerfil,Servidor s,[Mod m1,Mod m2],Mision m]

				for (int i = 0; i < perfiles.size(); i++) {
					//MODS
					String modsTxt = "";
					for (int j = 0; j < perfiles.get(i).getListaMods().size(); j++) {
						Mod mod = perfiles.get(i).getListaMods().get(j);
						if (j == perfiles.get(i).getListaMods().size() - 1) {
							modsTxt = modsTxt + "[" + mod.getNombreMod() + ";" + mod.getDirMod() + ";" + mod.escribirKeys() + ";" + mod.getId() + ";" + mod.escribirPaquetes() +  "]";
						}else {
							modsTxt = modsTxt + "[" + mod.getNombreMod() + ";" + mod.getDirMod() + ";" + mod.escribirKeys() + ";" + mod.getId() + ";" + mod.escribirPaquetes() +  "]&";
						}

					}
					//PERFIL
					Perfil p = perfiles.get(i);
					Servidor servidorPerfil  =  p.getSv();
					Mision m = p.getMision();
					if (i == perfiles.size() - 1) {
						perfilesTxt = perfilesTxt + "[" + p.getNombrePerfil() + "_-_" + "[" + servidorPerfil.getNombreServidor() + ";" + servidorPerfil.getDirServidor() + ";" + servidorPerfil.getContrasena() + ";" + servidorPerfil.getNombreServidorMision() + ";" + servidorPerfil.getPuerto()+ ";" + servidorPerfil.isAutoinit()+
								";" + servidorPerfil.isHugepages()+";" + servidorPerfil.isNologs()+";" + servidorPerfil.isEnableHT()+";" + servidorPerfil.isFilePatching()+";" + servidorPerfil.isNoCB()+"]_-_" + modsTxt + "_-_[" + m.getNombreMision() + ";" + m.getDirMision() + "]"+"]";
					}else {
						perfilesTxt = perfilesTxt + "[" + p.getNombrePerfil() + "_-_" + "[" + servidorPerfil.getNombreServidor() + ";" + servidorPerfil.getDirServidor() + ";" + servidorPerfil.getContrasena() + ";" + servidorPerfil.getNombreServidorMision() + ";" + servidorPerfil.getPuerto()+ ";" + servidorPerfil.isAutoinit()+
								";" + servidorPerfil.isHugepages()+";" + servidorPerfil.isNologs()+";" + servidorPerfil.isEnableHT()+";" + servidorPerfil.isFilePatching()+";" + servidorPerfil.isNoCB()+"]_-_" + modsTxt + "_-_[" + m.getNombreMision() + ";" + m.getDirMision() + "]"+"]#";
					}

				}
				prop.setProperty("perfiles", perfilesTxt);
			}
			servidoresTxt = "";
			modsTxt = "";
			misionesTxt = "";
			perfilesTxt = "";
			prop.setProperty("version", Interfaz.version);
			prop.setProperty("otaupdate",Interfaz.otarestart);
			prop.setProperty("deviceId", Interfaz.deviceId);
			prop.setProperty("deviceToken", Interfaz.deviceToken);
			prop.setProperty("token", Interfaz.token);
			prop.setProperty("instalacion", Interfaz.instalacionCompletada);
			prop.setProperty("defaultFolderMods", defaultFolderMods);
			// save properties to project root folder
			prop.store(output, "ToolKit AK");

		} catch (IOException io) {
			mensajeInformacion("Error al intentar abrir el archivo 'config.properties'");
		} finally {
			if (output != null) {
				try {
					output.close();
					File f = new File("config.properties");
					try {
						hide(f);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} catch (IOException e) {
					mensajeInformacion("Error al intentar cerrar el archivo 'config.properties'");
				}
			}

		}
	}





	private static class ThreadServerArma3 implements Runnable { 
		private ArrayList<Mod> mods;
		private ArrayList<Mod> modsOpcionales;
		private Servidor sv;
		private Mision m;

		public ThreadServerArma3 (ArrayList<Mod> mods,ArrayList<Mod> modsOpcionales, Servidor sv,Mision m) {
			this.mods = mods;
			this.modsOpcionales = modsOpcionales;
			this.sv = sv;
			this.m = m;
		}
		@Override
		public void run() { 

			String fullMods = "";
			for (int i = 0; i < mods.size(); i++) {
				fullMods = fullMods + mods.get(i).getDirMod()+";";
			}
			/*
			String fullModsOpcionales = "";
			for (int i = 0; i < modsOpcionales.size(); i++) {
				fullModsOpcionales = fullModsOpcionales + modsOpcionales.get(i).getDirMod()+";";
			}*/
			String advanceSetting = "";
			if (sv.isAutoinit()) {
				advanceSetting = advanceSetting + " -autoinit";
			}
			if (sv.isEnableHT()) {
				advanceSetting = advanceSetting + " -enableHT";
			}
			if (sv.isFilePatching()) {
				advanceSetting = advanceSetting + " -filePatching";
			}
			if (sv.isHugepages()) {
				advanceSetting = advanceSetting + " -hugepages";
			}
			if (sv.isNoCB()) {
				advanceSetting = advanceSetting + " -noCB";
			}
			if (sv.isNologs()) {
				advanceSetting = advanceSetting + " -noLogs";
			}

			//Limpio el directorio de keys
			try {
				FileUtils.copyFile(new File(sv.getDirServidor()+"\\keys\\a3.bikey"), new File(sv.getDirServidor()+"\\a3.bikey"));
				FileUtils.cleanDirectory(new File(sv.getDirServidor()+"\\keys\\"));
				FileUtils.copyFile(new File(sv.getDirServidor()+"\\a3.bikey"),new File(sv.getDirServidor()+"\\keys\\a3.bikey"));
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			//Copiar KEYS AL DIRSERVER/keys
			for (int i = 0; i < mods.size(); i++) {
				for (int j = 0; j < mods.get(i).getDirKeys().size(); j++) {
					File modKey = new File(mods.get(i).getDirKeys().get(j));
					try {
						System.out.println("Moviendo "+modKey+" a "+sv.getDirServidor()+"\\keys\\"+modKey.getName());
						Files.copy(modKey.toPath(), (new File(sv.getDirServidor()+"\\keys\\" + modKey.getName())).toPath(), StandardCopyOption.REPLACE_EXISTING);
					} catch (IOException e) {
						System.out.println("Mod "+mods.get(i).getNombreMod()+" sin key");
					}
				}
			}
			
			for (int i = 0; i < modsOpcionales.size(); i++) {
				for (int j = 0; j < modsOpcionales.get(i).getDirKeys().size(); j++) {
					File modKey = new File(modsOpcionales.get(i).getDirKeys().get(j));
					try {
						System.out.println("Moviendo "+modKey+" a "+sv.getDirServidor()+"\\keys\\"+modKey.getName());
						Files.copy(modKey.toPath(), (new File(sv.getDirServidor()+"\\keys\\" + modKey.getName())).toPath(), StandardCopyOption.REPLACE_EXISTING);
					} catch (IOException e) {
						System.out.println("Mod "+modsOpcionales.get(i).getNombreMod()+" sin key");
					}
				}
			}
			//Mision (copio mision por si no esta en mpmissions)
			try {
				Files.copy(new File(m.getDirMision()).toPath(), (new File(sv.getDirServidor()+"\\mpmissions\\" + m.getDirMision().substring(m.getDirMision().lastIndexOf("\\")+1, m.getDirMision().length()))).toPath(), StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			//PREPARAR config_basic
			FileOutputStream fopbasic = null;
			File filebasic;
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss - dd-MM-YY");
			String contentbasic = "//Generated By AK ToolKit " + sdf.format(cal.getTime())+"\r\n"
					+ "language=\"English\";\r\n" + 
					"adapter=-1;\r\n" + 
					"3D_Performance=1.000000;\r\n" + 
					"Resolution_W=800;\r\n" + 
					"Resolution_H=600;\r\n" + 
					"Resolution_Bpp=32;\r\n" + 
					"\r\n" + 
					"\r\n" + 
					"Maxbandwidth= "+sv.getMaxBandWidth()+";\r\n" + 
					"Minbandwidth= "+sv.getMinbandwidth()+";\r\n" + 
					"Maxmsgsend= "+sv.getMaxmsgsend()+";\r\n" + 
					"MaxSizeGuaranteed= "+sv.getMaxSizeGuaranteed()+";\r\n" + 
					"MaxSizeNonguaranteed= "+sv.getMaxSizeNonguaranteed()+";\r\n" + 
					"MinErrorToSend="+sv.getMinErrorToSend()+";\r\n" + 
					"MinErrorToSendNear="+sv.getMinErrorToSendNear()+";";
			try {

				filebasic = new File(sv.getDirServidor()+"\\config_basic.cfg");
				fopbasic = new FileOutputStream(filebasic);

				// if file doesnt exists, then create it
				if (!filebasic.exists()) {
					filebasic.createNewFile();
				}

				// get the content in bytes
				byte[] contentInBytes = contentbasic.getBytes();

				fopbasic.write(contentInBytes);
				fopbasic.flush();
				fopbasic.close();

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (fopbasic != null) {
						fopbasic.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}


			//PREPARAR config_config.
			//
			FileOutputStream fop = null;
			File f2 = new File(m.getDirMision());
			File file;
			cal = Calendar.getInstance();
			String content = "//Generated By AK ToolKit " + sdf.format(cal.getTime())+"\r\n"
					+ "hostName = \""+sv.getNombreServidorMision()+"\";\r\n" +  
					"password = \""+String.valueOf(sv.getContrasena())+"\";\r\n" + 
					"passwordAdmin = \""+sv.getPasswordAdmin()+"\";\r\n" + 
					"serverCommandPassword = \""+sv.getServerCommandPassword()+"\";\r\n" + 
					"logFile = \""+sv.getLogFile()+"\";\r\n" + 
					"\r\n" + 
					"motd[] = {"+sv.getMotd()+"\r\n" + 
					"};\r\n" + 
					"motdInterval = "+sv.getMotdInterval()+";\r\n" + 
					"\r\n" + 
					"maxPlayers = "+sv.getMaxPlayers()+";\r\n" + 
					"kickduplicate = "+sv.getKickduplicate()+";\r\n" + 
					"verifySignatures = "+sv.getVerifySignatures()+";\r\n" + 
					"allowedFilePatching = "+sv.getAllowedFilePatching()+";\r\n" + 
					"requiredSecureId = "+sv.getRequiredSecureId()+";\r\n" + 
					"\r\n" + 
					"voteMissionPlayers = "+sv.getVoteMissionPlayers()+";\r\n" + 
					"voteThreshold = "+sv.getVoteThreshold()+";\r\n" + 
					"allowedVoteCmds[] = {"+sv.getAllowedVoteCmds()+"};\r\n" + 
					"disableVoN = "+sv.getDisableVoN()+";\r\n" + 
					"vonCodecQuality = "+sv.getVonCodecQuality()+";\r\n" + 
					"persistent = "+sv.getPersistent()+";\r\n" + 
					"timeStampFormat = \""+sv.getTimeStampFormat()+"\";\r\n" + 
					"BattlEye = "+sv.getBattlEye()+";\r\n" + 
					"headlessClients[] ={"+sv.getHeadlessClients()+"};\r\n" + 
					"\r\n" + 
					"doubleIdDetected = \""+sv.getDoubleIdDetected()+"\";\r\n" + 
					"onUserConnected = \""+sv.getOnUserConnected()+"\";\r\n" + 
					"onUserDisconnected = \""+sv.getOnUserDisconnected()+"\";\r\n" + 
					"onHackedData = \""+sv.getOnHackedData()+"\";\r\n" + 
					"onDifferentData = \""+sv.getOnDifferentData()+"\";\r\n" + 
					"onUnsignedData = \""+sv.getOnUnsignedData()+"\";\r\n" + 
					"regularCheck = \""+sv.getRegularCheck()+"\";\r\n" + 
					"\r\n" + 
					"class Missions\r\n" + 
					"{\r\n" + 
					"	class Mission_1\r\n" + 
					"	{\r\n" + 
					"		template = \""+f2.getName().substring(0, f2.getName().length()-4)+"\";\r\n" + 
					"		difficulty = \"Custom\";\r\n" + 
					"	};\r\n" + 
					"\r\n" + 
					"\r\n" + 
					"};";

			try {

				file = new File(sv.getDirServidor()+"\\config_config.cfg");
				fop = new FileOutputStream(file);

				// if file doesnt exists, then create it
				if (!file.exists()) {
					file.createNewFile();
				}

				// get the content in bytes
				byte[] contentInBytes = content.getBytes();

				fop.write(contentInBytes);
				fop.flush();
				fop.close();

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (fop != null) {
						fop.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			//INICIAMOS EL SERVIDOR
			Process p = null;
			File f = new File(sv.getDirServidor());
			File[] matchingFiles = f.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return (name.startsWith("arma3server") && name.endsWith(".exe")) || (name.startsWith("arma3server_x64") && name.endsWith(".exe"));
				}
			});
			if (matchingFiles != null && matchingFiles.length > 0) {
				//Existe el archivo
				try {
					//ProcessBuilder pr = new ProcessBuilder("cmd","/c","start","/wait","\"Arma 3 Server Op."+m.getNombreMision()+"\"","\"" + sv.getDirServidor() + "\\arma3server_x64.exe\"","-port="+sv.getPuerto()+"","\"-config="+sv.getDirServidor()+"\\config_config.cfg\"","\"-cfg="+sv.getDirServidor()+"\\config_basic.cfg\"","\"-profiles="+sv.getDirServidor()+"\\logs\"",advanceSetting," \"-mod="+fullMods+"\" \"-servermod=\"");
					//System.out.println(""+pr.command());
					//p = pr.start();
					p = Runtime.getRuntime().exec("cmd /c start /wait \"Arma 3 Server\" \"" + sv.getDirServidor() + "\\arma3server_x64.exe\" -port="+sv.getPuerto()+" \"-config="+sv.getDirServidor()+"\\config_config.cfg\" \"-cfg="+sv.getDirServidor()+"\\config_basic.cfg\" \"-profiles="+sv.getDirServidor()+"\\logs\" \"-mod="+fullMods+"\" \"-servermod="+"\""+advanceSetting);
					System.out.println("cmd /c start /wait \"Arma 3 Server\" \"" + sv.getDirServidor() + "\\arma3server_x64.exe\" -port="+sv.getPuerto()+" \"-config="+sv.getDirServidor()+"\\config_config.cfg\" \"-cfg="+sv.getDirServidor()+"\\config_basic.cfg\" \"-profiles="+sv.getDirServidor()+"\\logs\" \"-mod="+fullMods+"\" \"-servermod="+"\""+advanceSetting);
					try {
						p.waitFor();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					p.destroy();
				} catch (IOException e1) {
					mensajeError("Error al abrir arma3server.exe", "Error al abrir arma3server.exe (Contacta con Fr4nKy)");
				}
				//start arma3server_x64.exe -port=2302 "-config=D:\Servidor1\config_config.cfg" "-cfg=D:\Servidor1\config_basic.cfg" "-profiles=D:\Servidor1\logs" -name=default -filePatching "-mod=D:\Mods\@CBA_A3;D:\Mods\@taskforceradio;D:\Mods\@ACE;D:\Mods\@FFAA;D:\Mods\@FFAA_MOD_ACE3_Compatibility_Addon;D:\Mods\@EnchanceMovement;D:\Mods\@AdvancedUrbanRappelling" "-servermod=D:\Mods\@gcam" -autoInit
			}else {
				mensajeError("ERROR","NO SE A ENCONTRADO LANZADOR DE ARMA 3");
			}

		}
	}

	private class ThreadSteamCMD implements Runnable { 
		private String[] commands;
		private String tipo;

		public ThreadSteamCMD (String[] commands,String tipo) {
			this.commands = commands;
			this.tipo = tipo;
		}
		@Override
		public void run() { 
			if (txtSteamcmdStatus.getText().equals("STEAMCMD ENCONTRADO")) {
				String fullCommands = "";
				for (int i = 0; i < commands.length; i++) {
					fullCommands = fullCommands + "+" + commands[i]+" ";
				}
				try {
					downloadLabel.setText("Ejecutando SteamCMD...");
					if (isWindows()) {
						Process p = Runtime.getRuntime().exec("cmd /c start /wait \"SteamCMD\" \""+ steamCMD + "\\steamcmd.exe\" "+fullCommands+"+quit");
						p.waitFor();
						p.destroy();
						mensajeInformacion("STEAMCMD Finalizado", "Proceso de Steamcmd finalizado");
						downloadLabel.setText("");
						downloadPB.setValue(0);
						downloadPB.setStringPainted(false);
					}
					if (isLinux()) {
						try {
							Process s = Runtime.getRuntime().exec("sudo chmod +x "+steamCMD + "/steamcmd.sh");
							s = Runtime.getRuntime().exec("sudo chmod +x "+steamCMD + "/linux32/steamcmd");
							try {
								s.waitFor();
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
						Runtime.getRuntime().exec("sudo " + steamCMD + "/steamcmd.sh "+fullCommands+" +quit");	
					}
				}catch (Exception e) {
					System.out.println("Error al ejecutar steamcmd");
				}
			}else {
				mensajeError("ERROR", "STEAMCMD NO ENCONTRADO. Visita el apartado SteamCMD para configurar el SteamCMD");
			} 
			switch (tipo) {
			case "servidor":
				File f = new File(serverDir.getText());
				File[] matchingFiles = f.listFiles(new FilenameFilter() {
					public boolean accept(File dir, String name) {
						return (name.startsWith("arma3server") && name.endsWith(".exe")) || (name.startsWith("arma3server_x64") && name.endsWith(".exe"));
					}
				});
				if (matchingFiles != null && matchingFiles.length > 0) {
					//Existe el archivo
					if (!serverName.getText().equals("")) {
						btnAgregarServidor.setEnabled(true);
					}
					mensajeInformacion("Servidor Encontrado en la ruta: " + serverDir.getText());
					btnDescargarServidor.setEnabled(false);
				}else {
					btnAgregarServidor.setEnabled(false);
					if (!serverName.getText().equals("")) {
						btnDescargarServidor.setEnabled(true);
					}else {
						btnDescargarServidor.setEnabled(false);
					}
					mensajeInformacion("Servidor no Encontrado en la ruta: " + serverDir.getText());
				}

				break;
			case "mod":
				//Mover mod
				downloadLabel.setText("Moviendo Mods");
				Process p2 = null;
				try {
					if (isWindows()) {
						p2 = Runtime.getRuntime().exec("cmd /c start /wait xcopy \""+steamCMD+"\\steamapps\\workshop\\content\\107410\\"+inputIdMod.getText()+"\" \""+inputDirMod.getText()+"\" /s/h/e/k/f/c/i/y");	
					}
					if (isLinux()) {
						p2 = Runtime.getRuntime().exec("sudo cp \""+steamCMD+"\\steamapps\\workshop\\content\\107410\\"+inputIdMod.getText()+"\" \""+inputDirMod.getText()+"\"");	
					}
				} catch (IOException e1) {
					mensajeError("ERROR", "STEAMCMD NO ENCONTRADO. Visita el apartado SteamCMD para configurar el SteamCMD");
				}
				try {
					p2.waitFor();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				p2.destroy();
				File fmod = new File(inputDirMod.getText());
				File[] matchingMod = fmod.listFiles(new FilenameFilter() {
					public boolean accept(File dir, String name) {
						return name.startsWith("meta") && name.endsWith(".cpp");
					}
				});
				ArrayList<String> bikeys = new ArrayList<String>();
				searchBikeyLastModify(bikeys,fmod);
				if (!bikeys.isEmpty()) {
					dirKeyMod.setText(Arrays.toString(bikeys.toArray()).substring(1, Arrays.toString(bikeys.toArray()).length()-1));
				}
				if (matchingMod != null && matchingMod.length > 0) {
					//Existe el archivo
					if (!inputNombreMod.getText().equals("")) {
						btnAgregarMod.setEnabled(true);
					}
					btnDescargarMod.setEnabled(false);
				}else {
					btnAgregarMod.setEnabled(false);
					if (!inputNombreMod.getText().equals("")) {
						btnDescargarMod.setEnabled(true);
					}else {
						btnDescargarMod.setEnabled(false);
					}
				}
				p2.destroy();

				break;

			default:
				System.out.println("Error en la seleccion de tipo de descarga");
				break;
			}
			downloadLabel.setText("");


		}
	} 
	public void execSteamCMD(String[] commands,String tipo){
		ThreadSteamCMD mr = new ThreadSteamCMD(commands,tipo);
		Thread t = new Thread(mr);
		t.setDaemon(true);
		t.start();
		try {
			t.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void execArma3Server(ArrayList<Mod> mods,ArrayList<Mod> modsOpcionales,Servidor sv, Mision m){
		ThreadServerArma3 sva3 = new ThreadServerArma3(mods,modsOpcionales,sv, m);
		Thread t = new Thread(sva3);
		t.setDaemon(true);
		t.start();
	}

	private void extraerDatosServidor (String datos) {
		String[] datosDivididos = datos.split("\\#");
		for (int i = 0; i < datosDivididos.length; i++) {
			String[] subDatosDivididos = datosDivididos[i].substring(1, datosDivididos[i].length()-1).split(";",44);
			Servidor s = new Servidor(subDatosDivididos[0], subDatosDivididos[1],subDatosDivididos[2],subDatosDivididos[3],Integer.parseInt(subDatosDivididos[4]),Boolean.parseBoolean(subDatosDivididos[5]),Boolean.parseBoolean(subDatosDivididos[6]),Boolean.parseBoolean(subDatosDivididos[7]),Boolean.parseBoolean(subDatosDivididos[8]),Boolean.parseBoolean(subDatosDivididos[9]),Boolean.parseBoolean(subDatosDivididos[10]),subDatosDivididos[11],
					subDatosDivididos[12],subDatosDivididos[13],subDatosDivididos[14],subDatosDivididos[15],subDatosDivididos[16],subDatosDivididos[17],subDatosDivididos[18],subDatosDivididos[19],subDatosDivididos[20],subDatosDivididos[21],subDatosDivididos[22],subDatosDivididos[23],subDatosDivididos[24],subDatosDivididos[25],subDatosDivididos[26],subDatosDivididos[27],subDatosDivididos[28],subDatosDivididos[29],subDatosDivididos[30],
					subDatosDivididos[31],subDatosDivididos[32],subDatosDivididos[33],subDatosDivididos[34],subDatosDivididos[35],subDatosDivididos[36],subDatosDivididos[37],subDatosDivididos[38],subDatosDivididos[39],subDatosDivididos[40],subDatosDivididos[41],subDatosDivididos[42],subDatosDivididos[43]);
			servidores.add(s);
			añadirServidorGUI(subDatosDivididos[0],listServidor.getModel().getSize());//
		}
	}

	static void limpiarCamposDespuesDeAgregarMod() {
		inputDirMod.setText("");
		getInputIdMod().setText("");
		dirKeyMod.setText("");
		inputNombreMod.setText("");
		btnAgregarMod.setEnabled(false);
	}

	private void extraerDatosMod(String datos) {
		String[] datosDivididos = datos.split("\\#");
		for (int i = 0; i < datosDivididos.length; i++) {
				String[] subDatosDivididos = datosDivididos[i].substring(1, datosDivididos[i].length()-1).split(";");
				ArrayList<String> bikeys = null;
				try {
					bikeys = new ArrayList<String>(Arrays.asList(subDatosDivididos[2].substring(0, subDatosDivididos[2].length()).split(",")));
				}catch (Exception e2) {
					bikeys = new ArrayList<String>();
				}
				Mod m = new Mod(subDatosDivididos[0], subDatosDivididos[1], bikeys,Integer.parseInt(subDatosDivididos[3]),false);
				limpiarCamposDespuesDeAgregarMod();
				String[] paquetesMod = null;
				try {
					paquetesMod = subDatosDivididos[4].substring(1, subDatosDivididos[4].length()-1).split(",");
				}catch (Exception e) {
					System.err.println(subDatosDivididos[4]);
					e.printStackTrace();
				}
				try {
					for (int j = 0; j < paquetesMod.length; j++) {
						try {
						String nombre = paquetesMod[j].substring(0,paquetesMod[j].indexOf("*"));
						String[] modulesPaquetes = paquetesMod[j].substring(paquetesMod[j].indexOf("*")+1,paquetesMod[j].indexOf("*",paquetesMod[j].indexOf("*")+1)).split("\\=");
						ArrayList<String> modulos = new ArrayList<String>();
						for (String string : modulesPaquetes) {
							modulos.add(string);
						}
						m.addElement(new Modulo(nombre, modulos));
						}catch (Exception e) {
							System.err.println("Error al cargar un modulo del mod "+m.getNombreMod()+" -> "+Arrays.toString(paquetesMod));
							e.printStackTrace();
						}
					}
					mods.add(m);
					añadirModGUI(subDatosDivididos[0],buscarModPos(m));
				}catch (Exception e) {
				}
		}

	}

	private int buscarModPos(Mod mod) {
		for (int i = 0; i < mods.size(); i++) {
			Mod modsLista = mods.get(i);
			if(modsLista.getNombreMod().equals(mod.getNombreMod())) {
				return i;
			}
		}
		return mods.size();
	}

	private void extraerDatosMision(String datos) {
		String[] datosDivididos = datos.split("\\#");
		for (int i = 0; i < datosDivididos.length; i++) {
			String[] subDatosDivididos = datosDivididos[i].substring(1, datosDivididos[i].length()-1).split(";");
			Mision m = new Mision(subDatosDivididos[0], subDatosDivididos[1]);
			System.out.println(m.getDirMision());
			boolean extraidoPBO =  extraerMisionPBO(m);
			if (extraidoPBO) {
				misiones.add(m);
				añadirMisionGUI(subDatosDivididos[0],listMision.getModel().getSize());
			}else {
				mensajeError("ERROR AL EXTRAER "+m.getNombreMision());
			}

		}

	}

	private void deseleccionarCheckBoxModsPerfil() {
		for (int i = 0; i < modelModPerfil.size(); i++) {
			modelModPerfil.getElementAt(i).setSelected(false);
		}
		listModsPerfil.repaint();

	}



	private void extraerDatosPerfil(String datos) {
		//[nombrePerfil_-_[Servidor s]_-_[Mod m1;Mod m2]_-_[Mision m]]#...
		String[] datosDivididos = datos.split("\\#"); //DIVIDE POR PERFILES
		Mision mision = null;
		ArrayList<Mod> listaMod = new ArrayList<Mod>();
		String nombrePerfil = null;
		Servidor s = null;
		for (int i = 0; i < datosDivididos.length; i++) {
			//MAIN
			String[] subDatosDivididos = datosDivididos[i].substring(1, datosDivididos[i].length()-1).split("_-_",4);
			nombrePerfil = subDatosDivididos[0];
			//SERVIDOR
			String[] subDatosDivididosServidor = subDatosDivididos[1].substring(1, subDatosDivididos[1].length()-1).split(";",11);
			s = new Servidor(subDatosDivididosServidor[0], subDatosDivididosServidor[1],subDatosDivididosServidor[2],subDatosDivididosServidor[3],Integer.parseInt(subDatosDivididosServidor[4]),Boolean.parseBoolean(subDatosDivididosServidor[5]),Boolean.parseBoolean(subDatosDivididosServidor[6]),Boolean.parseBoolean(subDatosDivididosServidor[7]),Boolean.parseBoolean(subDatosDivididosServidor[8]),Boolean.parseBoolean(subDatosDivididosServidor[9]),Boolean.parseBoolean(subDatosDivididosServidor[10]));
			//MODS
			String[] subDatosDivididosMod = subDatosDivididos[2].substring(0, subDatosDivididos[2].length()).split("&");
			for (int j = 0; j < subDatosDivididosMod.length; j++) {
				String[] subDatosDivididosModInterno = subDatosDivididosMod[j].substring(1, subDatosDivididosMod[j].length()-1).split(";");
				ArrayList<String> bikeys = null;
				try {
					bikeys = new ArrayList<String>(Arrays.asList(subDatosDivididosModInterno[2].substring(1, subDatosDivididosModInterno[2].length()-1).split(",")));
				}catch (Exception e2) {
					bikeys = new ArrayList<String>();
				}
				Mod m = new Mod(subDatosDivididosModInterno[0], subDatosDivididosModInterno[1], bikeys,Integer.parseInt(subDatosDivididosModInterno[3]),false);
				String[] paquetesMod = subDatosDivididosModInterno[4].substring(1, subDatosDivididosModInterno[4].length()-1).split(",");
				for (int k = 0; k < paquetesMod.length; k++) {
					String nombre = paquetesMod[j].substring(0,paquetesMod[j].indexOf("*")-1);
					String[] modulesPaquetes = paquetesMod[j].substring(paquetesMod[j].indexOf("*")+1,paquetesMod[j].indexOf("*",paquetesMod[j].indexOf("*")+1)).split("\\=");
					ArrayList<String> modulos = new ArrayList<String>();
					for (String string : modulesPaquetes) {
						modulos.add(string);
					}
					System.out.println(paquetesMod[j]);
					m.addElement(new Modulo(nombre, modulos));
				}
				listaMod.add(m);
			}
			//MISION
			String[] subDatosDivididosMision = subDatosDivididos[3].substring(1, subDatosDivididos[3].length()-1).split(";");
			mision = new Mision(subDatosDivididosMision[0], subDatosDivididosMision[1]);
			extraerMisionPBO(mision);
		}
		Perfil p = new Perfil(nombrePerfil, s, listaMod, mision);
		perfiles.add(p);
		modelPerfiles.addElement(new JCheckBox(p.getNombrePerfil()));

	}

	public static String listToString(String[] list) {
		String result = "+";
		for (int i = 0; i < list.length; i++) {
			result += " " + list[i];
		}
		return result;
	}
	private static boolean buscarServidor (String nombreServidor) {
		for (int i = 0; i < servidores.size(); i++) {
			Servidor servidor = servidores.get(i);
			if (servidor.getNombreServidor().equals(nombreServidor)) {
				return true;
			}
		}
		return false;
	}

	private static boolean buscarMision (String nombreMision) {
		for (int i = 0; i < misiones.size(); i++) {
			Mision mision = misiones.get(i);
			if (mision.getNombreMision().equals(nombreMision)) {
				return true;
			}
		}
		return false;
	}

	ArrayList<File> fileList = new ArrayList<File>();
	private void buscarModsPack(File file) {

		if (file.isDirectory()) {
			File[] arr = file.listFiles();
			for (File f : arr) {
				buscarModsPack(f);
			}
		} else {
			fileList.add(file);
		}
	}


	private void añadirServidor() throws ErrorDirectorio, ErrorNombre, ErrorPuerto {
		String nombreServidor = serverName.getText();
		String dirServidor = serverDir.getText();
		String contrasenaServidor = new String(serverPassword.getPassword());
		String nombrePublicoServidor = serverPublicName.getText();
		int puertoServidor = Integer.parseInt(serverPuerto.getText());
		boolean autoInit  = chckbxAutoinit.isSelected();
		boolean hugepages  = chckbxHugepages.isSelected();
		boolean nologs  = chckbxNologs.isSelected();
		boolean enableht  = chckbxNewEnableHT.isSelected();
		boolean filepatching  = chckbxFilepatching.isSelected();
		boolean nocb  = chckbxNocb.isSelected();
		if (nombreServidor.equals("")) {
			throw new ErrorNombre();
		}
		if (dirServidor.equals("")) {
			throw new ErrorDirectorio();
		}
		if (serverPuerto.getText().equals("")) {
			throw new ErrorPuerto();
		}
		//Añado servidor a servidores
		if (servidores.size() < 1) {
			Servidor servidor = new Servidor(nombreServidor, dirServidor,contrasenaServidor,nombrePublicoServidor,puertoServidor,autoInit,hugepages,nologs,enableht,filepatching,nocb);
			servidores.add(servidor);
		}else {
			if (buscarServidor(nombreServidor)) {
				throw new ErrorNombre();
			}else {
				Servidor servidor = new Servidor(nombreServidor, dirServidor,contrasenaServidor,nombrePublicoServidor,puertoServidor,autoInit,hugepages,nologs,enableht,filepatching,nocb);
				servidores.add(servidor);
			}
		}

		//Dibujo en la interfaz el servidor

		añadirServidorGUI(nombreServidor,listServidor.getModel().getSize());


	}

	public static void añadirIdMod(int id,String nombreMod) {
		inputIdMod.setText(""+id);
		inputNombreMod.setText(nombreMod);
	}

	static void añadirMod() throws ErrorDirectorio, ErrorNombre {
		String nombreMod = inputNombreMod.getText();
		String dirMod = inputDirMod.getText();
		String dirKey = dirKeyMod.getText();
		ArrayList<String> bikeys = new ArrayList<String>(Arrays.asList(dirKey.substring(0, dirKey.length()).split(",")));
		int idMod = Integer.parseInt(getInputIdMod().getText());
		if (nombreMod.equals("")) {
			throw new ErrorNombre();
		}
		if (dirMod.equals("")) {
			throw new ErrorDirectorio();
		}
		//Añado servidor a servidores

		if (mods.size() < 1) {
			Mod mod = new Mod(nombreMod, dirMod, bikeys,idMod,true);
			mod.leerPaquetes();
			mods.add(mod);
		}else {
			if (buscarMod(nombreMod)) {
				throw new ErrorNombre();
			}else {
				Mod mod = new Mod(nombreMod, dirMod, bikeys,idMod,true);
				mod.leerPaquetes();
				mods.add(mod);
			}
		}
		añadirModGUI(nombreMod,listMod.getModel().getSize());
	}

	private static boolean buscarMod(String nombreMod) {
		for (int i = 0; i < mods.size(); i++) {
			Mod mod = mods.get(i);
			if (mod.getNombreMod().equals(nombreMod)) {
				return true;
			}
		}
		return false;
	}

	public static Servidor buscarServidorPorNombre(String nombreServidor) {
		for (int i = 0; i < servidores.size(); i++) {
			Servidor sv = servidores.get(i);
			if (sv.getNombreServidor().equals(nombreServidor)) {
				return sv;
			}
		}
		return null;
	}

	public static Mod buscarModPorNombre(String nombreMod) {
		for (int i = 0; i < mods.size(); i++) {
			Mod mod = mods.get(i);
			if (mod.getNombreMod().equals(nombreMod)) {
				return mod;
			}
		}
		return null;
	}

	public static Mision buscarMisionPorNombre(String nombreMision) {
		for (int i = 0; i < misiones.size(); i++) {
			Mision mision  = misiones.get(i);
			if (mision.getNombreMision().equals(nombreMision)) {
				return mision;
			}
		}
		return null;
	}

	private Perfil buscarPerfilPorNombre(String nombrePerfil) {
		for (int i = 0; i < perfiles.size(); i++) {
			Perfil perfil  = perfiles.get(i);
			if (perfil.getNombrePerfil().equals(nombrePerfil)) {
				return perfil;
			}
		}
		return null;
	}

	static void searchBikeyLastModify(ArrayList<String> bikeys,File file) {
		if (file.isDirectory()) {
			File[] arr = file.listFiles();
			for (File f : arr) {
				searchBikeyLastModify(bikeys,f);
			}
		} else {
			if (file.getName().endsWith(".bikey")) {
				bikeys.add(file.getAbsolutePath());
			}
		}
	}

	private static void añadirServidorGUI(String nombreServidor,int pos) {
		modelServidor.add(pos, new JCheckBox(nombreServidor));
	}

	private static void añadirModGUI(String nombreMod,int pos) {
		modelMod.add(pos,new JCheckBox(nombreMod));
	}

	private void añadirMisionGUI(String nombreMision, int pos) {
		modelMision.add(pos, new JCheckBox(nombreMision));
	}

	@SuppressWarnings("unlikely-arg-type")
	public static void modificarServidor(Servidor sv, Servidor svModificado) throws ErrorNombre {
		for (int i = 0; i < servidores.size(); i++) {
			if (servidores.get(i).getNombreServidor().equals(sv.getNombreServidor())) {
				if (!buscarServidor(svModificado.getNombreServidor()) || sv.getNombreServidor().equals(svModificado.getNombreServidor())) {
					servidores.remove(i);
					servidores.add(i, svModificado);
					int posSv = 0;
					for (int j = 0; j < listServidor.getModel().getSize(); j++) {
						if (listServidor.getModel().getElementAt(j).equals(sv.getNombreServidor())) {
							posSv = j;
						}
					}
					((DefaultListModel<JCheckBox>) listServidor.getModel()).remove(posSv);
					añadirServidorGUI(svModificado.getNombreServidor(),posSv);
				}else {
					throw new ErrorNombre();
				}
			}
		}
	}

	public static void eliminarServidor(Servidor sv,int pos) {
		servidores.remove(sv);
		modelServidor.removeElementAt(pos);

	}

	public static boolean isNumeric(String str)  
	{  
		try  
		{  
			Double.parseDouble(str);  
		}  
		catch(NumberFormatException nfe)  
		{  
			return false;  
		}  
		return true;  
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		List<Image> icons = new ArrayList<Image>();
		icons.add(Toolkit.getDefaultToolkit().getImage(Interfaz.class.getResource("/images/icon.gif")));
		icons.add(Toolkit.getDefaultToolkit().getImage(Interfaz.class.getResource("/images/icon16x16.gif")));
		icons.add(Toolkit.getDefaultToolkit().getImage(Interfaz.class.getResource("/images/icon256x256.gif")));
		frmArmaAk = new JFrame();
		frmArmaAk.setIconImages(icons);
		frmArmaAk.setResizable(false);
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

			@Override
			public void run() {
				saveInfoToProperties();
			}
		}));
		//frmArmaAk.setIconImage(Toolkit.getDefaultToolkit().getImage(Interfaz.class.getResource("/images/icon.gif")));
		PopupMenu popup=new PopupMenu();
		MenuItem defaultItem=new MenuItem("Salir");
		ActionListener exitListener=new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (JOptionPane.showConfirmDialog(frmArmaAk, 
						"¿Seguro que quieres cerrar la aplicacion?", "¿Seguro que quieres cerrar la aplicacion?", 
						JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
					saveInfoToProperties();
					System.exit(0);
				}
			}
		};
		defaultItem.addActionListener(exitListener);
		popup.add(defaultItem);
		defaultItem=new MenuItem("Abrir");
		defaultItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmArmaAk.setVisible(true);
				frmArmaAk.setExtendedState(JFrame.NORMAL);
				LogGUI.frmLog.setVisible(true);
				LogGUI.frmLog.setExtendedState(JFrame.NORMAL);
			}
		});
		popup.add(defaultItem);


		if(SystemTray.isSupported()){
			trayIcon=new TrayIcon(Toolkit.getDefaultToolkit().getImage(Interfaz.class.getResource("/images/icon16x16.gif")), "AK ToolKit", popup);
			trayIcon.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					if (arg0.getClickCount() == 2) {
						frmArmaAk.setVisible(true);
						frmArmaAk.setExtendedState(JFrame.NORMAL);
						LogGUI.frmLog.setVisible(true);
						LogGUI.frmLog.setExtendedState(JFrame.NORMAL);
					}
				}

			});
			tray=SystemTray.getSystemTray();
			frmArmaAk.addWindowStateListener(new WindowStateListener() {
				public void windowStateChanged(WindowEvent e) {
					if(e.getNewState()==JFrame.ICONIFIED){
						try {
							tray.add(trayIcon);
							frmArmaAk.setVisible(false);
							LogGUI.frmLog.setVisible(false);
						} catch (AWTException ex) {
						}
					}
					if(e.getNewState()==7){
						try{
							tray.add(trayIcon);
							frmArmaAk.setVisible(false);
							LogGUI.frmLog.setVisible(false);
						}catch(AWTException ex){
						}
					}
					if(e.getNewState()==JFrame.MAXIMIZED_BOTH){
						tray.remove(trayIcon);
						frmArmaAk.setVisible(true);
					}
					if(e.getNewState()==JFrame.NORMAL){
						tray.remove(trayIcon);
						frmArmaAk.setVisible(true);
					}
				}
			});
		}
		frmArmaAk.setTitle("Arma 3 AK ToolKit");
		frmArmaAk.setBounds(100, 100, 1000, 600);
		frmArmaAk.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frmArmaAk.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				if (JOptionPane.showConfirmDialog(frmArmaAk, 
						"¿Seguro que quieres cerrar la aplicacion?", "¿Seguro que quieres cerrar la aplicacion?", 
						JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
					saveInfoToProperties();
					System.exit(0);
				}

			}
		});



		tabbedPane.setBorder(null);

		downloadPB = new JProgressBar();

		downloadLabel = new JLabel("");
		downloadLabel.setHorizontalAlignment(SwingConstants.CENTER);
		downloadLabel.setFont(new Font("Dialog", Font.PLAIN, 11));
		GroupLayout groupLayout = new GroupLayout(frmArmaAk.getContentPane());
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap()
						.addComponent(downloadPB, GroupLayout.DEFAULT_SIZE, 735, Short.MAX_VALUE)
						.addGap(21))
				.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap()
						.addComponent(downloadLabel, GroupLayout.DEFAULT_SIZE, 735, Short.MAX_VALUE)
						.addGap(21))
				.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
						.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, 884, Short.MAX_VALUE)
						.addContainerGap())
				);
		groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, 484, GroupLayout.PREFERRED_SIZE)
						.addGap(18)
						.addComponent(downloadLabel, GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(downloadPB, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
						.addContainerGap())
				);

		pnlServidores = new JPanel();
		tabbedPane.addTab("Servidores", new ImageIcon(Interfaz.class.getResource("/images/server-icon.gif")), pnlServidores, "");

		JPanel panelServidoresAñadidos = new JPanel();
		panelServidoresAñadidos.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Servidores", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));

		JPanel pnlAgregarServidor = new JPanel();
		pnlAgregarServidor.setBorder(new TitledBorder(null, "A\u00F1adir Servidor", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		JButton btnNewButton = new JButton("Iniciar Servidor");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				StartServer svgui = new StartServer(perfiles, servidores, mods, misiones,interfaz);
				svgui.frame.setVisible(true);
			}
		});

		final JButton btnModificarServidor = new JButton("Modificar Servidor");
		btnModificarServidor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String nombreServidor = null;
				try {
					nombreServidor = listServidor.getSelectedValue().getText();
				} catch (Exception e) {
					mensajeError("Servidor no seleccionado");
				}
				System.out.println("Modificando servidor "+nombreServidor);
				Servidor sv = buscarServidorPorNombre(nombreServidor);
				if (sv != null) {
					ModificarServidorGUI servidorGui = new ModificarServidorGUI(sv,listServidor.getSelectedIndex());
					servidorGui.frame.setVisible(true);
				}
			}
		});
		btnModificarServidor.setEnabled(false);
		GroupLayout gl_pnlServidores = new GroupLayout(pnlServidores);
		gl_pnlServidores.setHorizontalGroup(
				gl_pnlServidores.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnlServidores.createSequentialGroup()
						.addContainerGap()
						.addComponent(panelServidoresAñadidos, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_pnlServidores.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_pnlServidores.createSequentialGroup()
										.addGap(16)
										.addComponent(btnModificarServidor, GroupLayout.PREFERRED_SIZE, 266, GroupLayout.PREFERRED_SIZE)
										.addGap(27)
										.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 234, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_pnlServidores.createSequentialGroup()
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(pnlAgregarServidor, GroupLayout.PREFERRED_SIZE, 425, Short.MAX_VALUE)))
						.addContainerGap())
				);
		gl_pnlServidores.setVerticalGroup(
				gl_pnlServidores.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnlServidores.createSequentialGroup()
						.addGroup(gl_pnlServidores.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_pnlServidores.createSequentialGroup()
										.addComponent(pnlAgregarServidor, GroupLayout.PREFERRED_SIZE, 375, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addGroup(gl_pnlServidores.createParallelGroup(Alignment.BASELINE)
												.addComponent(btnModificarServidor, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
												.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)))
								.addComponent(panelServidoresAñadidos, GroupLayout.PREFERRED_SIZE, 437, GroupLayout.PREFERRED_SIZE))
						.addContainerGap(31, Short.MAX_VALUE))
				);


		GroupLayout gl_panelServidoresAñadidos = new GroupLayout(panelServidoresAñadidos);
		gl_panelServidoresAñadidos.setHorizontalGroup(
				gl_panelServidoresAñadidos.createParallelGroup(Alignment.LEADING)
				.addComponent(scrollServidor, GroupLayout.DEFAULT_SIZE, 349, Short.MAX_VALUE)
				);
		gl_panelServidoresAñadidos.setVerticalGroup(
				gl_panelServidoresAñadidos.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelServidoresAñadidos.createSequentialGroup()
						.addGap(2)
						.addComponent(scrollServidor, GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE))
				);
		panelServidoresAñadidos.setLayout(gl_panelServidoresAñadidos);

		JLabel lblNombre = new JLabel("Nombre:");

		btnAgregarServidor = new JButton("A\u00F1adir Servidor");
		btnAgregarServidor.setEnabled(false);

		btnDescargarServidor = new JButton("Descargar Servidor");

		serverName = new JTextField();
		serverName.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (serverName.getText().equals("")) {
					btnAgregarServidor.setEnabled(false);
					btnDescargarServidor.setEnabled(false);
				}else {
					File f = new File(serverDir.getText());
					File[] matchingFiles = f.listFiles(new FilenameFilter() {
						public boolean accept(File dir, String name) {
							return (name.startsWith("arma3server") && name.endsWith(".exe")) || (name.startsWith("arma3server_x64") && name.endsWith(".exe"));
						}
					});
					if (matchingFiles != null && matchingFiles.length > 0) {
						//Existe el archivo
						if (!serverName.getText().equals("") && !serverPuerto.getText().equals("")) {
							btnAgregarServidor.setEnabled(true);
							btnDescargarServidor.setEnabled(false);
						}
					}else {
						btnDescargarServidor.setEnabled(true);
						btnAgregarServidor.setEnabled(false);

					}
				}
			}
		});
		serverName.setColumns(10);

		JButton button = new JButton("");


		button.setIcon(new ImageIcon(Interfaz.class.getResource("/images/folder-icon.gif")));

		serverDir = new JTextField();
		serverDir.setColumns(10);

		JLabel lblDireccin = new JLabel("Direcci\u00F3n:");

		serverPassword = new JPasswordField();
		serverPassword.setEchoChar('*');
		serverPassword.setColumns(10);

		JLabel lblContrasea_1 = new JLabel("Contrase\u00F1a:");

		serverPublicName = new JTextField();
		serverPublicName.setColumns(10);

		JLabel lblNombrePbico = new JLabel("Nombre P\u00FAblico:");
		lblNombrePbico.setFont(new Font("Tahoma", Font.PLAIN, 10));


		btnDescargarServidor.setEnabled(false);


		serverPuerto = new JTextField();
		serverPuerto.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (serverPuerto.getText().equals("")) {
					btnAgregarServidor.setEnabled(false);
					btnDescargarServidor.setEnabled(false);
				}else {
					if (isNumeric(serverPuerto.getText())) {
						File f = new File(serverDir.getText());
						File[] matchingFiles = f.listFiles(new FilenameFilter() {
							public boolean accept(File dir, String name) {
								return (name.startsWith("arma3server") && name.endsWith(".exe")) || (name.startsWith("arma3server_x64") && name.endsWith(".exe"));
							}
						});
						if (matchingFiles != null && matchingFiles.length > 0) {
							//Existe el archivo
							if (!serverName.getText().equals("") && !serverPuerto.getText().equals("")) {
								btnAgregarServidor.setEnabled(true);
								btnDescargarServidor.setEnabled(false);
							}
						}else{
							if (!serverName.getText().equals("") && !serverPuerto.getText().equals("")) {
								btnAgregarServidor.setEnabled(false);
								btnDescargarServidor.setEnabled(true);
							}else {
								btnDescargarServidor.setEnabled(false);
								btnAgregarServidor.setEnabled(false);
							}


						}
					}else {
						mensajeInformacion("El puerto del servidor debe de ser un numero entero");
						serverPuerto.setText("");
					}

				}

			}
		});

		serverPuerto.setColumns(10);

		JLabel lblNewLabel = new JLabel("Puerto:");

		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(null, "Opciones Avanzadas", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		JLabel label_4 = new JLabel("*");

		JLabel label_5 = new JLabel("*");

		JLabel lblRellenar = new JLabel("* Obligatorio");

		final JCheckBox checkBoxPasswordServer = new JCheckBox("");
		checkBoxPasswordServer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (checkBoxPasswordServer.isSelected()) {
					serverPassword.setEchoChar((char)0);
				} else {
					serverPassword.setEchoChar('*');
				}
			}
		});
		
		JButton btnActualizarServidor = new JButton("Actualizar Servidor");
		btnActualizarServidor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String nombreServidor = listServidor.getSelectedValue().getText();
					Servidor servidor = buscarServidorPorNombre(nombreServidor);
					if (servidor != null) {
						String [] command = {"login \""+user+"\" \""+password+"\" \""+securityKey+"\"","force_install_dir \""+ servidor.getDirServidor()+"\"","app_update 233780 -beta validate"};
						execSteamCMD(command,"servidor");
					}else {
						System.err.println("Es necesario seleccionar un servidor");
					}
				}catch (Exception e) {
					System.err.println("Es necesario seleccionar un servidor");
				}
			}
		});
		btnActualizarServidor.setEnabled(true);
		GroupLayout gl_pnlAgregarServidor = new GroupLayout(pnlAgregarServidor);
		gl_pnlAgregarServidor.setHorizontalGroup(
			gl_pnlAgregarServidor.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnlAgregarServidor.createSequentialGroup()
					.addGroup(gl_pnlAgregarServidor.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_pnlAgregarServidor.createSequentialGroup()
							.addGap(26)
							.addGroup(gl_pnlAgregarServidor.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_pnlAgregarServidor.createSequentialGroup()
									.addGroup(gl_pnlAgregarServidor.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_pnlAgregarServidor.createSequentialGroup()
											.addGap(0)
											.addComponent(lblContrasea_1))
										.addGroup(gl_pnlAgregarServidor.createSequentialGroup()
											.addGap(18)
											.addComponent(lblDireccin, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE))
										.addGroup(gl_pnlAgregarServidor.createSequentialGroup()
											.addGap(29)
											.addComponent(lblNombre)))
									.addGap(7))
								.addGroup(gl_pnlAgregarServidor.createSequentialGroup()
									.addComponent(lblNewLabel)
									.addPreferredGap(ComponentPlacement.RELATED)))
							.addGroup(gl_pnlAgregarServidor.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_pnlAgregarServidor.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(serverName, GroupLayout.DEFAULT_SIZE, 424, Short.MAX_VALUE))
								.addComponent(serverDir, GroupLayout.DEFAULT_SIZE, 424, Short.MAX_VALUE)
								.addComponent(serverPassword, GroupLayout.DEFAULT_SIZE, 424, Short.MAX_VALUE)
								.addComponent(serverPuerto, GroupLayout.DEFAULT_SIZE, 424, Short.MAX_VALUE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(gl_pnlAgregarServidor.createParallelGroup(Alignment.LEADING)
								.addComponent(lblRellenar)
								.addGroup(gl_pnlAgregarServidor.createSequentialGroup()
									.addComponent(label_4, GroupLayout.PREFERRED_SIZE, 6, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(button, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
								.addComponent(label_5)
								.addComponent(checkBoxPasswordServer))
							.addGap(31))
						.addGroup(Alignment.LEADING, gl_pnlAgregarServidor.createSequentialGroup()
							.addContainerGap()
							.addComponent(btnAgregarServidor, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnDescargarServidor, GroupLayout.PREFERRED_SIZE, 187, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnActualizarServidor, GroupLayout.PREFERRED_SIZE, 187, GroupLayout.PREFERRED_SIZE)))
					.addGap(2))
				.addGroup(gl_pnlAgregarServidor.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel_3, GroupLayout.DEFAULT_SIZE, 611, Short.MAX_VALUE)
					.addContainerGap())
				.addGroup(gl_pnlAgregarServidor.createSequentialGroup()
					.addComponent(lblNombrePbico, GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(serverPublicName, GroupLayout.DEFAULT_SIZE, 437, Short.MAX_VALUE)
					.addGap(104))
		);
		gl_pnlAgregarServidor.setVerticalGroup(
			gl_pnlAgregarServidor.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_pnlAgregarServidor.createSequentialGroup()
					.addGap(14)
					.addGroup(gl_pnlAgregarServidor.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_pnlAgregarServidor.createSequentialGroup()
							.addGroup(gl_pnlAgregarServidor.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblNombre)
								.addComponent(lblRellenar))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(label_4, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
							.addGap(26))
						.addGroup(gl_pnlAgregarServidor.createSequentialGroup()
							.addComponent(serverName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_pnlAgregarServidor.createParallelGroup(Alignment.LEADING)
								.addComponent(button)
								.addGroup(gl_pnlAgregarServidor.createSequentialGroup()
									.addGroup(gl_pnlAgregarServidor.createParallelGroup(Alignment.BASELINE)
										.addComponent(serverDir, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(lblDireccin, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
									.addGap(23)))))
					.addGap(0)
					.addGroup(gl_pnlAgregarServidor.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNombrePbico)
						.addComponent(serverPublicName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(6)
					.addGroup(gl_pnlAgregarServidor.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_pnlAgregarServidor.createParallelGroup(Alignment.BASELINE)
							.addComponent(lblContrasea_1)
							.addComponent(serverPassword, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(checkBoxPasswordServer))
					.addGap(5)
					.addGroup(gl_pnlAgregarServidor.createParallelGroup(Alignment.BASELINE)
						.addComponent(serverPuerto, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel)
						.addComponent(label_5))
					.addGap(38)
					.addComponent(panel_3, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_pnlAgregarServidor.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_pnlAgregarServidor.createParallelGroup(Alignment.BASELINE)
							.addComponent(btnAgregarServidor, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
							.addGroup(gl_pnlAgregarServidor.createSequentialGroup()
								.addGap(1)
								.addComponent(btnDescargarServidor, GroupLayout.DEFAULT_SIZE, 43, Short.MAX_VALUE)))
						.addComponent(btnActualizarServidor, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE))
					.addGap(27))
		);

		chckbxAutoinit = new JCheckBox("autoInit");
		chckbxAutoinit.setToolTipText("Automatically initialize mission just like first client does.");

		chckbxNewEnableHT = new JCheckBox("enableHT");
		chckbxNewEnableHT.setToolTipText("Enables the use of all logical CPU cores for parallel tasks processing. If your CPU does not support Hyper-Threading or similar technology, this parameter is ignored.");

		chckbxHugepages = new JCheckBox("hugepages");
		chckbxHugepages.setToolTipText("Enables hugepages with the default memory allocator (malloc) for both client/server ");

		chckbxFilepatching = new JCheckBox("filePatching");
		chckbxFilepatching.setToolTipText("Allow the game to load unpacked data.");

		chckbxNologs = new JCheckBox("noLogs");
		chckbxNologs.setToolTipText("Be aware this means none errors saved to RPT file (report log). Yet in case of crash the fault address block info is saved.");

		chckbxNocb = new JCheckBox("noCB");
		chckbxNocb.setToolTipText("Turns off multicore use. It slows down rendering but may resolve visual glitches.");
		GroupLayout gl_panel_3 = new GroupLayout(panel_3);
		gl_panel_3.setHorizontalGroup(
				gl_panel_3.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_3.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
								.addComponent(chckbxAutoinit)
								.addComponent(chckbxNewEnableHT))
						.addGap(18)
						.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
								.addComponent(chckbxHugepages)
								.addComponent(chckbxFilepatching))
						.addGap(18)
						.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
								.addComponent(chckbxNologs)
								.addComponent(chckbxNocb))
						.addGap(167))
				);
		gl_panel_3.setVerticalGroup(
				gl_panel_3.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_3.createSequentialGroup()
						.addGroup(gl_panel_3.createParallelGroup(Alignment.BASELINE)
								.addComponent(chckbxAutoinit)
								.addComponent(chckbxHugepages)
								.addComponent(chckbxNologs))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel_3.createParallelGroup(Alignment.BASELINE)
								.addComponent(chckbxNewEnableHT)
								.addComponent(chckbxFilepatching)
								.addComponent(chckbxNocb))
						.addContainerGap(8, Short.MAX_VALUE))
				);
		panel_3.setLayout(gl_panel_3);
		pnlAgregarServidor.setLayout(gl_pnlAgregarServidor);
		pnlServidores.setLayout(gl_pnlServidores);

		JPanel pnlMods = new JPanel();
		tabbedPane.addTab("Mods", new ImageIcon(Interfaz.class.getResource("/images/mod_icon.gif")), pnlMods, null);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Mods", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "A\u00F1adir Mod", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GroupLayout gl_pnlMods = new GroupLayout(pnlMods);
		gl_pnlMods.setHorizontalGroup(
				gl_pnlMods.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, gl_pnlMods.createSequentialGroup()
						.addContainerGap()
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 448, Short.MAX_VALUE)
						.addContainerGap())
				);
		gl_pnlMods.setVerticalGroup(
				gl_pnlMods.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_pnlMods.createSequentialGroup()
						.addGroup(gl_pnlMods.createParallelGroup(Alignment.TRAILING)
								.addComponent(panel, GroupLayout.DEFAULT_SIZE, 427, Short.MAX_VALUE)
								.addComponent(panel_1, 0, 0, Short.MAX_VALUE))
						.addContainerGap())
				);

		inputNombreMod = new JTextField();

		inputNombreMod.setColumns(10);

		JLabel lblNombre_1 = new JLabel("Nombre:");

		JLabel lblDireccin_1 = new JLabel("Direcci\u00F3n:");

		inputDirMod = new JTextField();

		inputDirMod.setColumns(10);

		JButton button_1 = new JButton("");
		button_1.setBackground(Color.WHITE);

		button_1.setIcon(new ImageIcon(Interfaz.class.getResource("/images/folder-icon.gif")));
		btnAgregarMod = new JButton();
		btnAgregarMod.setText("Añadir Mod");
		btnAgregarMod.setEnabled(false);




		JLabel lblDirKey = new JLabel("Dir. Key:");

		dirKeyMod = new JTextField();
		dirKeyMod.setEditable(false);
		dirKeyMod.setColumns(10);

		btnDescargarMod = new JButton();
		btnDescargarMod.setText("Descargar Mod");
		btnDescargarMod.setEnabled(false);

		inputIdMod = new JTextField();
		getInputIdMod().addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				if (isNumeric(getInputIdMod().getText())) {
					File f = new File(inputDirMod.getText());
					File[] matchingFiles = f.listFiles(new FilenameFilter() {
						public boolean accept(File dir, String name) {
							return name.startsWith("meta") && name.endsWith(".cpp");
						}
					});
					if (matchingFiles != null && matchingFiles.length > 0 && !inputNombreMod.getText().equals("") && !inputDirMod.getText().equals("") && !getInputIdMod().getText().equals("")) {
						//Existe el archivo y los campos estan completados	
						btnAgregarMod.setEnabled(true);
						btnDescargarMod.setEnabled(false);
					}else {
						//No existe el archivo
						if (!inputNombreMod.getText().equals("") && !inputDirMod.getText().equals("") && !getInputIdMod().getText().equals("")) { //Los campos estan completados
							btnAgregarMod.setEnabled(false);
							btnDescargarMod.setEnabled(true);
						}else { //Los campos no estan completados
							btnAgregarMod.setEnabled(false);
							btnDescargarMod.setEnabled(false);
						}
					}
				}else {
					mensajeInformacion("El id de un mod solo puede tener numeros enteros");
					getInputIdMod().setText("");
				}
			}
		});
		getInputIdMod().setColumns(10);

		JLabel lblId = new JLabel("Id:");

		JLabel lblObligatorio = new JLabel("* Obligatorio");

		JButton button_2 = new JButton("");
		button_2.setBackground(Color.WHITE);

		button_2.setIcon(new ImageIcon(Interfaz.class.getResource("/images/search-icon.gif")));

		inputPackMods = new JTextField();
		inputPackMods.setColumns(10);

		JButton btnNewButton_1 = new JButton("");
		btnNewButton_1.setBackground(Color.WHITE);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new java.io.File("."));
				chooser.setDialogTitle("Directorio Mod");
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				chooser.setAcceptAllFileFilterUsed(false);

				if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					inputPackMods.setText(chooser.getSelectedFile().toString());					
				}
			}
		});
		btnNewButton_1.setIcon(new ImageIcon(Interfaz.class.getResource("/images/folder-icon.gif")));
		JButton btnNewButton_2 = new JButton("Cargar Carpeta de Mods");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						File file = new File(inputPackMods.getText());
						File[] files = file.listFiles(new FileFilter() {
							@Override
							public boolean accept(File f) {
								return f.isDirectory();
							}
						});
						int numberFiles = files.length;
						allFileNumber = numberFiles;
						downloadPB.setMaximum(allFileNumber);
						downloadLabel.setText("Mods: "+progressFile+"/"+allFileNumber);
						buscarModsPack(file);
						es.akademi.toolkit.akademitoolkit.Parallel.blockingFor(fileList, new Operation<File>(){
							public void perform(File file) {
								downloadLabel.setText("Mods: "+progressFile+"/"+allFileNumber);
								downloadPB.setValue(progressFile);
								if (file.getName().startsWith("meta") && file.getName().endsWith(".cpp")) {				
									String nombreMod = null;
									int id = 0;
									FileReader fileReader = null;
									try {
										fileReader = new FileReader(file);
									} catch (FileNotFoundException e) {
										mensajeInformacion("Error al leer archivo "+file.getPath());
									}

									// Always wrap FileReader in BufferedReader.
									BufferedReader bufferedReader = 
											new BufferedReader(fileReader);
									String line = null;
									try {
										while((line = bufferedReader.readLine()) != null) {
											if (line.contains("publishedid")) {
												id = Integer.parseInt(line.substring(line.indexOf("=")+2, line.length()-1));
											}
											if (line.contains("name")) {
												nombreMod = line.substring(line.indexOf("\"")+1,line.indexOf("\"", line.indexOf("\"")+1));
											}
										}
									} catch (NumberFormatException | IOException e) {
										e.printStackTrace();
										mensajeInformacion("Error al leer archivo "+file.getPath());
									}
									// Always close files.
									try {
										bufferedReader.close();
									} catch (IOException e) {
										mensajeInformacion("Error al leer archivo "+file.getPath());
									}
									ArrayList<String> bikeys = new ArrayList<String>();
									searchBikeyLastModify(bikeys,file.getParentFile());
									boolean existeElMod = buscarMod(nombreMod);
										if (!existeElMod) {
											Mod mod = new Mod(nombreMod, file.getParentFile().getAbsolutePath(), bikeys, id,true);
											System.out.println("Cargando "+nombreMod+" con keys "+Arrays.toString(bikeys.toArray()));
											mod.leerPaquetes();
											mods.add(mod);
											añadirModGUI(nombreMod, modelMod.size());
											System.out.println(nombreMod+" Cargado");
											progressFile = progressFile + 1;
										}
								}
								downloadLabel.setText("");
							};
						});
						System.out.println("Todos los mods cargados");
					}
				}).start();
			}
		});

		JLabel lblCarpetaDeMods = new JLabel("Carpeta De Mods:");

		final JButton btnModificarMod = new JButton("Modificar Mod");
		btnModificarMod.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String nombreMod = listMod.getSelectedValue().getText();
				Mod mod = buscarModPorNombre(nombreMod);
				if (mod != null) {
					ModificarModGUI servidorGui = new ModificarModGUI(mod,listMod.getSelectedIndex());
					servidorGui.frame.setVisible(true);
				}

			}
		});
		btnModificarMod.setEnabled(false);

		JLabel label_12 = new JLabel("*");

		JLabel label_13 = new JLabel("*");

		JLabel label_15 = new JLabel("*");

		JLabel lblCarpetaPorDefecto = new JLabel("Carpeta por defecto:");

		defaultModsFolder = new JTextField();
		defaultModsFolder.setColumns(10);

		JButton button_5 = new JButton("");
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new java.io.File("."));
				chooser.setDialogTitle("Directorio Mod");
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				chooser.setAcceptAllFileFilterUsed(false);

				if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					defaultModsFolder.setText(chooser.getSelectedFile().toString());	
					defaultFolderMods = chooser.getSelectedFile().toString();

				}
			}
		});
		button_5.setIcon(new ImageIcon(Interfaz.class.getResource("/images/folder-icon.gif")));
		button_5.setBackground(Color.WHITE);

		JButton btnNewButton_3 = new JButton("Añadir HTML");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Thread t = new Thread(new Runnable() {

					@Override
					public void run() {
						LoadModsPack frame = new LoadModsPack();
						frame.setVisible(true);
					}
				});
				t.start();
			}
		});
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_1.createSequentialGroup()
										.addContainerGap()
										.addComponent(lblCarpetaPorDefecto))
								.addGroup(gl_panel_1.createSequentialGroup()
										.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
												.addGroup(gl_panel_1.createSequentialGroup()
														.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
																.addComponent(lblId)
																.addComponent(lblNombre_1)
																.addComponent(lblDireccin_1))
														.addGap(10))
												.addGroup(gl_panel_1.createSequentialGroup()
														.addComponent(lblDirKey)
														.addPreferredGap(ComponentPlacement.UNRELATED)))
										.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING, false)
												.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
														.addGroup(gl_panel_1.createSequentialGroup()
																.addGap(1)
																.addComponent(dirKeyMod))
														.addComponent(inputDirMod)
														.addComponent(inputIdMod, GroupLayout.DEFAULT_SIZE, 304, Short.MAX_VALUE))
												.addComponent(inputNombreMod, GroupLayout.PREFERRED_SIZE, 304, GroupLayout.PREFERRED_SIZE))
										.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
												.addGroup(gl_panel_1.createSequentialGroup()
														.addPreferredGap(ComponentPlacement.RELATED)
														.addComponent(lblObligatorio))
												.addGroup(gl_panel_1.createSequentialGroup()
														.addGap(4)
														.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
																.addGroup(gl_panel_1.createSequentialGroup()
																		.addComponent(label_12, GroupLayout.PREFERRED_SIZE, 8, GroupLayout.PREFERRED_SIZE)
																		.addGap(18)
																		.addComponent(button_2, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE))
																.addComponent(label_15, GroupLayout.PREFERRED_SIZE, 8, GroupLayout.PREFERRED_SIZE)
																.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
																		.addComponent(button_5, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
																		.addGroup(gl_panel_1.createSequentialGroup()
																				.addComponent(label_13, GroupLayout.PREFERRED_SIZE, 8, GroupLayout.PREFERRED_SIZE)
																				.addGap(18)
																				.addComponent(button_1, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)))))))
								.addGroup(gl_panel_1.createSequentialGroup()
										.addContainerGap()
										.addComponent(defaultModsFolder, GroupLayout.PREFERRED_SIZE, 404, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel_1.createSequentialGroup()
										.addContainerGap()
										.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
												.addGroup(gl_panel_1.createSequentialGroup()
														.addComponent(inputPackMods, 480, 480, 480)
														.addPreferredGap(ComponentPlacement.RELATED)
														.addComponent(btnNewButton_1, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
												.addGroup(gl_panel_1.createSequentialGroup()
														.addComponent(btnAgregarMod, GroupLayout.PREFERRED_SIZE, 198, GroupLayout.PREFERRED_SIZE)
														.addPreferredGap(ComponentPlacement.RELATED)
														.addComponent(btnDescargarMod, GroupLayout.PREFERRED_SIZE, 212, GroupLayout.PREFERRED_SIZE)))))
						.addContainerGap())
				.addGroup(gl_panel_1.createSequentialGroup()
						.addContainerGap()
						.addComponent(btnModificarMod, GroupLayout.DEFAULT_SIZE, 416, Short.MAX_VALUE)
						.addGap(100))
				.addGroup(gl_panel_1.createSequentialGroup()
						.addContainerGap()
						.addComponent(lblCarpetaDeMods)
						.addContainerGap(429, Short.MAX_VALUE))
				.addGroup(gl_panel_1.createSequentialGroup()
						.addGap(142)
						.addComponent(btnNewButton_2)
						.addContainerGap(233, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING, gl_panel_1.createSequentialGroup()
						.addContainerGap(426, Short.MAX_VALUE)
						.addComponent(btnNewButton_3, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
						.addContainerGap())
				);
		gl_panel_1.setVerticalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblNombre_1)
								.addComponent(inputNombreMod, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblObligatorio))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
										.addComponent(inputIdMod, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(lblId)
										.addComponent(label_12))
								.addComponent(button_2, 0, 0, Short.MAX_VALUE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_1.createSequentialGroup()
										.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
												.addComponent(lblDireccin_1)
												.addComponent(inputDirMod, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(label_13))
										.addPreferredGap(ComponentPlacement.RELATED)
										.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
												.addComponent(dirKeyMod, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(lblDirKey)
												.addComponent(label_15)))
								.addComponent(button_1, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
						.addGap(40)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addComponent(button_5, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblCarpetaPorDefecto))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(defaultModsFolder, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING, false)
								.addComponent(btnAgregarMod, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnDescargarMod, GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(btnModificarMod, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(lblCarpetaDeMods)
						.addGap(1)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addComponent(inputPackMods, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnNewButton_1, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(btnNewButton_2, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(btnNewButton_3, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
				);
		panel_1.setLayout(gl_panel_1);

		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
				.addComponent(scrollMod, GroupLayout.DEFAULT_SIZE, 403, Short.MAX_VALUE)
				);
		gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
				.addComponent(scrollMod, GroupLayout.DEFAULT_SIZE, 235, Short.MAX_VALUE)
				);

		scrollMod.setViewportView(listMod);
		panel.setLayout(gl_panel);
		pnlMods.setLayout(gl_pnlMods);

		JPanel pnlMisiones = new JPanel();
		tabbedPane.addTab("Misiones", new ImageIcon(Interfaz.class.getResource("/images/file-icon.gif")), pnlMisiones, null);

		JPanel panel_17 = new JPanel();
		panel_17.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "A\u00F1adir Mision", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		JLabel label_9 = new JLabel("* Obligatorio");
		final JButton btnAgregarMision = new JButton("A\u00F1adir Mision");
		btnAgregarMision.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					agregarMision(inputNombreMision.getText(),inputDirMision.getText());
				} catch (ErrorNombre e) {
					mensajeInformacion("Error en el nombre de la mision.");
				} catch (ErrorDirectorio e) {
					mensajeInformacion("Error en el directorio de la mision.");
				}
			}

			private void agregarMision(String nombreMision, String dirMision) throws ErrorNombre, ErrorDirectorio {
				if (nombreMision.equals("") || buscarMision(nombreMision)) {
					throw new ErrorNombre();
				}
				downloadLabel.setText("DIR MISION: "+dirMision);
				if (dirMision.equals("")) {
					throw new ErrorDirectorio();
				}

				Mision m =  new Mision(nombreMision, dirMision);
				misiones.add(m);
				extraerMisionPBO(m);
				añadirMisionGUI(nombreMision, modelMision.getSize());
			}


		});
		btnAgregarMision.setEnabled(false);

		JLabel lblDir = new JLabel("Dir:");

		JLabel label_11 = new JLabel("Nombre:");

		inputDirMision = new JTextField();
		inputDirMision.setEditable(false);
		inputDirMision.setColumns(10);

		inputNombreMision = new JTextField();
		inputNombreMision.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				if (inputDirMision.getText().equals("") || inputNombreMision.getText().equals("")) {
					btnAgregarMision.setEnabled(false);
				}

				if (!inputDirMision.getText().equals("") && !inputNombreMision.getText().equals("")) {
					btnAgregarMision.setEnabled(true);
				}
			}
		});
		inputNombreMision.setColumns(10);

		JButton button_6 = new JButton("");
		button_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter(".pbo", "pbo", "archive");
				chooser.setFileFilter(filter);
				chooser.setCurrentDirectory(new java.io.File("."));
				chooser.setDialogTitle("Directorio Mod");
				chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				chooser.setAcceptAllFileFilterUsed(false);
				if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					inputDirMision.setText(chooser.getSelectedFile().toString());
					inputNombreMision.setText(chooser.getSelectedFile().getName().substring(0,chooser.getSelectedFile().getName().indexOf(".")));
				}

				if (inputDirMision.getText().equals("") || inputNombreMision.getText().equals("")) {
					btnAgregarMision.setEnabled(false);
				}

				if (!inputDirMision.getText().equals("") && !inputNombreMision.getText().equals("")) {
					btnAgregarMision.setEnabled(true);
				}
			}
		});
		button_6.setIcon(new ImageIcon(Interfaz.class.getResource("/images/folder-icon.gif")));

		JLabel label_14 = new JLabel("*");

		JLabel label_16 = new JLabel("*");

		JLabel lblCarpetaDeMisiones = new JLabel("Carpeta De Misiones:");

		inputPackMisiones = new JTextField();
		inputPackMisiones.setColumns(10);

		JButton button_3 = new JButton("");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new java.io.File("."));
				chooser.setDialogTitle("Directorio Mision");
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				chooser.setAcceptAllFileFilterUsed(false);

				if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					inputPackMisiones.setText(chooser.getSelectedFile().toString());					
				}

			}
		});
		button_3.setIcon(new ImageIcon(Interfaz.class.getResource("/images/folder-icon.gif")));

		JButton btnCargarCarpetaDe = new JButton("Cargar Carpeta de Misiones");
		btnCargarCarpetaDe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Thread(new Runnable() {
					@Override
					public void run() {
						// TODO Auto-generated method stub
						File file = new File(inputPackMisiones.getText());
						buscarMisionesPack(file);
					}
				}).start();

			}
			private void buscarMisionesPack(File file) {
				if (file.isDirectory()) {
					File[] arr = file.listFiles();
					for (File f : arr) {
						buscarMisionesPack(f);
					}
				} else {
					if (file.getName().endsWith(".pbo")) {
						String nombreMision = file.getName().substring(0, file.getName().indexOf("."));
						String dirMision = file.getAbsolutePath();
						boolean existeMision = buscarMision(nombreMision);
						if (!existeMision) {
							Mision mision = new Mision(nombreMision, dirMision);
							misiones.add(mision);
							extraerMisionPBO(mision);
							añadirMisionGUI(nombreMision, modelMision.getSize());
						}
					}
				}
			}
		});
		GroupLayout gl_panel_17 = new GroupLayout(panel_17);
		gl_panel_17.setHorizontalGroup(
				gl_panel_17.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_17.createSequentialGroup()
						.addGroup(gl_panel_17.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_panel_17.createSequentialGroup()
										.addGap(5)
										.addGroup(gl_panel_17.createParallelGroup(Alignment.TRAILING)
												.addComponent(lblDir)
												.addComponent(label_11))
										.addGap(10)
										.addGroup(gl_panel_17.createParallelGroup(Alignment.LEADING)
												.addGroup(gl_panel_17.createParallelGroup(Alignment.LEADING)
														.addGroup(gl_panel_17.createSequentialGroup()
																.addGroup(gl_panel_17.createParallelGroup(Alignment.LEADING, false)
																		.addComponent(inputDirMision)
																		.addComponent(inputNombreMision, GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE))
																.addPreferredGap(ComponentPlacement.RELATED)
																.addGroup(gl_panel_17.createParallelGroup(Alignment.LEADING)
																		.addGroup(gl_panel_17.createSequentialGroup()
																				.addComponent(label_16)
																				.addGap(12)
																				.addComponent(button_6, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE))
																		.addComponent(label_14, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)))
														.addComponent(label_9))
												.addComponent(btnAgregarMision, GroupLayout.PREFERRED_SIZE, 208, GroupLayout.PREFERRED_SIZE))
										.addGap(9))
								.addGroup(gl_panel_17.createSequentialGroup()
										.addContainerGap()
										.addComponent(inputPackMisiones, GroupLayout.DEFAULT_SIZE, 293, Short.MAX_VALUE)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(button_3, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap())
				.addGroup(gl_panel_17.createSequentialGroup()
						.addContainerGap(82, Short.MAX_VALUE)
						.addComponent(btnCargarCarpetaDe)
						.addGap(75))
				.addGroup(Alignment.LEADING, gl_panel_17.createSequentialGroup()
						.addContainerGap()
						.addComponent(lblCarpetaDeMisiones)
						.addContainerGap(206, Short.MAX_VALUE))
				);
		gl_panel_17.setVerticalGroup(
				gl_panel_17.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_17.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panel_17.createParallelGroup(Alignment.BASELINE)
								.addComponent(label_11)
								.addComponent(inputNombreMision, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(label_14))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel_17.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_17.createSequentialGroup()
										.addGroup(gl_panel_17.createParallelGroup(Alignment.BASELINE)
												.addComponent(inputDirMision, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(lblDir))
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(label_9)
										.addGap(18)
										.addComponent(btnAgregarMision, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addGap(80)
										.addComponent(lblCarpetaDeMisiones))
								.addComponent(label_16)
								.addComponent(button_6, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel_17.createParallelGroup(Alignment.LEADING)
								.addComponent(button_3, 0, 0, Short.MAX_VALUE)
								.addComponent(inputPackMisiones, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(btnCargarCarpetaDe, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
						.addGap(46))
				);
		panel_17.setLayout(gl_panel_17);

		JPanel panel_16 = new JPanel();
		panel_16.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Misiones", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));


		GroupLayout gl_panel_16 = new GroupLayout(panel_16);
		gl_panel_16.setHorizontalGroup(
				gl_panel_16.createParallelGroup(Alignment.LEADING)
				.addGap(0, 382, Short.MAX_VALUE)
				.addComponent(scrollMision, GroupLayout.DEFAULT_SIZE, 370, Short.MAX_VALUE)
				);
		gl_panel_16.setVerticalGroup(
				gl_panel_16.createParallelGroup(Alignment.LEADING)
				.addGap(0, 415, Short.MAX_VALUE)
				.addComponent(scrollMision, GroupLayout.DEFAULT_SIZE, 392, Short.MAX_VALUE)
				);


		panel_16.setLayout(gl_panel_16);
		GroupLayout gl_pnlMisiones = new GroupLayout(pnlMisiones);
		gl_pnlMisiones.setHorizontalGroup(
				gl_pnlMisiones.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnlMisiones.createSequentialGroup()
						.addContainerGap()
						.addComponent(panel_16, GroupLayout.PREFERRED_SIZE, 337, GroupLayout.PREFERRED_SIZE)
						.addGap(18)
						.addComponent(panel_17, GroupLayout.PREFERRED_SIZE, 360, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(26, Short.MAX_VALUE))
				);
		gl_pnlMisiones.setVerticalGroup(
				gl_pnlMisiones.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_pnlMisiones.createSequentialGroup()
						.addGroup(gl_pnlMisiones.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(panel_16, 0, 0, Short.MAX_VALUE)
								.addGroup(gl_pnlMisiones.createSequentialGroup()
										.addContainerGap()
										.addComponent(panel_17, GroupLayout.PREFERRED_SIZE, 403, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap())
				);
		pnlMisiones.setLayout(gl_pnlMisiones);





		JPanel panel_12 = new JPanel();
		tabbedPane.addTab("Perfiles", new ImageIcon(Interfaz.class.getResource("/images/perfil-icon.gif")), panel_12, null);

		JPanel panel_13 = new JPanel();
		panel_13.setBorder(new TitledBorder(null, "Mods", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		JScrollPane scrollPaneModsPerfil = new JScrollPane((Component) null);
		GroupLayout gl_panel_13 = new GroupLayout(panel_13);
		gl_panel_13.setHorizontalGroup(
				gl_panel_13.createParallelGroup(Alignment.LEADING)
				.addGap(0, 344, Short.MAX_VALUE)
				.addComponent(scrollPaneModsPerfil, GroupLayout.DEFAULT_SIZE, 332, Short.MAX_VALUE)
				);
		gl_panel_13.setVerticalGroup(
				gl_panel_13.createParallelGroup(Alignment.LEADING)
				.addGap(0, 301, Short.MAX_VALUE)
				.addComponent(scrollPaneModsPerfil, GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE)
				);


		scrollPaneModsPerfil.setViewportView(listModsPerfil);
		panel_13.setLayout(gl_panel_13);

		JPanel panel_14 = new JPanel();
		panel_14.setLayout(null);
		panel_14.setBorder(new TitledBorder(null, "Servidor", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		String[] arrayServidores = new String[servidores.size()];
		for(int i = 0; i < arrayServidores.length; i++) {
			arrayServidores[i] = servidores.get(i).toString();
		}

		ComboBoxModel<Object> s = new DefaultComboBoxModel<Object>(arrayServidores);
		final JComboBox<Object> servidorComboBox = new JComboBox<Object>(s);
		servidorComboBox.setBounds(10, 45, 166, 31);
		panel_14.add(servidorComboBox);

		JPanel panel_15 = new JPanel();
		panel_15.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Agregar Perfil", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));

		JLabel label_10 = new JLabel("Nombre: *");

		inputPerfilName = new JTextField();
		inputPerfilName.setColumns(10);

		JButton btnAgregarPerfil = new JButton("Agregar");

		GroupLayout gl_panel_15 = new GroupLayout(panel_15);
		gl_panel_15.setHorizontalGroup(
				gl_panel_15.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_15.createSequentialGroup()
						.addGroup(gl_panel_15.createParallelGroup(Alignment.LEADING)
								.addComponent(label_10)
								.addComponent(inputPerfilName, GroupLayout.DEFAULT_SIZE, 128, Short.MAX_VALUE)
								.addGroup(gl_panel_15.createSequentialGroup()
										.addGap(20)
										.addComponent(btnAgregarPerfil, GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap())
				);
		gl_panel_15.setVerticalGroup(
				gl_panel_15.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_15.createSequentialGroup()
						.addComponent(label_10)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(inputPerfilName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(btnAgregarPerfil, GroupLayout.PREFERRED_SIZE, 24, Short.MAX_VALUE)
						.addContainerGap())
				);
		panel_15.setLayout(gl_panel_15);

		JPanel panel_18 = new JPanel();
		panel_18.setBorder(new TitledBorder(null, "Misiones", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		JScrollPane scrollPaneMisionesPerfil = new JScrollPane((Component) null);
		GroupLayout gl_panel_18 = new GroupLayout(panel_18);
		gl_panel_18.setHorizontalGroup(
				gl_panel_18.createParallelGroup(Alignment.TRAILING)
				.addGap(0, 367, Short.MAX_VALUE)
				.addComponent(scrollPaneMisionesPerfil, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 355, Short.MAX_VALUE)
				);
		gl_panel_18.setVerticalGroup(
				gl_panel_18.createParallelGroup(Alignment.TRAILING)
				.addGap(0, 301, Short.MAX_VALUE)
				.addComponent(scrollPaneMisionesPerfil, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE)
				);
		listMisionesPerfil.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				deseleccionarCheckBoxModsPerfil();
				if (!listMisionesPerfil.isSelectionEmpty()) {
					String nombreMision = listMisionesPerfil.getSelectedValue().getText();
					Mision mision = Interfaz.buscarMisionPorNombre(nombreMision);
					ArrayList<String> modulosMision = mision.getModulosNecesarios();
					ArrayList<Mod> modsNecesarios = new ArrayList<Mod>();
					boolean moduloEncontrado = false;
					Mod mod = null;
					for (String moduloMision : modulosMision) {
						moduloEncontrado = false;
						//MODULOS DE LA MISION
						for (int i = 0; i < modelModPerfil.getSize(); i++) { //MODS EN LA LISTA
							JCheckBox ckbmod = modelModPerfil.getElementAt(i);
							mod = Interfaz.buscarModPorNombre(ckbmod.getText());
							ArrayList<Modulo> modulosMod = mod.getModulos();
							for (Modulo moduloMod : modulosMod) { //MODULOS DEL MOD
								if (moduloMision.equals(moduloMod.getNombre())) {
									for (String string : moduloMod.getdModulos()) {
										for (Mod mod2 : mods) {
											for (Modulo string2 : mod2.getModulos()) {
												if(string2.getNombre().equals(string)) {
													modsNecesarios.add(mod2);
													break;
												}
											}
										}
									}
									ckbmod.setSelected(true);
									listModsPerfil.repaint();
									moduloEncontrado = true;
									System.out.println("Modulo "+moduloMision+" encontrado");
									continue;
								}
							}

						}
						if (!moduloEncontrado) {
							String nombreModNoEncontrado = moduloMision.substring(0, moduloMision.indexOf("_")).toUpperCase();
							System.out.println("Mod "+nombreModNoEncontrado+" no encontrado y añadido para descargar");
						}

						for (Mod mod3 : modsNecesarios) {
							for (int i = 0; i < modelModPerfil.getSize(); i++) {
								if(mod3.getNombreMod().equals(modelModPerfil.get(i).getText())) {
									modelModPerfil.get(i).setSelected(true);
									listModsPerfil.repaint();
								}
							}
						}
					}
				}
			}
		});


		scrollPaneMisionesPerfil.setViewportView(listMisionesPerfil);
		panel_18.setLayout(gl_panel_18);

		JPanel panel_19 = new JPanel();
		panel_19.setBorder(new TitledBorder(null, "Perfiles", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		JScrollPane scrollPane_2 = new JScrollPane();
		GroupLayout gl_panel_19 = new GroupLayout(panel_19);
		gl_panel_19.setHorizontalGroup(
				gl_panel_19.createParallelGroup(Alignment.LEADING)
				.addGap(0, 247, Short.MAX_VALUE)
				.addComponent(scrollPane_2, GroupLayout.DEFAULT_SIZE, 235, Short.MAX_VALUE)
				);
		gl_panel_19.setVerticalGroup(
				gl_panel_19.createParallelGroup(Alignment.LEADING)
				.addGap(0, 112, Short.MAX_VALUE)
				.addComponent(scrollPane_2, GroupLayout.DEFAULT_SIZE, 89, Short.MAX_VALUE)
				);


		scrollPane_2.setViewportView(listPerfiles);
		panel_19.setLayout(gl_panel_19);

		JButton button_4 = new JButton("Modificar");
		GroupLayout gl_panel_12 = new GroupLayout(panel_12);
		gl_panel_12.setHorizontalGroup(
				gl_panel_12.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_12.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panel_12.createParallelGroup(Alignment.LEADING)
								.addComponent(panel_13, GroupLayout.DEFAULT_SIZE, 344, Short.MAX_VALUE)
								.addGroup(gl_panel_12.createSequentialGroup()
										.addComponent(panel_14, GroupLayout.PREFERRED_SIZE, 188, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(panel_15, GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)))
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(gl_panel_12.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_panel_12.createSequentialGroup()
										.addComponent(panel_19, GroupLayout.DEFAULT_SIZE, 251, Short.MAX_VALUE)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(button_4, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
								.addComponent(panel_18, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 367, GroupLayout.PREFERRED_SIZE))
						.addContainerGap(24, Short.MAX_VALUE))
				);
		gl_panel_12.setVerticalGroup(
				gl_panel_12.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_12.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panel_12.createParallelGroup(Alignment.LEADING)
								.addComponent(button_4, GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)
								.addGroup(gl_panel_12.createParallelGroup(Alignment.BASELINE)
										.addComponent(panel_14, GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)
										.addComponent(panel_15, GroupLayout.PREFERRED_SIZE, 112, Short.MAX_VALUE)
										.addComponent(panel_19, GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel_12.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(panel_18, 0, 0, Short.MAX_VALUE)
								.addComponent(panel_13, GroupLayout.DEFAULT_SIZE, 301, Short.MAX_VALUE))
						.addContainerGap())
				);
		panel_12.setLayout(gl_panel_12);
		JPanel pnlSteamCMD = new JPanel();
		tabbedPane.addTab("SteamCMD", new ImageIcon(Interfaz.class.getResource("/images/Steam_icon.gif")), pnlSteamCMD, null);
		pnlSteamCMD.setBorder(new TitledBorder(null, "SteamCMD", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		steamCMDTF = new JTextField();
		steamCMDTF.setToolTipText("Carpeta SteamCMD");
		steamCMDTF.setHorizontalAlignment(SwingConstants.LEFT);
		steamCMDTF.setColumns(10);

		JButton steamCMDOpenFolderBtn = new JButton("");

		steamCMDOpenFolderBtn.setIcon(new ImageIcon(Interfaz.class.getResource("/images/folder-icon.gif")));

		final JButton btnDescargarSteamcmd = new JButton("Descargar SteamCMD");
		btnDescargarSteamcmd.setEnabled(false);



		JPanel pnlEstadoSteamCMD = new JPanel();
		pnlEstadoSteamCMD.setBorder(new TitledBorder(null, "Estado", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		txtSteamcmdStatus = new JTextField();
		txtSteamcmdStatus.setHorizontalAlignment(SwingConstants.CENTER);
		txtSteamcmdStatus.setText("STEAMCMD NO ENCONTRADO");
		txtSteamcmdStatus.setEditable(false);
		txtSteamcmdStatus.setColumns(10);
		GroupLayout gl_pnlEstadoSteamCMD = new GroupLayout(pnlEstadoSteamCMD);
		gl_pnlEstadoSteamCMD.setHorizontalGroup(
				gl_pnlEstadoSteamCMD.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_pnlEstadoSteamCMD.createSequentialGroup()
						.addContainerGap()
						.addComponent(txtSteamcmdStatus, GroupLayout.DEFAULT_SIZE, 194, Short.MAX_VALUE)
						.addContainerGap())
				);
		gl_pnlEstadoSteamCMD.setVerticalGroup(
				gl_pnlEstadoSteamCMD.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, gl_pnlEstadoSteamCMD.createSequentialGroup()
						.addGap(25)
						.addComponent(txtSteamcmdStatus, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(26, Short.MAX_VALUE))
				);
		pnlEstadoSteamCMD.setLayout(gl_pnlEstadoSteamCMD);

		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "Usuario", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		JLabel label = new JLabel("Usuario:");

		JLabel label_1 = new JLabel("Contrase\u00F1a:");

		JLabel label_2 = new JLabel("Seguridad:");

		inputUser = new JTextField();
		inputUser.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				user = inputUser.getText();
			}
		});
		inputUser.setColumns(10);

		inputPassword = new JPasswordField();
		inputPassword.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				password = inputPassword.getPassword().toString();
			}
		});
		inputPassword.setEchoChar('*');

		inputKey = new JPasswordField();
		inputKey.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				securityKey = new String(inputKey.getPassword());
			}
		});
		inputKey.setEchoChar('*');

		final JCheckBox checkBoxUserPassword = new JCheckBox("");
		checkBoxUserPassword.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (checkBoxUserPassword.isSelected()) {
					inputPassword.setEchoChar((char)0);
				} else {
					inputPassword.setEchoChar('*');
				}
			}
		});

		final JCheckBox checkBoxUserKey = new JCheckBox("");
		checkBoxUserKey.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (checkBoxUserKey.isSelected()) {
					inputKey.setEchoChar((char)0);
				} else {
					inputKey.setEchoChar('*');
				}
			}
		});
		inputDirMod.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				File f = new File(inputDirMod.getText());
				File[] matchingFiles = f.listFiles(new FilenameFilter() {
					public boolean accept(File dir, String name) {
						return name.startsWith("meta") && name.endsWith(".cpp");
					}
				});
				if (matchingFiles != null && matchingFiles.length > 0 && !inputNombreMod.getText().equals("") && !inputDirMod.getText().equals("") && !getInputIdMod().getText().equals("")) {
					//Existe el archivo y los campos estan completados	
					btnAgregarMod.setEnabled(true);
					btnDescargarMod.setEnabled(false);
				}else {
					//No existe el archivo
					if (!inputNombreMod.getText().equals("") && !inputDirMod.getText().equals("") && !getInputIdMod().getText().equals("")) { //Los campos estan completados
						btnAgregarMod.setEnabled(false);
						btnDescargarMod.setEnabled(true);
					}else { //Los campos no estan completados
						btnAgregarMod.setEnabled(false);
						btnDescargarMod.setEnabled(false);
					}
				}
			}
		});

		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File f2 = new File("config.properties");
				try {
					Interfaz.nohide(f2);
				} catch (InterruptedException e2) {
					e2.printStackTrace();
				} catch (IOException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				user = inputUser.getText();
				password = new String(inputPassword.getPassword());
				securityKey = new String(inputKey.getPassword());
				steamCMD = steamCMDTF.getText();
				try {
					output = new FileOutputStream("config.properties");
					prop.setProperty("dirSteamCMD", steamCMDTF.getText());
					prop.setProperty("steamCMDStatus", txtSteamcmdStatus.getText());
					prop.setProperty("user", inputUser.getText());
					/***PASSWORD***/
					prop.setProperty("password", new String(inputPassword.getPassword()));
					/***KEY***/
					prop.setProperty("securityKey", new String(inputKey.getPassword()));
					// save properties to project root folder
					prop.store(output, "ToolKit AK");

				} catch (IOException io) {
					mensajeInformacion("Error al intentar abrir el archivo 'config.properties'");
				} finally {
					if (output != null) {
						try {
							output.close();
							File f3 = new File("config.properties");
							try {
								Interfaz.hide(f3);
							} catch (InterruptedException e2) {
								e2.printStackTrace();
							} catch (IOException e2) {
								// TODO Auto-generated catch block
								e2.printStackTrace();
							}
						} catch (IOException e2) {
							mensajeInformacion("Error al intentar cerrar el archivo 'config.properties'");
						}
					}

				}
				mensajeInformacion("Datos almacenados correctamente");
			}
		});
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
				gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
						.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_2.createSequentialGroup()
										.addContainerGap()
										.addGroup(gl_panel_2.createParallelGroup(Alignment.TRAILING)
												.addComponent(label)
												.addComponent(label_1)
												.addComponent(label_2))
										.addPreferredGap(ComponentPlacement.RELATED)
										.addGroup(gl_panel_2.createParallelGroup(Alignment.TRAILING, false)
												.addComponent(inputUser, GroupLayout.DEFAULT_SIZE, 226, Short.MAX_VALUE)
												.addGroup(gl_panel_2.createSequentialGroup()
														.addGroup(gl_panel_2.createParallelGroup(Alignment.TRAILING)
																.addComponent(inputKey, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 199, Short.MAX_VALUE)
																.addComponent(inputPassword, GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE))
														.addPreferredGap(ComponentPlacement.UNRELATED)
														.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
																.addComponent(checkBoxUserKey, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
																.addComponent(checkBoxUserPassword)))))
								.addGroup(gl_panel_2.createSequentialGroup()
										.addGap(101)
										.addComponent(btnGuardar, GroupLayout.PREFERRED_SIZE, 101, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				);
		gl_panel_2.setVerticalGroup(
				gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
								.addComponent(label)
								.addComponent(inputUser, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
								.addComponent(label_1)
								.addComponent(inputPassword, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(checkBoxUserPassword))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
								.addComponent(label_2)
								.addComponent(inputKey, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(checkBoxUserKey, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
						.addGap(18)
						.addComponent(btnGuardar, GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
						.addContainerGap())
				);
		panel_2.setLayout(gl_panel_2);
		GroupLayout gl_pnlSteamCMD = new GroupLayout(pnlSteamCMD);
		gl_pnlSteamCMD.setHorizontalGroup(
				gl_pnlSteamCMD.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnlSteamCMD.createSequentialGroup()
						.addGroup(gl_pnlSteamCMD.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_pnlSteamCMD.createSequentialGroup()
										.addGap(110)
										.addComponent(btnDescargarSteamcmd, GroupLayout.PREFERRED_SIZE, 176, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_pnlSteamCMD.createSequentialGroup()
										.addGap(10)
										.addComponent(steamCMDTF, GroupLayout.PREFERRED_SIZE, 335, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(steamCMDOpenFolderBtn)))
						.addGap(28)
						.addComponent(pnlEstadoSteamCMD, GroupLayout.PREFERRED_SIZE, 284, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(48, Short.MAX_VALUE))
				.addGroup(gl_pnlSteamCMD.createSequentialGroup()
						.addContainerGap()
						.addComponent(panel_2, 0, 0, Short.MAX_VALUE)
						.addGap(394))
				);
		gl_pnlSteamCMD.setVerticalGroup(
				gl_pnlSteamCMD.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnlSteamCMD.createSequentialGroup()
						.addGroup(gl_pnlSteamCMD.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_pnlSteamCMD.createSequentialGroup()
										.addGap(7)
										.addGroup(gl_pnlSteamCMD.createParallelGroup(Alignment.TRAILING, false)
												.addComponent(steamCMDOpenFolderBtn, Alignment.LEADING, 0, 0, Short.MAX_VALUE)
												.addComponent(steamCMDTF, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 25, Short.MAX_VALUE))
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(btnDescargarSteamcmd, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
										.addGap(35)
										.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 198, GroupLayout.PREFERRED_SIZE))
								.addComponent(pnlEstadoSteamCMD, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE))
						.addContainerGap(92, Short.MAX_VALUE))
				);
		pnlSteamCMD.setLayout(gl_pnlSteamCMD);

		JPanel panel_4 = new JPanel();
		tabbedPane.addTab("Ayuda", new ImageIcon(Interfaz.class.getResource("/images/ask-question.gif")), panel_4, null);

		JTabbedPane tabbedPane_1 = new JTabbedPane(JTabbedPane.TOP);
		GroupLayout gl_panel_4 = new GroupLayout(panel_4);
		gl_panel_4.setHorizontalGroup(
				gl_panel_4.createParallelGroup(Alignment.LEADING)
				.addComponent(tabbedPane_1, GroupLayout.DEFAULT_SIZE, 755, Short.MAX_VALUE)
				);
		gl_panel_4.setVerticalGroup(
				gl_panel_4.createParallelGroup(Alignment.LEADING)
				.addComponent(tabbedPane_1, GroupLayout.DEFAULT_SIZE, 435, Short.MAX_VALUE)
				);

		JPanel panel_5 = new JPanel();
		tabbedPane_1.addTab("Servidores", null, panel_5, null);

		JTextPane txtpnParaAadirUn = new JTextPane();
		txtpnParaAadirUn.setBackground(SystemColor.control);
		txtpnParaAadirUn.setText("Para añadir un servidor es necesario:\r\n1. Añadir nombre del servidor.  (Este nombre sera solo visible en la aplicación)\r\n2. Añadir la carpeta del servidor.\r\n3. Añadir el puerto del servidor.\r\nOpcionalmente se pueden añadir:\r\n1. Contraseña del servidor.\r\n2. Nombre del servidor que será público en Arma 3.\r\n3. Opciones avanzadas. Estas opciones son avanzadas y solo deberán ser usadas por profesionales... vamos que cualquiera puede usarlas (si algo explota no es culpa mia).\r\nSi el servidor no esta añadido, se podrá descargar utilizando SteamCMD (para ello es necesario haber añadido el usuario y contraseña en el apartado \"SteamCMD\").\r\nUna vez descargado, se podrá añadir a la base de datos de la aplicación, para así en un futuro, poder iniciar una mision en ese servidor.");
		txtpnParaAadirUn.setEditable(false);
		GroupLayout gl_panel_5 = new GroupLayout(panel_5);
		gl_panel_5.setHorizontalGroup(
				gl_panel_5.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_5.createSequentialGroup()
						.addContainerGap()
						.addComponent(txtpnParaAadirUn, GroupLayout.DEFAULT_SIZE, 731, Short.MAX_VALUE)
						.addContainerGap())
				);
		gl_panel_5.setVerticalGroup(
				gl_panel_5.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_5.createSequentialGroup()
						.addGap(5)
						.addComponent(txtpnParaAadirUn, GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
						.addContainerGap())
				);
		panel_5.setLayout(gl_panel_5);

		JPanel panel_6 = new JPanel();
		tabbedPane_1.addTab("Mods", null, panel_6, null);

		JTextPane textPane = new JTextPane();
		textPane.setEditable(false);
		textPane.setBackground(SystemColor.control);
		GroupLayout gl_panel_6 = new GroupLayout(panel_6);
		gl_panel_6.setHorizontalGroup(
				gl_panel_6.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_6.createSequentialGroup()
						.addContainerGap()
						.addComponent(textPane, GroupLayout.DEFAULT_SIZE, 736, Short.MAX_VALUE)
						.addContainerGap())
				);
		gl_panel_6.setVerticalGroup(
				gl_panel_6.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_6.createSequentialGroup()
						.addContainerGap()
						.addComponent(textPane, GroupLayout.DEFAULT_SIZE, 388, Short.MAX_VALUE)
						.addContainerGap())
				);
		panel_6.setLayout(gl_panel_6);

		JPanel panel_7 = new JPanel();
		tabbedPane_1.addTab("Misiones", null, panel_7, null);

		JTextPane textPane_1 = new JTextPane();
		textPane_1.setEditable(false);
		textPane_1.setBackground(SystemColor.menu);
		GroupLayout gl_panel_7 = new GroupLayout(panel_7);
		gl_panel_7.setHorizontalGroup(
				gl_panel_7.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_7.createSequentialGroup()
						.addContainerGap()
						.addComponent(textPane_1, GroupLayout.PREFERRED_SIZE, 736, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				);
		gl_panel_7.setVerticalGroup(
				gl_panel_7.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_7.createSequentialGroup()
						.addContainerGap()
						.addComponent(textPane_1, GroupLayout.PREFERRED_SIZE, 388, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				);
		panel_7.setLayout(gl_panel_7);

		JPanel panel_8 = new JPanel();
		tabbedPane_1.addTab("Perfiles", null, panel_8, null);

		JTextPane textPane_2 = new JTextPane();
		textPane_2.setEditable(false);
		textPane_2.setBackground(SystemColor.menu);
		GroupLayout gl_panel_8 = new GroupLayout(panel_8);
		gl_panel_8.setHorizontalGroup(
				gl_panel_8.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_8.createSequentialGroup()
						.addContainerGap()
						.addComponent(textPane_2, GroupLayout.PREFERRED_SIZE, 736, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				);
		gl_panel_8.setVerticalGroup(
				gl_panel_8.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_8.createSequentialGroup()
						.addContainerGap()
						.addComponent(textPane_2, GroupLayout.PREFERRED_SIZE, 388, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				);
		panel_8.setLayout(gl_panel_8);

		JPanel panel_9 = new JPanel();
		tabbedPane_1.addTab("SteamCMD", null, panel_9, null);

		JTextPane textPane_3 = new JTextPane();
		textPane_3.setEditable(false);
		textPane_3.setBackground(SystemColor.menu);
		GroupLayout gl_panel_9 = new GroupLayout(panel_9);
		gl_panel_9.setHorizontalGroup(
				gl_panel_9.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_9.createSequentialGroup()
						.addContainerGap()
						.addComponent(textPane_3, GroupLayout.PREFERRED_SIZE, 736, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				);
		gl_panel_9.setVerticalGroup(
				gl_panel_9.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_9.createSequentialGroup()
						.addContainerGap()
						.addComponent(textPane_3, GroupLayout.PREFERRED_SIZE, 388, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				);
		panel_9.setLayout(gl_panel_9);

		JPanel panel_10 = new JPanel();
		tabbedPane_1.addTab("Informacion Adicional", null, panel_10, null);

		JTextPane textPane_4 = new JTextPane();
		textPane_4.setEditable(false);
		textPane_4.setBackground(SystemColor.menu);
		GroupLayout gl_panel_10 = new GroupLayout(panel_10);
		gl_panel_10.setHorizontalGroup(
				gl_panel_10.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_10.createSequentialGroup()
						.addContainerGap()
						.addComponent(textPane_4, GroupLayout.PREFERRED_SIZE, 736, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				);
		gl_panel_10.setVerticalGroup(
				gl_panel_10.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_10.createSequentialGroup()
						.addContainerGap()
						.addComponent(textPane_4, GroupLayout.PREFERRED_SIZE, 388, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				);
		panel_10.setLayout(gl_panel_10);

		JPanel panel_11 = new JPanel();
		tabbedPane_1.addTab("Creditos", null, panel_11, null);

		JTextPane textPane_5 = new JTextPane();
		textPane_5.setEditable(false);
		textPane_5.setBackground(SystemColor.menu);
		GroupLayout gl_panel_11 = new GroupLayout(panel_11);
		gl_panel_11.setHorizontalGroup(
				gl_panel_11.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_11.createSequentialGroup()
						.addContainerGap()
						.addComponent(textPane_5, GroupLayout.PREFERRED_SIZE, 736, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				);
		gl_panel_11.setVerticalGroup(
				gl_panel_11.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_11.createSequentialGroup()
						.addContainerGap()
						.addComponent(textPane_5, GroupLayout.PREFERRED_SIZE, 388, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				);
		panel_11.setLayout(gl_panel_11);
		panel_4.setLayout(gl_panel_4);

		JPanel panel_20 = new JPanel();
		tabbedPane.addTab("Log", new ImageIcon(Interfaz.class.getResource("/images/icon.gif")), panel_20, null);

		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new SearchMod(inputNombreMod.getText(),true);
			}
		});


		//STATUS STEAM
		// your directory
		//ACTIONS
		//STEAM CMD FOLDER SEARCH
		steamCMDOpenFolderBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new java.io.File("."));
				chooser.setDialogTitle("Directorio STEAMCMD");
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				chooser.setAcceptAllFileFilterUsed(false);

				if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					steamCMDTF.setText(chooser.getSelectedFile().toString());
					//STATUS STEAM
					// your directory
					File f = new File(chooser.getSelectedFile().toString());
					File[] matchingFiles = f.listFiles(new FilenameFilter() {
						public boolean accept(File dir, String name) {
							return name.startsWith("steamcmd") && (name.endsWith(".exe") || name.endsWith(".sh")) ;
						}
					});
					if (matchingFiles != null && matchingFiles.length > 0) {
						//Existe el archivo
						txtSteamcmdStatus.setText("STEAMCMD ENCONTRADO");
						btnDescargarSteamcmd.setEnabled(false);
						btnDescargarSteamcmd.setToolTipText("STEAMCMD ya ha sido descargado");
					}else {
						txtSteamcmdStatus.setText("STEAMCMD NO ENCONTRADO");
						btnDescargarSteamcmd.setEnabled(true);
						btnDescargarSteamcmd.setToolTipText("");
					}
				}
			}
		});
		//STEAMCMD DOWNLOAD
		btnDescargarSteamcmd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					descargarSteamCMD(steamCMDTF.getText());
				} catch (ErrorDirectorioSteamCMD e) {
					mensajeInformacion("Debe seleccionar un directorio antes de descargar SteamCMD");
				} catch (IOException e) {
					mensajeInformacion("Error al intentar descargar SteamCMD");
				}
			}
		});
		File f = new File(steamCMDTF.getText());
		frmArmaAk.getContentPane().setLayout(groupLayout);
		File[] matchingFiles = f.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.startsWith("steamcmd") && name.endsWith(".exe");
			}
		});
		if (matchingFiles != null && matchingFiles.length > 0) {
			//Existe el archivo
			txtSteamcmdStatus.setText("STEAMCMD ENCONTRADO");
		}else {
			txtSteamcmdStatus.setText("STEAMCMD NO ENCONTRADO");
		}
		/*************SERVIDOR***************/
		//Añadir Servidor
		btnAgregarServidor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					añadirServidor();
					serverName.setText("");
					serverDir.setText("");
					serverPassword.setText("");
					serverPublicName.setText("");
					serverPuerto.setText("");
					btnDescargarServidor.setEnabled(false);
					btnAgregarServidor.setEnabled(false);
				} catch (ErrorDirectorio e) {
					mensajeInformacion("Error al intentar agregar el servidor:\nEl directorio del servidor no es el correcto");
				} catch (ErrorNombre e) {
					mensajeInformacion("Error al intentar agregar el servidor:\nEl nombre del servidor ya existe o esta en blanco");
				} catch (ErrorPuerto e) {
					mensajeInformacion("Error al intentar agregar el servidor:\nEl puerto del servidor no es el correcto");
				}
			}
		});
		//Btn BUSCAR DIRECTORIO SERVIDOR
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new java.io.File("."));
				chooser.setDialogTitle("Directorio Servidor");
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				chooser.setAcceptAllFileFilterUsed(false);

				if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					serverDir.setText(chooser.getSelectedFile().toString());
					//STATUS STEAM
					// your directory
					File f = new File(chooser.getSelectedFile().toString());
					File[] matchingFiles = f.listFiles(new FilenameFilter() {
						public boolean accept(File dir, String name) {
							return (name.startsWith("arma3server") && name.endsWith(".exe")) || (name.startsWith("arma3server_x64") && name.endsWith(".exe"));
						}
					});
					if (matchingFiles != null && matchingFiles.length > 0) {
						//Existe el archivo
						if (!serverName.getText().equals("") && !serverPuerto.getText().equals("")) {
							btnAgregarServidor.setEnabled(true);
						}
						mensajeInformacion("Servidor Encontrado en la ruta: " + chooser.getSelectedFile().toString());
						btnDescargarServidor.setEnabled(false);
					}else {
						btnAgregarServidor.setEnabled(false);
						if (!serverName.getText().equals("") && !serverPuerto.getText().equals("")) {
							btnDescargarServidor.setEnabled(true);
						}else {
							btnDescargarServidor.setEnabled(false);
						}
						mensajeInformacion("Servidor no Encontrado en la ruta: " + chooser.getSelectedFile().toString());
					}
				}
			}
		});
		//VER INFORMACION SERVIDOR
		listServidor.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				/*
				if (list.getSelectedValue() != null && arg0.getClickCount() == 1) {
					String nombreServidor = list.getSelectedValue().getText().substring(0, list.getSelectedValue().getText().indexOf("(")-3);
					Servidor sv = buscarServidorPorNombre(nombreServidor);
					ThreadServerArma3 st = new ThreadServerArma3(new Mod[]{}, sv);
					Thread t = new Thread(st);
					t.start();
				}
				 */
				if (!listServidor.isSelectionEmpty()) {
					btnModificarServidor.setEnabled(true);
				}else {
					btnModificarServidor.setEnabled(false);
				}

			}

		});
		//Descargar Servidor
		btnDescargarServidor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!estadoSteam()) {
					mensajeInformacion("Rellene la informacion de usuario del STEAMCMD");
				}else {
					String [] command = {"login \""+user+"\" \""+password+"\" \""+securityKey+"\"","force_install_dir \""+ serverDir.getText()+"\"","app_update 233780 -beta validate"};
					execSteamCMD(command,"servidor");
				}
			}

		});
		
		//Actualizar Servidor
		btnDescargarServidor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!estadoSteam()) {
					mensajeInformacion("Rellene la informacion de usuario del STEAMCMD");
				}else {
					

					String [] command = {"login \""+user+"\" \""+password+"\" \""+securityKey+"\"","force_install_dir \""+ serverDir.getText()+"\"","app_update 233780 -beta validate"};
					execSteamCMD(command,"servidor");
				}
			}

		});
		/******************MODS********************/
		//BTN Dir Mod
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new java.io.File(defaultFolderMods));
				chooser.setDialogTitle("Directorio Mod");
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				chooser.setAcceptAllFileFilterUsed(false);

				if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					inputDirMod.setText(chooser.getSelectedFile().toString());
					//STATUS STEAM
					// your directory
					File f = new File(chooser.getSelectedFile().toString());
					File[] matchingFiles = f.listFiles(new FilenameFilter() {
						public boolean accept(File dir, String name) {
							return name.startsWith("meta") && name.endsWith(".cpp");
						}
					});
					if (matchingFiles != null && matchingFiles.length > 0) {
						//Existe el archivo
						if (!inputNombreMod.getText().equals("") && !inputNombreMod.getText().equals("") && !inputDirMod.getText().equals("") && !getInputIdMod().getText().equals("")) {
							btnAgregarMod.setEnabled(true);
							btnDescargarMod.setEnabled(false);
						}else {
							btnAgregarMod.setEnabled(false);
							btnDescargarMod.setEnabled(false);
						}
						mensajeInformacion("Mod Encontrado en la ruta: " + chooser.getSelectedFile().toString());
						buscarModsPack(f);
						es.akademi.toolkit.akademitoolkit.Parallel.For(fileList, new Operation<File>(){
							public void perform(File file) {
								downloadLabel.setText("Mods: "+progressFile+"/"+allFileNumber);
								downloadPB.setValue(progressFile);
								if (file.getName().startsWith("meta") && file.getName().endsWith(".cpp")) {				
									String nombreMod = null;
									int id = 0;
									FileReader fileReader = null;
									try {
										fileReader = new FileReader(file);
									} catch (FileNotFoundException e) {
										mensajeInformacion("Error al leer archivo "+file.getPath());
									}

									// Always wrap FileReader in BufferedReader.
									BufferedReader bufferedReader = 
											new BufferedReader(fileReader);
									String line = null;
									try {
										while((line = bufferedReader.readLine()) != null) {
											if (line.contains("publishedid")) {
												id = Integer.parseInt(line.substring(line.indexOf("=")+2, line.length()-1));
											}
											if (line.contains("name")) {
												nombreMod = line.substring(line.indexOf("\"")+1,line.indexOf("\"", line.indexOf("\"")+1));
											}
										}
									} catch (NumberFormatException | IOException e) {
										e.printStackTrace();
										mensajeInformacion("Error al leer archivo "+file.getPath());
									}
									// Always close files.
									try {
										bufferedReader.close();
									} catch (IOException e) {
										mensajeInformacion("Error al leer archivo "+file.getPath());
									}
									ArrayList<String> bikeys = new ArrayList<String>();
									searchBikeyLastModify(bikeys,file.getParentFile());
									boolean existeElMod = buscarMod(nombreMod);
									if (!existeElMod) {
										inputNombreMod.setText(nombreMod);
										inputDirMod.setText(file.getParentFile().getAbsolutePath());
										getInputIdMod().setText(""+id);
										dirKeyMod.setText(Arrays.toString(bikeys.toArray()).substring(1, Arrays.toString(bikeys.toArray()).length()-1));
										bikeys.clear();
										progressFile = progressFile + 1;
									}
								}
								downloadLabel.setText("");
							};
						});
						btnAgregarMod.setEnabled(true);
					}else {
						mensajeInformacion("Mod no Encontrado en la ruta: " + chooser.getSelectedFile().toString());
						if (!inputNombreMod.getText().equals("") && !inputDirMod.getText().equals("") && !getInputIdMod().getText().equals("")) {
							btnAgregarMod.setEnabled(false);
							btnDescargarMod.setEnabled(true);
						}else {
							btnAgregarMod.setEnabled(false);
							btnDescargarMod.setEnabled(false);
						}


					}
				}

			}
		});

		//MODIFICAR MOD
		listMod.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (!listMod.isSelectionEmpty()) {
					btnModificarMod.setEnabled(true);
				}else {
					btnModificarMod.setEnabled(false);
				}
			}
		});

		//NAME MOD
		inputNombreMod.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				File f = new File(inputDirMod.getText());
				File[] matchingFiles = f.listFiles(new FilenameFilter() {
					public boolean accept(File dir, String name) {
						return name.startsWith("meta") && name.endsWith(".cpp");
					}
				});
				if (matchingFiles != null && matchingFiles.length > 0 && !inputNombreMod.getText().equals("") && !inputDirMod.getText().equals("") && !getInputIdMod().getText().equals("")) {
					//Existe el archivo y los campos estan completados	
					btnAgregarMod.setEnabled(true);
					btnDescargarMod.setEnabled(false);
				}else {
					//No existe el archivo
					if (!inputNombreMod.getText().equals("") && (!inputDirMod.getText().equals("") || !defaultModsFolder.getText().equals("")) && !getInputIdMod().getText().equals("")) { //Los campos estan completados
						btnAgregarMod.setEnabled(false);
						btnDescargarMod.setEnabled(true);
					}else { //Los campos no estan completados
						btnAgregarMod.setEnabled(false);
						btnDescargarMod.setEnabled(false);
					}
				}
			}
		});
		//BTN Agregar Mod
		btnAgregarMod.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					instalandoMods = true;
					añadirMod();
					limpiarCamposDespuesDeAgregarMod();
					instalandoMods = false;
				} catch (ErrorDirectorio e) {
					mensajeInformacion("Error al intentar agregar el servidor:\nEl directorio del servidor no es el correcto");
				} catch (ErrorNombre e) {
					mensajeInformacion("Error al intentar agregar el servidor:\nEl nombre del servidor ya existe o esta en blanco");
				}
			}
		});



		//Descargar MOD

		btnDescargarMod.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!estadoSteam()) {
					mensajeInformacion("Rellene la informacion de usuario del STEAMCMD");
				}else {
					update();
					if(inputDirMod.getText().equals("") && defaultModsFolder.getText().equals("")) {
						System.out.println("Error direccion de guardado");
						return;
					}
					descargandoMods = true;
					if(inputDirMod.getText().equals("") && !defaultModsFolder.getText().equals("")) {
						inputDirMod.setText(defaultModsFolder.getText()+File.separator+inputNombreMod.getText());
					}
					String [] command = {"login \""+user+"\" \""+password+"\" \""+securityKey+"\"","workshop_download_item 107410 "+ getInputIdMod().getText()+" validate"};
					execSteamCMD(command,"mod");
					update();
					btnAgregarMod.setEnabled(true);
					btnAgregarMod.doClick();
					LogGUI.update();
					descargandoMods = false;
				}
			}
		});
		//PERFILES

		//ACTUALIZAR TAB PERFILES AL ENTRAR
		tabbedPane.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				if (tabbedPane.getSelectedIndex() == 3) {
					servidorComboBox.removeAllItems();
					modelModPerfil.removeAllElements();
					modelMisionPerfil.removeAllElements();


					for (Servidor sv : servidores) {
						servidorComboBox.addItem(sv.toString());
					}

					for (Mod mod : mods) {
						modelModPerfil.addElement(new JCheckBox(mod.getNombreMod()));
					}

					for (Mision mision : misiones) {
						modelMisionPerfil.addElement(new JCheckBox(mision.getNombreMision()));
					}
				}

				if (tabbedPane.getSelectedIndex() == 6) {
					tabbedPane.setSelectedIndex(0);
					if (!LogGUI.frmLog.isVisible()) {
						LogGUI.frmLog.setVisible(true);
					}
				}
			}
		});
		//AGREGAR PERFIL
		btnAgregarPerfil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {


				if (inputPerfilName.getText().equals("") || buscarPerfilPorNombre(inputPerfilName.getText()) != null) {
					mensajeInformacion("Error");
				}else {
					ArrayList<Mod> modsPerfil = new ArrayList<Mod>();
					for(int i = 0; i< listModsPerfil.getModel().getSize();i++){
						JCheckBox check = listModsPerfil.getModel().getElementAt(i);
						if (check.isSelected()) {
							Mod m = buscarModPorNombre(check.getText());
							modsPerfil.add(m);
						}
					}
					Mision mision = null;
					for(int i = 0; i< listMisionesPerfil.getModel().getSize();i++){
						JCheckBox check = listMisionesPerfil.getModel().getElementAt(i);
						if (check.isSelected()) {
							mision = buscarMisionPorNombre(check.getText());
						}
					}

					Servidor s = buscarServidorPorNombre((String) servidorComboBox.getSelectedItem());
					Perfil p = new Perfil(inputPerfilName.getText(), s, modsPerfil, mision);
					perfiles.add(p);
					modelPerfiles.addElement(new JCheckBox(p.getNombrePerfil()));
				}
			}
		});
		//CARGO LOS DATOS GUARDADOS
		try {
			try {
				nohide(new File(System.getProperty("user.dir")+"/config.properties"));
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			input = new FileInputStream("config.properties");
			if (input != null) {
				prop.load(input);
				//Cargamos el directorio de SteamCMD
				version = prop.getProperty("version");
				steamCMDTF.setText(prop.getProperty("dirSteamCMD"));
				steamCMD = prop.getProperty("dirSteamCMD");
				if (steamCMD != null) {
					if (steamCMD.equals("")) {
						txtSteamcmdStatus.setText("STEAMCMD NO ENCONTRADO");
					}else {
						//STATUS STEAM
						// your directory
						File[] encontrado = buscarSteamCMD();
						if (encontrado != null && encontrado.length > 0) {
							//Existe el archivo
							txtSteamcmdStatus.setText("STEAMCMD ENCONTRADO");
							btnDescargarSteamcmd.setEnabled(false);
							btnDescargarSteamcmd.setToolTipText("STEAMCMD ya ha sido descargado");
						}else {
							txtSteamcmdStatus.setText("STEAMCMD NO ENCONTRADO");
							btnDescargarSteamcmd.setEnabled(true);
							steamCMDTF.setText("");
							btnDescargarSteamcmd.setToolTipText("Para descargar SteamCMD debe seleccionar un directorio");
						}
					}
				}
				//Usuario
				user = prop.getProperty("user");
				inputUser.setText(user);
				//Contraseña
				password = prop.getProperty("password");
				inputPassword.setText(password);
				//SecurityKey
				securityKey = prop.getProperty("securityKey");
				inputKey.setText(securityKey);
				//Servidores
				if (prop.getProperty("servidores") != null) {
					if (!prop.getProperty("servidores").equals("")) {
						extraerDatosServidor(prop.getProperty("servidores"));
					}
				}

				//Mods
				if (prop.getProperty("mods") != null) {
					if (!prop.getProperty("mods").equals("")) {
						mensajeInformacion("Cargando Mods");
						extraerDatosMod(prop.getProperty("mods"));
					}
				}
				//Misiones
				if (prop.getProperty("misiones") != null) {
					if (!prop.getProperty("misiones").equals("")) {
						mensajeInformacion("Cargando Misiones");
						extraerDatosMision(prop.getProperty("misiones"));
					}
				}
				//Perfiles
				if (prop.getProperty("perfiles") != null) {
					if (!prop.getProperty("perfiles").equals("")) {
						mensajeInformacion("Cargando Perfiles");
						extraerDatosPerfil(prop.getProperty("perfiles"));
					}
				}
				if (prop.getProperty("defaultFolderMods") != null) {
					defaultFolderMods = prop.getProperty("defaultFolderMods");
					defaultModsFolder.setText(defaultFolderMods);
				}
			}
		} catch (IOException e1) {
			saveInfoToProperties();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private File[] buscarSteamCMD() {
		File archivo = new File(steamCMD);
		File[] encontrado = archivo.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.startsWith("steamcmd") && (name.endsWith(".exe") || name.endsWith(".sh"));
			}
		});
		return encontrado;
	}

	//GETTERS Y SETTERS


	public JFrame getFrmArmaAk() {
		return frmArmaAk;
	}

	public void setFrmArmaAk(JFrame frmArmaAk) {
		Interfaz.frmArmaAk = frmArmaAk;
	}

	public JTextField getSteamCMDTF() {
		return steamCMDTF;
	}

	public void setSteamCMDTF(JTextField steamCMDTF) {
		Interfaz.steamCMDTF = steamCMDTF;
	}

	public static JTextField getTxtSteamcmdStatus() {
		return txtSteamcmdStatus;
	}

	public void setTxtSteamcmdStatus(JTextField txtSteamcmdStatus) {
		Interfaz.txtSteamcmdStatus = txtSteamcmdStatus;
	}

	public JTextField getServerName() {
		return serverName;
	}

	public void setServerName(JTextField serverName) {
		Interfaz.serverName = serverName;
	}

	public JTextField getServerDir() {
		return serverDir;
	}

	public void setServerDir(JTextField serverDir) {
		Interfaz.serverDir = serverDir;
	}

	public Properties getProp() {
		return prop;
	}

	public void setProp(Properties prop) {
		Interfaz.prop = prop;
	}

	public InputStream getInput() {
		return input;
	}

	public void setInput(InputStream input) {
		Interfaz.input = input;
	}

	public OutputStream getOutput() {
		return output;
	}

	public void setOutput(OutputStream output) {
		Interfaz.output = output;
	}

	public static DefaultListModel<JCheckBox> getModel() {
		return modelServidor;
	}

	public static void setModel(DefaultListModel<JCheckBox> model) {
		Interfaz.modelServidor = model;
	}

	public static JList<JCheckBox> getList() {
		return listServidor;
	}

	public static void setList(JCheckBoxListSingle list) {
		Interfaz.listServidor = list;
	}

	public JScrollPane getListaServidores() {
		return scrollServidor;
	}

	public void setListaServidores(JScrollPane listaServidores) {
		this.scrollServidor = listaServidores;
	}

	public static DefaultListModel<JCheckBox> getModel2() {
		return modelMod;
	}

	public static void setModel2(DefaultListModel<JCheckBox> model2) {
		Interfaz.modelMod = model2;
	}

	public static JList<JCheckBox> getList2() {
		return listMod;
	}

	public static void setList2(JCheckBoxListSingle list2) {
		Interfaz.listMod = list2;
	}

	public JScrollPane getListaMods() {
		return scrollMod;
	}

	public void setListaMods(JScrollPane listaMods) {
		this.scrollMod = listaMods;
	}

	public String getSteamCMD() {
		return steamCMD;
	}

	public void setSteamCMD(String steamCMD) {
		Interfaz.steamCMD = steamCMD;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		Interfaz.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		Interfaz.password = password;
	}

	public String getSecurityKey() {
		return securityKey;
	}

	public void setSecurityKey(String securityKey) {
		Interfaz.securityKey = securityKey;
	}

	public static ArrayList<Servidor> getServidores() {
		return servidores;
	}

	public static void setServidores(ArrayList<Servidor> servidores) {
		Interfaz.servidores = servidores;
	}

	public ArrayList<Mod> getMods() {
		return mods;
	}

	public void setMods(ArrayList<Mod> mods) {
		Interfaz.mods = mods;
	}

	public ArrayList<Mision> getMisiones() {
		return misiones;
	}

	public void setMisiones(ArrayList<Mision> misiones) {
		Interfaz.misiones = misiones;
	}

	public JTextField getInputNombreMod() {
		return inputNombreMod;
	}

	public void setInputNombreMod(JTextField inputNombreMod) {
		Interfaz.inputNombreMod = inputNombreMod;
	}

	public JTextField getInputDirMod() {
		return inputDirMod;
	}

	public void setInputDirMod(JTextField inputDirMod) {
		Interfaz.inputDirMod = inputDirMod;
	}

	public JTextField getServerPassword() {
		return serverPassword;
	}

	public void setServerPassword(JPasswordField serverPassword) {
		this.serverPassword = serverPassword;
	}

	public JTextField getServerPublicName() {
		return serverPublicName;
	}

	public void setServerPublicName(JTextField serverPublicName) {
		this.serverPublicName = serverPublicName;
	}

	public JTextField getDirKeyMod() {
		return dirKeyMod;
	}

	public void setDirKeyMod(JTextField dirKeyMod) {
		Interfaz.dirKeyMod = dirKeyMod;
	}

	public JTextField getInputUser() {
		return inputUser;
	}

	public void setInputUser(JTextField inputUser) {
		Interfaz.inputUser = inputUser;
	}

	public JPasswordField getInputPassword() {
		return inputPassword;
	}

	public void setInputPassword(JPasswordField inputPassword) {
		Interfaz.inputPassword = inputPassword;
	}

	public JPasswordField getInputKey() {
		return inputKey;
	}

	public void setInputKey(JPasswordField inputKey) {
		Interfaz.inputKey = inputKey;
	}

	public static void modificarMod(Mod modModificado, Mod mod) throws ErrorNombre {
		for (int i = 0; i < mods.size(); i++) {
			if (mods.get(i).getNombreMod().equals(mod.getNombreMod())) {
				if (!buscarMod(modModificado.getNombreMod()) || mod.getNombreMod().equals(modModificado.getNombreMod())) {
					mods.remove(i);
					mods.add(i, modModificado);
					int posMod = 0;
					for (int j = 0; j < listMod.getModel().getSize(); j++) {
						if (listMod.getModel().getElementAt(j).getText().equals(mod.getNombreMod())) {
							posMod = j;
						}
					}
					((DefaultListModel<JCheckBox>) listMod.getModel()).remove(posMod);
					añadirModGUI(modModificado.getNombreMod(),posMod); //;
				}else {
					throw new ErrorNombre();
				}
			}
		}

	}

	public static void update() {
		frmArmaAk.update(frmArmaAk.getGraphics());
	}

	public static void eliminarMod(Mod mod,int pos) {
		mods.remove(mod);
		modelMod.removeElementAt(pos);
		mod.setEliminado(true);
	}

	static void cargarModsHtml(File file) {
		FileReader fileReader = null;
		try {
			fileReader = new FileReader(file);
		} catch (FileNotFoundException e) {
			System.out.println("Error al leer archivo "+file.getPath());
		}

		BufferedReader bufferedReader = null;
		try {
			bufferedReader = 
					new BufferedReader(fileReader);
			String line = null;
			boolean tablaMods = false;
			bufferedReader.readLine();
			bufferedReader.readLine();
			line = bufferedReader.readLine();
			if (!line.equals("  <!--Created by Arma 3 Launcher: https://arma3.com-->")) {
				System.out.println("Archivo "+file.getName()+" error en el formato. Solucion: Exportar html del Arma 3 Launcher "+line);
				return;
			}
			while((line = bufferedReader.readLine()) != null) {
				if (line.toLowerCase().contains("mod-list")) { //Llegamos a la tabla
					tablaMods = true;
				}
				if (tablaMods) {
					if (line.toLowerCase().contains("<a href")) {
						boolean modDuplicado = false;
						int idMod = Integer.parseInt(line.substring(line.indexOf("id=")+3, line.indexOf("\"", line.indexOf("id=")+3)));
						for (Mod mod : mods) {
							if(mod.getId() == idMod) {
								modDuplicado = true;
							}
						}
						inputIdMod.setText(""+idMod);
						String nombreMod = SearchMod.obtenerNombreModPorId(idMod);
						if (modDuplicado) {
							inputIdMod.setText("");
							System.out.println("Mod "+nombreMod+" ya instalado");
						}else {
							inputNombreMod.setText(nombreMod);
							btnDescargarMod.setEnabled(true);
							btnDescargarMod.doClick();
							System.out.println("Mod "+nombreMod+" instalado");
						}
					}
				}
			}
		} catch (NumberFormatException | IOException | NullPointerException e ) {
			e.printStackTrace();
			Interfaz.mensajeInformacion("Error al leer archivo "+file.getPath());
			try {
				bufferedReader.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}   
		// Always close files.
		try {
			bufferedReader.close();
		} catch (IOException e) {
			e.printStackTrace();
			Interfaz.mensajeError("Error al cerrar archivo "+file.getPath());
		}
	}


	public static JTextField getInputIdMod() {
		return inputIdMod;
	}
}
