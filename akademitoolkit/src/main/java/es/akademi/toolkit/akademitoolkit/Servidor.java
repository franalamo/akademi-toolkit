package es.akademi.toolkit.akademitoolkit;

/**
 * Objeto servidor que almacena toda la informacion relevante del servidor
 * @author Francisco
 *
 */
public class Servidor {
	
	private String nombreServidor;
	private String dirServidor;
	private String contrasena;
	private String nombreServidorMision;
	private int puerto;
	//Opciones avanzadas
	private boolean autoinit;
	private boolean hugepages;
	private boolean nologs;
	private boolean enableHT;
	private boolean filePatching;
	private boolean noCB;
	//Basic
	private String maxBandWidth = "1073741824";
	private String minbandwidth = "107374182";
	private String maxmsgsend = "2048";
	private String maxSizeGuaranteed = "958";
	private String maxSizeNonguaranteed = "224";
	private String minErrorToSend = "0.002";
	private String minErrorToSendNear = "0.02";
	//Advance
	private String passwordAdmin = "";
	private String serverCommandPassword = "";
	private String logFile = "";
	private String motd = "";
	private String motdInterval = "3";
	private String maxPlayers = "100";
	private String kickduplicate = "1";
	private String verifySignatures = "0";
	private String allowedFilePatching = "0";
	private String requiredSecureId = "0";
	private String voteMissionPlayers = "3";
	private String voteThreshold = "1.5";
	private String allowedVoteCmds = "";
	private String disableVoN = "0";
	private String vonCodecQuality = "6";
	private String persistent = "1";
	private String timeStampFormat = "none";
	private String BattlEye = "1";
	private String headlessClients = "";
	private String doubleIdDetected = "";
	private String onUserConnected = "";
	private String onUserDisconnected = "";
	private String onHackedData = "";
	private String onDifferentData = "";
	private String onUnsignedData = "";
	private String regularCheck = "";
	
	
	public Servidor(String nombreServidor, String dirServidor, String contrasena, String nombreServidorMision,
			int puerto, boolean autoinit, boolean hugepages, boolean nologs, boolean enableHT, boolean filePatching,
			boolean noCB) {
		super();
		this.nombreServidor = nombreServidor;
		this.dirServidor = dirServidor;
		this.contrasena = contrasena;
		this.nombreServidorMision = nombreServidorMision;
		this.puerto = puerto;
		this.autoinit = autoinit;
		this.hugepages = hugepages;
		this.nologs = nologs;
		this.enableHT = enableHT;
		this.filePatching = filePatching;
		this.noCB = noCB;
	}
	
	public Servidor(String nombreServidor, String dirServidor, String contrasena, String nombreServidorMision,
			int puerto, boolean autoinit, boolean hugepages, boolean nologs, boolean enableHT, boolean filePatching,
			boolean noCB, String maxBandWidth, String minbandwidth, String maxmsgsend, String maxSizeGuaranteed,
			String maxSizeNonguaranteed, String minErrorToSend, String minErrorToSendNear, String passwordAdmin, 
			String serverCommandPassword, String logFile, String motd, String motdInterval, String maxPlayers, String kickduplicate,
			String verifySignatures, String allowedFilePatching, String requiredSecureId,String voteMissionPlayers, 
			String voteThreshold,String allowedVoteCmds, String disableVoN, String vonCodecQuality, String persistent, 
			String timeStampFormat, String BattlEye, String headlessClients, String doubleIdDetected, String onUserConnected,
			String onUserDisconnected, String onHackedData, String onDifferentData, String onUnsignedData, String regularCheck) {
		super();
		this.nombreServidor = nombreServidor;
		this.dirServidor = dirServidor;
		this.contrasena = contrasena;
		this.nombreServidorMision = nombreServidorMision;
		this.puerto = puerto;
		//Advance settings
		this.autoinit = autoinit;
		this.hugepages = hugepages;
		this.nologs = nologs;
		this.enableHT = enableHT;
		this.filePatching = filePatching;
		this.noCB = noCB;
		//Settings
		this.maxBandWidth = maxBandWidth;
		this.minbandwidth = minbandwidth;
		this.maxmsgsend = maxmsgsend;
		this.maxSizeGuaranteed = maxSizeGuaranteed;
		this.maxSizeNonguaranteed = maxSizeNonguaranteed;
		this.minErrorToSend = minErrorToSend;
		this.minErrorToSendNear = minErrorToSendNear;
		this.passwordAdmin = passwordAdmin;
		this.serverCommandPassword = serverCommandPassword;
		this.logFile = logFile;
		this.motd = motd;
		this.motdInterval = motdInterval;
		this.maxPlayers = maxPlayers;
		this.kickduplicate = kickduplicate;
		this.verifySignatures = verifySignatures;
		this.allowedFilePatching = allowedFilePatching;
		this.requiredSecureId = requiredSecureId;
		this.voteMissionPlayers = voteMissionPlayers;
		this.voteThreshold = voteThreshold;
		this.allowedVoteCmds = allowedVoteCmds;
		this.disableVoN = disableVoN;
		this.vonCodecQuality = vonCodecQuality;
		this.persistent = persistent;
		this.timeStampFormat = timeStampFormat;
		this.BattlEye = BattlEye;
		this.headlessClients = headlessClients;
		this.doubleIdDetected = doubleIdDetected;
		this.onUserConnected = onUserConnected;
		this.onUserDisconnected = onUserDisconnected;
		this.onHackedData = onHackedData;
		this.onDifferentData = onDifferentData;
		this.onUnsignedData = onUnsignedData;
		this.regularCheck = regularCheck;
	}

	public String getNombreServidor() {
		return nombreServidor;
	}
	
	
	public String getDirServidor() {
		return dirServidor;
	}

	
	public String getContrasena() {
		return contrasena;
	}



	public String getNombreServidorMision() {
		return nombreServidorMision;
	}

	public int getPuerto() {
		return puerto;
	}



	public boolean isAutoinit() {
		return autoinit;
	}



	public boolean isHugepages() {
		return hugepages;
	}



	public boolean isNologs() {
		return nologs;
	}



	public boolean isEnableHT() {
		return enableHT;
	}



	public boolean isFilePatching() {
		return filePatching;
	}



	public boolean isNoCB() {
		return noCB;
	}
	
	



	public String getMaxBandWidth() {
		return maxBandWidth;
	}



	public String getMinbandwidth() {
		return minbandwidth;
	}



	public String getMaxmsgsend() {
		return maxmsgsend;
	}



	public String getMaxSizeGuaranteed() {
		return maxSizeGuaranteed;
	}



	public String getMaxSizeNonguaranteed() {
		return maxSizeNonguaranteed;
	}



	public String getMinErrorToSend() {
		return minErrorToSend;
	}



	public String getMinErrorToSendNear() {
		return minErrorToSendNear;
	}



	public String getPasswordAdmin() {
		return passwordAdmin;
	}



	public String getServerCommandPassword() {
		return serverCommandPassword;
	}



	public String getLogFile() {
		return logFile;
	}



	public String getMotdInterval() {
		return motdInterval;
	}



	public String getMaxPlayers() {
		return maxPlayers;
	}



	public String getKickduplicate() {
		return kickduplicate;
	}



	public String getVerifySignatures() {
		return verifySignatures;
	}



	public String getAllowedFilePatching() {
		return allowedFilePatching;
	}



	public String getRequiredSecureId() {
		return requiredSecureId;
	}



	public String getVoteMissionPlayers() {
		return voteMissionPlayers;
	}



	public String getVoteThreshold() {
		return voteThreshold;
	}



	public String getAllowedVoteCmds() {
		return allowedVoteCmds;
	}



	public String getDisableVoN() {
		return disableVoN;
	}



	public String getVonCodecQuality() {
		return vonCodecQuality;
	}



	public String getPersistent() {
		return persistent;
	}



	public String getTimeStampFormat() {
		return timeStampFormat;
	}



	public String getBattlEye() {
		return BattlEye;
	}



	public String getHeadlessClients() {
		return headlessClients;
	}



	public String getDoubleIdDetected() {
		return doubleIdDetected;
	}



	public String getOnUserConnected() {
		return onUserConnected;
	}



	public String getOnUserDisconnected() {
		return onUserDisconnected;
	}



	public String getOnHackedData() {
		return onHackedData;
	}



	public String getOnDifferentData() {
		return onDifferentData;
	}



	public String getOnUnsignedData() {
		return onUnsignedData;
	}



	public String getRegularCheck() {
		return regularCheck;
	}
	
	public String getMotd() {
		return motd;
	}

	public void setNombreServidor(String nombreServidor) {
		this.nombreServidor = nombreServidor;
	}

	public void setDirServidor(String dirServidor) {
		this.dirServidor = dirServidor;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public void setNombreServidorMision(String nombreServidorMision) {
		this.nombreServidorMision = nombreServidorMision;
	}

	public void setPuerto(int puerto) {
		this.puerto = puerto;
	}

	public void setAutoinit(boolean autoinit) {
		this.autoinit = autoinit;
	}

	public void setHugepages(boolean hugepages) {
		this.hugepages = hugepages;
	}

	public void setNologs(boolean nologs) {
		this.nologs = nologs;
	}

	public void setEnableHT(boolean enableHT) {
		this.enableHT = enableHT;
	}

	public void setFilePatching(boolean filePatching) {
		this.filePatching = filePatching;
	}

	public void setNoCB(boolean noCB) {
		this.noCB = noCB;
	}

	public void setMaxBandWidth(String maxBandWidth) {
		this.maxBandWidth = maxBandWidth;
	}

	public void setMinbandwidth(String minbandwidth) {
		this.minbandwidth = minbandwidth;
	}

	public void setMaxmsgsend(String maxmsgsend) {
		this.maxmsgsend = maxmsgsend;
	}

	public void setMaxSizeGuaranteed(String maxSizeGuaranteed) {
		this.maxSizeGuaranteed = maxSizeGuaranteed;
	}

	public void setMaxSizeNonguaranteed(String maxSizeNonguaranteed) {
		this.maxSizeNonguaranteed = maxSizeNonguaranteed;
	}

	public void setMinErrorToSend(String minErrorToSend) {
		this.minErrorToSend = minErrorToSend;
	}

	public void setMinErrorToSendNear(String minErrorToSendNear) {
		this.minErrorToSendNear = minErrorToSendNear;
	}

	public void setPasswordAdmin(String passwordAdmin) {
		this.passwordAdmin = passwordAdmin;
	}

	public void setServerCommandPassword(String serverCommandPassword) {
		this.serverCommandPassword = serverCommandPassword;
	}

	public void setLogFile(String logFile) {
		this.logFile = logFile;
	}

	public void setMotd(String motd) {
		this.motd = motd;
	}

	public void setMotdInterval(String motdInterval) {
		this.motdInterval = motdInterval;
	}

	public void setMaxPlayers(String maxPlayers) {
		this.maxPlayers = maxPlayers;
	}

	public void setKickduplicate(String kickduplicate) {
		this.kickduplicate = kickduplicate;
	}

	public void setVerifySignatures(String verifySignatures) {
		this.verifySignatures = verifySignatures;
	}

	public void setAllowedFilePatching(String allowedFilePatching) {
		this.allowedFilePatching = allowedFilePatching;
	}

	public void setRequiredSecureId(String requiredSecureId) {
		this.requiredSecureId = requiredSecureId;
	}

	public void setVoteMissionPlayers(String voteMissionPlayers) {
		this.voteMissionPlayers = voteMissionPlayers;
	}

	public void setVoteThreshold(String voteThreshold) {
		this.voteThreshold = voteThreshold;
	}

	public void setAllowedVoteCmds(String allowedVoteCmds) {
		this.allowedVoteCmds = allowedVoteCmds;
	}

	public void setDisableVoN(String disableVoN) {
		this.disableVoN = disableVoN;
	}

	public void setVonCodecQuality(String vonCodecQuality) {
		this.vonCodecQuality = vonCodecQuality;
	}

	public void setPersistent(String persistent) {
		this.persistent = persistent;
	}

	public void setTimeStampFormat(String timeStampFormat) {
		this.timeStampFormat = timeStampFormat;
	}

	public void setBattlEye(String battlEye) {
		BattlEye = battlEye;
	}

	public void setHeadlessClients(String headlessClients) {
		this.headlessClients = headlessClients;
	}

	public void setDoubleIdDetected(String doubleIdDetected) {
		this.doubleIdDetected = doubleIdDetected;
	}

	public void setOnUserConnected(String onUserConnected) {
		this.onUserConnected = onUserConnected;
	}

	public void setOnUserDisconnected(String onUserDisconnected) {
		this.onUserDisconnected = onUserDisconnected;
	}

	public void setOnHackedData(String onHackedData) {
		this.onHackedData = onHackedData;
	}

	public void setOnDifferentData(String onDifferentData) {
		this.onDifferentData = onDifferentData;
	}

	public void setOnUnsignedData(String onUnsignedData) {
		this.onUnsignedData = onUnsignedData;
	}

	public void setRegularCheck(String regularCheck) {
		this.regularCheck = regularCheck;
	}

	@Override
	public String toString() {
		return nombreServidor;
	}
	
	

}
