package es.akademi.toolkit.akademitoolkit;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FilenameFilter;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.TitledBorder;

import javax.swing.JCheckBox;
import javax.swing.JFileChooser;

import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.UIManager;
import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class ModificarServidorGUI {

	public JFrame frame;

	private int pos;
	private Servidor servidor;
	private JTextField inputNombrePublicoServidor;
	private JPasswordField inputPasswordServidor;
	private JTextField inputPuertoServidor;
	private JTextField inputDirServidor;
	private JTextField inputNombreServidor;

	/**
	 * Create the application.
	 */
	public ModificarServidorGUI(Servidor servidor,int pos) {
		this.pos = pos;
		this.servidor = servidor;
		initialize();
	}
	
	private boolean buscarArchivoServidor(String file) {
		File f = new File(file);
		File[] matchingFiles = f.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return (name.startsWith("arma3server") && name.endsWith(".exe")) || (name.startsWith("arma3server_x64") && name.endsWith(".exe"));
			}
		});
		if (matchingFiles != null && matchingFiles.length > 0 && !inputNombreServidor.getText().equals("") && !inputPuertoServidor.getText().equals("")) {
			System.out.println("Servidor Encontrado en la ruta: " + file);
			return true;
		}else {
			System.out.println("Servidor NO Encontrado en la ruta: " + file);
			return false;
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame(servidor.getNombreServidor());
		frame.setBounds(100, 100, 608, 459);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Modificar Servidor", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		frame.getContentPane().add(panel, BorderLayout.CENTER);

		final JButton btnModificarServidor = new JButton("Modificar Servidor");

		btnModificarServidor.setEnabled(true);

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Parametros Avanzados", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));

		final JCheckBox checkBoxAutoInit = new JCheckBox("autoInit");
		checkBoxAutoInit.setToolTipText("Automatically initialize mission just like first client does.");
		if (servidor.isAutoinit()) {
			checkBoxAutoInit.setSelected(true);
		}

		final JCheckBox checkBoxEnableHT = new JCheckBox("enableHT");
		checkBoxEnableHT.setToolTipText("Enables the use of all logical CPU cores for parallel tasks processing. If your CPU does not support Hyper-Threading or similar technology, this parameter is ignored.");
		if (servidor.isEnableHT()) {
			checkBoxEnableHT.setSelected(true);
		}
		final JCheckBox checkBoxHugepages = new JCheckBox("hugepages");
		checkBoxHugepages.setToolTipText("Enables hugepages with the default memory allocator (malloc) for both client/server ");
		if (servidor.isHugepages()) {
			checkBoxHugepages.setSelected(true);
		}
		final JCheckBox checkBoxFilepatching = new JCheckBox("filePatching");
		checkBoxFilepatching.setToolTipText("Allow the game to load unpacked data.");
		if (servidor.isFilePatching()) {
			checkBoxFilepatching.setSelected(true);
		}
		final JCheckBox checkBoxNologs = new JCheckBox("noLogs");
		checkBoxNologs.setToolTipText("Be aware this means none errors saved to RPT file (report log). Yet in case of crash the fault address block info is saved.");
		if (servidor.isNologs()) {
			checkBoxNologs.setSelected(true);
		}
		final JCheckBox checkBoxNoCB = new JCheckBox("noCB");
		checkBoxNoCB.setToolTipText("Turns off multicore use. It slows down rendering but may resolve visual glitches.");
		if (servidor.isNoCB()) {
			checkBoxNoCB.setSelected(true);
		}
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGap(0, 337, Short.MAX_VALUE)
				.addGroup(gl_panel_1.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addComponent(checkBoxAutoInit)
								.addComponent(checkBoxEnableHT))
						.addGap(18)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addComponent(checkBoxHugepages)
								.addComponent(checkBoxFilepatching))
						.addPreferredGap(ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addComponent(checkBoxNologs)
								.addComponent(checkBoxNoCB))
						.addGap(57))
				);
		gl_panel_1.setVerticalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGap(0, 122, Short.MAX_VALUE)
				.addGroup(gl_panel_1.createSequentialGroup()
						.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
								.addComponent(checkBoxAutoInit)
								.addComponent(checkBoxNologs)
								.addComponent(checkBoxHugepages))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
								.addComponent(checkBoxEnableHT)
								.addComponent(checkBoxNoCB)
								.addComponent(checkBoxFilepatching))
						.addContainerGap(53, Short.MAX_VALUE))
				);
		panel_1.setLayout(gl_panel_1);

		JLabel label = new JLabel("Puerto:");

		JLabel label_1 = new JLabel("Nombre P\u00FAblico:");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 10));

		JLabel label_2 = new JLabel("Contrase\u00F1a:");

		inputNombrePublicoServidor = new JTextField(servidor.getNombreServidorMision());
		inputNombrePublicoServidor.setColumns(10);

		inputPasswordServidor = new JPasswordField(servidor.getContrasena());
		inputPasswordServidor.setEchoChar('*');

		inputPuertoServidor = new JTextField(String.valueOf(servidor.getPuerto()));
		inputPuertoServidor.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				if (buscarArchivoServidor(inputDirServidor.getText()) && !inputNombreServidor.getText().equals("") && !inputPuertoServidor.getText().equals("")) {
					btnModificarServidor.setEnabled(true);
				}else {
					btnModificarServidor.setEnabled(false);
				}
			}
		});
		inputPuertoServidor.setColumns(10);

		JLabel label_3 = new JLabel("Direcci\u00F3n:");

		JLabel label_4 = new JLabel("Nombre:");

		inputDirServidor = new JTextField(servidor.getDirServidor());
		inputDirServidor.setColumns(10);

		inputNombreServidor = new JTextField(servidor.getNombreServidor());
		inputNombreServidor.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				if (buscarArchivoServidor(inputDirServidor.getText()) && !inputNombreServidor.getText().equals("") && !inputPuertoServidor.getText().equals("")) {
					btnModificarServidor.setEnabled(true);
				}else {
					btnModificarServidor.setEnabled(false);
				}
			}
		});
		
		inputNombreServidor.setColumns(10);

		JButton button_2 = new JButton("");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new java.io.File("."));
				chooser.setDialogTitle("Directorio Servidor");
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				chooser.setAcceptAllFileFilterUsed(false);

				if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					inputDirServidor.setText(chooser.getSelectedFile().toString());
					if (buscarArchivoServidor(chooser.getSelectedFile().toString()) && !inputNombreServidor.getText().equals("") && !inputPuertoServidor.getText().equals("")) {
						btnModificarServidor.setEnabled(true);
					}else {
						btnModificarServidor.setEnabled(false);
					}
				}
			}
		});
		button_2.setIcon(new ImageIcon(ModificarServidorGUI.class.getResource("/images/folder-icon.gif")));

		JButton btnNewButton = new JButton("Eliminar Servidor");
		
		final JCheckBox chckbxPasswordServer = new JCheckBox("");
		chckbxPasswordServer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (chckbxPasswordServer.isSelected()) {
					inputPasswordServidor.setEchoChar((char)0);
				} else {
					inputPasswordServidor.setEchoChar('*');
				}
			}
		});
		
		JButton btnNewButton_1 = new JButton("Opciones Avanzadas");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ServerInfoGUI sv = new ServerInfoGUI(servidor);
				sv.setVisible(true);
			}
		});

		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel.createSequentialGroup()
									.addContainerGap()
									.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_panel.createSequentialGroup()
											.addGap(40)
											.addComponent(label))
										.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
											.addComponent(label_1)
											.addComponent(label_2)))
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
										.addComponent(inputNombrePublicoServidor, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 441, Short.MAX_VALUE)
										.addComponent(inputPuertoServidor, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 441, Short.MAX_VALUE)
										.addComponent(inputPasswordServidor, GroupLayout.DEFAULT_SIZE, 441, Short.MAX_VALUE)))
								.addGroup(gl_panel.createSequentialGroup()
									.addGap(23)
									.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
										.addComponent(label_3)
										.addComponent(label_4))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
										.addComponent(inputDirServidor, GroupLayout.DEFAULT_SIZE, 463, Short.MAX_VALUE)
										.addComponent(inputNombreServidor, GroupLayout.DEFAULT_SIZE, 463, Short.MAX_VALUE))))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(chckbxPasswordServer)
								.addComponent(button_2, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(35)
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(btnNewButton, GroupLayout.DEFAULT_SIZE, 535, Short.MAX_VALUE)
								.addComponent(btnModificarServidor, GroupLayout.DEFAULT_SIZE, 535, Short.MAX_VALUE)
								.addGroup(gl_panel.createSequentialGroup()
									.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 277, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(btnNewButton_1, GroupLayout.DEFAULT_SIZE, 252, Short.MAX_VALUE)))))
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(11)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(inputNombreServidor, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_4))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
						.addComponent(button_2, 0, 0, Short.MAX_VALUE)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
							.addComponent(label_3)
							.addComponent(inputDirServidor, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
							.addComponent(label_2)
							.addComponent(inputPasswordServidor, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(chckbxPasswordServer))
					.addGap(6)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(inputNombrePublicoServidor, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_1))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(inputPuertoServidor, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(label))
					.addGap(24)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
						.addComponent(btnNewButton_1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 122, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnModificarServidor, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
					.addGap(11)
					.addComponent(btnNewButton, GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE)
					.addContainerGap())
		);

		panel.setLayout(gl_panel);
		btnModificarServidor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Servidor svModificado = new Servidor(inputNombreServidor.getText(), inputDirServidor.getText(), new String (inputPasswordServidor.getPassword()), inputNombrePublicoServidor.getText(),Integer.parseInt(inputPuertoServidor.getText()),checkBoxAutoInit.isSelected(),checkBoxHugepages.isSelected(),checkBoxNologs.isSelected(),checkBoxEnableHT.isSelected(),checkBoxFilepatching.isSelected(),checkBoxNoCB.isSelected()) ;
					Interfaz.modificarServidor(servidor,svModificado);
					frame.setTitle(svModificado.getNombreServidor());
					servidor = svModificado;
				} catch (Interfaz.ErrorNombre e) {
					System.out.println("El nombre del servidor ya existe");
				}
			}
		});

		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Interfaz.eliminarServidor(servidor,pos);
				frame.dispose();
			}
		});
	}
}
