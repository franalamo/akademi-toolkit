package es.akademi.toolkit.akademitoolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

/**
 * 
 * @author Francisco
 *
 */
public class Mod {
	private String nombreMod;
	private String dirMod;
	private ArrayList<String> dirKeys;
	private int id;
	private ArrayList<Modulo> modulos = new ArrayList<Modulo>();
	private ProgressBarGUI progressBarGui;
	private int numberReadMods = 0;
	private int numberDeleteFolder = 0;
	private int numberFiles;
	private int timeUpdated;
	private boolean eliminado = false; 
	public Mod(String nombreMod, String dirMod, ArrayList<String> dirKeys,int id, boolean leerPaquetes) {
		super();
		this.dirKeys = new ArrayList<String>();
		for (String string : dirKeys) {
			this.dirKeys.add(string.trim());
		}
		this.nombreMod = nombreMod;
		this.dirMod = dirMod;
		this.id = id;
		this.timeUpdated = obtenerUltimaActualizacion(this);
	}

	public String getNombreMod() {
		return nombreMod;
	}

	public String getDirMod() {
		return dirMod;
	}

	public ArrayList<String> getDirKeys() {
		return dirKeys;
	}

	public int getId() {
		return id;
	}

	public ArrayList<Modulo> getModulos() {
		return modulos;
	}



	public boolean isEliminado() {
		return eliminado;
	}

	public void setEliminado(boolean eliminado) {
		this.eliminado = eliminado;
	}

	private boolean extraerModPBO(File f) {
		if (f== null) {
			Interfaz.mensajeError("Mod no encontrado");
			return false;
		}
		String directorioPbo = f.getParent();
		//Existe el archivo
		Process r = null;
		try {
			r = Runtime.getRuntime().exec("cmd /c cd /d \"" + directorioPbo +"\" && ExtractPboDos.exe -P " + f.getName());	
		} catch (IOException e1) {
			Interfaz.mensajeError("Error al descomprimir");
		}
		try {
			r.waitFor();
		} catch (InterruptedException e1) {
			Interfaz.mensajeError("Error al esperar");
		}
		numberReadMods++;
		progressBarGui.label.setText(nombreMod+" "+numberReadMods+"/"+numberFiles);
		progressBarGui.progressBar.setValue(numberReadMods);
		progressBarGui.update(progressBarGui.getGraphics());
		progressBarGui.changeTitle(nombreMod+" "+numberReadMods+"/"+numberFiles);
		return true;
	}
	
	public void leerPaquetes() {
		progressBarGui = new ProgressBarGUI(nombreMod);
		File fileMod = new File(dirMod);
		File fileMod2 = new File(dirMod+File.separator+"addons");
		ArrayList<File> pbos = new ArrayList<File>();
		File[] files = fileMod2.listFiles(new FileFilter() {
			@Override
			public boolean accept(File f) {
				return f.getName().endsWith(".pbo");
			}
		});
		numberFiles = files.length;
		progressBarGui.progressBar.setMaximum(numberFiles);
		progressBarGui.progressBar.setString("Cargando Modulos Mod "+nombreMod);
		progressBarGui.label.setText(nombreMod+" "+numberReadMods+"/"+numberFiles);
		progressBarGui.update(progressBarGui.getGraphics());
		progressBarGui.changeTitle(nombreMod+" "+numberReadMods+"/"+numberFiles);
		for (File file : files) {
			pbos.add(file);
			extraerModPBO(file);
		}
		synchronized (modulos) {
			extraerPaquetesMod(fileMod);
			fileMod = new File(fileMod.getAbsolutePath()+File.separator+"addons");
			if (modulos.size() == 0) {
				for (File pbo : pbos) {
					modulos.add(new Modulo(pbo.getName().replaceAll(".pbo", ""), new ArrayList<String>()));
				}
			}
		}
		boolean encontrado = false;
		for (File file : files) {
			for (Modulo modulo : modulos) {
				if (file.getName().replaceAll(".pbo", "").toLowerCase().equals(modulo.getNombre().toLowerCase())) {
					encontrado = true;
				}
			}
			if (!encontrado) {
				System.out.println("AÑADIDO "+file.getName().replaceAll(".pbo", "").toLowerCase());
				modulos.add(new Modulo(file.getName().replaceAll(".pbo", ""), new ArrayList<String>()));
			}
			encontrado = false;
		}
		eliminarTodasLasCarpetas(fileMod);
		progressBarGui.dispose();
	}

	private void extraerPaquetesMod(File file) {
		if (file.isDirectory()) {
			File[] arr = file.listFiles();
			for (File f : arr) {
				extraerPaquetesMod(f);
			}
		} else {
			if (file.getName().toLowerCase().equals("config.cpp")) {
				FileReader fileReader = null;
				try {
					fileReader = new FileReader(file);
				} catch (FileNotFoundException e) {
					System.out.println("Error al leer archivo "+file.getPath());
				}

				BufferedReader bufferedReader = null;
				try {
					bufferedReader = 
							new BufferedReader(fileReader);
					String line2 = null;
					boolean moduloEncontrado = false;
					while((line2 = bufferedReader.readLine()) != null && !moduloEncontrado) {
						if (line2.toLowerCase().contains("class cfgpatches")) {
							ArrayList<String> dModulos = new ArrayList<String>();
							String name = null;
							while (!(name = bufferedReader.readLine()).contains("class")) {
							}
							if((name.indexOf(" ", name.indexOf("class ")+6)) == -1) {
								name = name.substring(name.indexOf("class ")+6, name.length());
							}else {
								name = name.substring(name.indexOf("class ")+6, name.indexOf(" ", name.indexOf("class ")+6));
							}
							if(name.equals("ADDON")){
								File f = new File(file.getParent()+File.separator+"README.md");
								if(!f.exists()) {
									break;
								}
								FileReader fileReader2 = null;
								try {
									fileReader2 = new FileReader(f);
								} catch (FileNotFoundException e) {
								}
								BufferedReader bufferedReader2 = null;
								try {
									bufferedReader2 = new BufferedReader(fileReader2);
								}catch (Exception e) {
									System.err.println("Documento no encontrado");
									break;
								}
								name = bufferedReader2.readLine();
								
								try {
									bufferedReader2.close();
								} catch (IOException e1) {
								}
							}
							String subModules = null;
							while((subModules = bufferedReader.readLine()) != null && !subModules.toLowerCase().contains("requiredaddons")) {
							}
							if(bufferedReader.readLine() == null) {
								break;
							}
							String[] modules = null;
							try {
								modules = subModules.substring(subModules.indexOf("{")+1, subModules.indexOf("}")).replaceAll("\"", "").toLowerCase().split(",");
							}catch (Exception e) {
								ArrayList<String> modules2 = new ArrayList<String>();
								try {
									while(!(subModules = bufferedReader.readLine()).endsWith("};")) {
										try {
											modules2.add(subModules.substring(subModules.indexOf("\"")+1, subModules.indexOf("\"",subModules.indexOf("\"")+1)));
										}catch (Exception e2) {
											modules2.add(subModules.replaceAll(",", "").trim());
										}
									}
									modules = modules2.toArray(new String[0]);
								} catch (Exception e2) {
									System.err.println("Error: "+subModules+" archivo "+file.getAbsolutePath());
								}
								
							}
							try {
								for (String string : modules) {
									string = string.replaceAll("[{}=]", "");
									dModulos.add(string);
								}
								if(!name.isEmpty()) {
									this.modulos.add(new Modulo(name,dModulos));
								}
							}catch (Exception e) {
							}
							moduloEncontrado = true;
						}
					}
				} catch (NumberFormatException | IOException | NullPointerException e ) {
					e.printStackTrace();
					Interfaz.mensajeInformacion("Error al leer archivo "+file.getPath());
					try {
						bufferedReader.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}   
				// Always close files.
				try {
					bufferedReader.close();
				} catch (IOException e) {
					e.printStackTrace();
					Interfaz.mensajeError("Error al cerrar archivo "+file.getPath());
				}
			}
			if (file.getName().toLowerCase().equals("cfgpatches.hpp")) {
				FileReader fileReader = null;
				try {
					fileReader = new FileReader(file);
				} catch (FileNotFoundException e) {
					System.out.println("Error al leer archivo "+file.getPath());
				}

				BufferedReader bufferedReader = null;
				try {
					bufferedReader = 
							new BufferedReader(fileReader);
					String line2 = null;
					boolean moduloEncontrado = false;
					while((line2 = bufferedReader.readLine()) != null && !moduloEncontrado) {
						if (line2.toLowerCase().contains("class cfgpatches")) {
							ArrayList<String> dModulos = new ArrayList<String>();
							String name = null;
							while (!(name = bufferedReader.readLine()).contains("class")) {
							}
							if((name.indexOf(" ", name.indexOf("class ")+6)) == -1) {
								name = name.substring(name.indexOf("class ")+6, name.length());
							}else {
								name = name.substring(name.indexOf("class ")+6, name.indexOf(" ", name.indexOf("class ")+6));
							}
							if(name.equals("ADDON")){
								File f = new File(file.getParent()+File.separator+"README.md");
								if(!f.exists()) {
									break;
								}
								FileReader fileReader2 = null;
								try {
									fileReader2 = new FileReader(f);
								} catch (FileNotFoundException e) {
								}

								BufferedReader bufferedReader2 = null;
								try {
									bufferedReader2 = new BufferedReader(fileReader2);
								}catch (Exception e) {
									System.err.println("Documento no encontrado");
									break;
								}
								name = bufferedReader2.readLine();
								try {
									bufferedReader2.close();
								} catch (IOException e1) {
								}
							}
							String subModules = null;
							while((subModules = bufferedReader.readLine()) != null && !subModules.toLowerCase().contains("requiredaddons")) {
							}
							if(bufferedReader.readLine() == null) {
								break;
							}
							String[] modules = null;
							try {
								modules = subModules.substring(subModules.indexOf("{")+1, subModules.indexOf("}")).replaceAll("\"", "").toLowerCase().split(",");
							}catch (Exception e) {
								ArrayList<String> modules2 = new ArrayList<String>();
								while(!(subModules = bufferedReader.readLine()).endsWith("};") && subModules != null) {
									try {
										modules2.add(subModules.substring(subModules.indexOf("\"")+1, subModules.indexOf("\"",subModules.indexOf("\"")+1)));
									}catch (Exception e2) {
										modules2.add(subModules.replaceAll(",", "").trim());
									}
								}
								modules = modules2.toArray(new String[0]);
							}
							for (String string : modules) {
								dModulos.add(string);
							}
							moduloEncontrado = true;
						}
					}
				} catch (NumberFormatException | IOException | NullPointerException e ) {
					e.printStackTrace();
					Interfaz.mensajeInformacion("Error al leer archivo "+file.getPath());
					try {
						bufferedReader.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}   
				// Always close files.
				try {
					bufferedReader.close();
				} catch (IOException e) {
					e.printStackTrace();
					Interfaz.mensajeError("Error al cerrar archivo "+file.getPath());
				}
			}
		}
	}
	public String escribirPaquetes() {
		String paquetes ="[";
		for (int i = 0; i < this.getModulos().size(); i++) {
			if (i == this.getModulos().size() - 1) {
				paquetes = paquetes + this.getModulos().get(i).getNombre()+this.getModulos().get(i).escribeModulos() + "]";
			}else {
				paquetes = paquetes + this.getModulos().get(i).getNombre()+this.getModulos().get(i).escribeModulos() + ",";
			}
		}
		return paquetes;
	}
	
	public String escribirKeys() {
		String keys ="";
		for (int i = 0; i < this.getDirKeys().size(); i++) {
			if (i == this.getDirKeys().size() - 1) {
				keys = keys + this.getDirKeys().get(i);
			}else {
				keys = keys + this.getDirKeys().get(i) + ",";
			}
		}
		return keys;
	}

	private void eliminarTodasLasCarpetas(File file) {
		if (file.isDirectory()) {
			File[] arr = file.listFiles();
			for (File f : arr) {
				eliminarTodasLasCarpetas(f);
			}
			if (file.isDirectory() && !(file.getAbsolutePath().toLowerCase().equals(dirMod.toLowerCase()+File.separator+"addons"))) {
				try {
					FileUtils.deleteDirectory(file);
					numberDeleteFolder++;
					progressBarGui.progressBar.setMaximum(numberFiles);
					progressBarGui.progressBar.setValue(numberDeleteFolder);
					progressBarGui.progressBar.setString("Eliminando Carpetas Generadas");
					progressBarGui.label.setText(nombreMod+" "+numberDeleteFolder+"/"+numberFiles);
					progressBarGui.update(progressBarGui.getGraphics());
					progressBarGui.changeTitle(nombreMod+" "+numberDeleteFolder+"/"+numberFiles);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	}

	private int obtenerUltimaActualizacion (Mod mod) {
		HttpClient httpclient = HttpClients.createDefault();
		HttpPost httppost = new HttpPost("https://api.steampowered.com/ISteamRemoteStorage/GetPublishedFileDetails/v1/");
		int stampTime = 0;
		// Request parameters and other properties.
		List<NameValuePair> params = new ArrayList<NameValuePair>(2);
		params.add(new BasicNameValuePair("key", "7D03BDF3CA93ACC07F53E386B9FBF8CE"));
		params.add(new BasicNameValuePair("itemcount", "1"));
		params.add(new BasicNameValuePair("publishedfileids[0]", ""+mod.getId()));

		try {
			httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		//Execute and get the response.
		HttpResponse response = null;
		try {
			response = httpclient.execute(httppost);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		HttpEntity entity = response.getEntity();

		if (entity != null) {
			InputStream instream = null;
			try {
				instream = entity.getContent();
			} catch (UnsupportedOperationException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(instream));
				String temp;
				try {
					while ((temp = in.readLine()) != null){
						try {
							stampTime =  Integer.parseInt(temp.substring(temp.indexOf("time_updated")+14, temp.indexOf(",",temp.indexOf("time_updated"))));	
						}catch (Exception e) {
						}

					}
				} catch (IOException e) {
					e.printStackTrace();
				}
				// do something useful
			} finally {
				try {
					instream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return stampTime;

	}
	
	

	public ProgressBarGUI getProgressBarGui() {
		return progressBarGui;
	}

	public void setProgressBarGui(ProgressBarGUI progressBarGui) {
		this.progressBarGui = progressBarGui;
	}

	public int getNumberReadMods() {
		return numberReadMods;
	}

	public void setNumberReadMods(int numberReadMods) {
		this.numberReadMods = numberReadMods;
	}

	public int getNumberDeleteFolder() {
		return numberDeleteFolder;
	}

	public void setNumberDeleteFolder(int numberDeleteFolder) {
		this.numberDeleteFolder = numberDeleteFolder;
	}

	public int getNumberFiles() {
		return numberFiles;
	}

	public void setNumberFiles(int numberFiles) {
		this.numberFiles = numberFiles;
	}

	public void setNombreMod(String nombreMod) {
		this.nombreMod = nombreMod;
	}

	public void setDirMod(String dirMod) {
		this.dirMod = dirMod;
	}

	public void setDirKeys(ArrayList<String> dirKeys) {
		this.dirKeys = dirKeys;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setModulos(ArrayList<Modulo> modulos) {
		this.modulos = modulos;
	}

	public void addElement(Modulo modulo) {
		this.modulos.add(modulo);
	}



	public void setTimeUpdated(int timeUpdated) {
		this.timeUpdated = timeUpdated;
	}



	public void setDirKey(ArrayList<String> dirKeys) {
		this.dirKeys = dirKeys;
	}

	public int getTimeUpdated() {
		return timeUpdated;
	}

	@Override
	public String toString() {
		return nombreMod;
	}








}
