package es.akademi.toolkit.akademitoolkit;
import java.util.ArrayList;

public class Mision {
	private String nombreMision;
	private String dirMision;
	private ArrayList<String> modulosNecesarios = new ArrayList<String>();
	public Mision(String nombreMision, String dirMision) {
		super();
		this.nombreMision = nombreMision;
		this.dirMision = dirMision;
	}
	@Override
	public String toString() {
		return nombreMision;
	}
	public String getNombreMision() {
		return nombreMision;
	}
	public String getDirMision() {
		return dirMision;
	}
	public ArrayList<String> getModulosNecesarios() {
		return modulosNecesarios;
	}
	public void addElement (String element) {
		modulosNecesarios.add(element);
	}
	public void setDirMision(String dirMision) {
		this.dirMision = dirMision;
	}
	
	
	
	
	
	
	
	

}
