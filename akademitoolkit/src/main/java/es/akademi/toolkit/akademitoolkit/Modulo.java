package es.akademi.toolkit.akademitoolkit;

import java.util.ArrayList;

public class Modulo {
	private String nombre;
	private ArrayList<String> dModulos;
	
	public Modulo (String nombre, ArrayList<String> dModulos) {
		this.nombre = nombre;
		this.dModulos = dModulos;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ArrayList<String> getdModulos() {
		return dModulos;
	}

	public void setdModulos(ArrayList<String> dModulos) {
		this.dModulos = dModulos;
	}
	
	public String escribeModulos() {
		String modules = "*";
		for (int i = 0; i< this.dModulos.size(); i++) {
			if (i == this.dModulos.size() - 1) {
				modules = modules + this.dModulos.get(i) + "*";
			}else {
				modules = modules + this.dModulos.get(i) + "=";
			}
		}
		if(modules.equals("*")) {
			modules = modules + "*";
		}
		return modules;
	}

}
