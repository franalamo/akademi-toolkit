package es.akademi.toolkit.akademitoolkit;

import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;


import javax.swing.JComboBox;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;
import java.awt.Color;

public class StartServer {

	JFrame frame;
	static DefaultListModel<JCheckBox> modelMods = new DefaultListModel<JCheckBox>();
	private static JCheckBoxListMultiple listMods = new JCheckBoxListMultiple(modelMods);
	private JScrollPane scrollPaneModsPerfil = new JScrollPane(listMods);
	static DefaultListModel<JCheckBox> modelModsOpcionales = new DefaultListModel<JCheckBox>();
	private static JCheckBoxListMultiple listModsOpcionales = new JCheckBoxListMultiple(modelModsOpcionales);
	private JScrollPane scrollPaneModsPerfilOpcionales = new JScrollPane(listModsOpcionales);
	static DefaultListModel<JCheckBox> modelMisiones = new DefaultListModel<JCheckBox>();
	private static JCheckBoxListSingle listMisiones = new JCheckBoxListSingle(modelMisiones);
	private JScrollPane scrollPaneMisionesPerfil = new JScrollPane(listMisiones);
	static DefaultListModel<JCheckBox> modelPerfiles = new DefaultListModel<JCheckBox>();
	private static JCheckBoxListSingle listPerfiles = new JCheckBoxListSingle(modelPerfiles);
	private JScrollPane scrollPanePerfilesPerfil = new JScrollPane(listPerfiles);


	private ArrayList<Servidor> listaServidores;


	/**
	 * Create the application.
	 */
	public StartServer(ArrayList<Perfil> listaPerfiles,ArrayList<Servidor> listaServidores,ArrayList<Mod> listaMods,ArrayList<Mision> listaMisiones,Interfaz interfaz) {

		this.listaServidores = listaServidores;
		modelMisiones.removeAllElements();
		modelPerfiles.removeAllElements();
		modelMods.removeAllElements();
		modelModsOpcionales.removeAllElements();
		//Misiones
		for (int i = 0; i < listaMisiones.size(); i++) {
			modelMisiones.addElement(new JCheckBox(listaMisiones.get(i).getNombreMision()));
		}
		//Perfiles
		for (int i = 0; i < listaPerfiles.size(); i++) {
			modelPerfiles.addElement(new JCheckBox(listaPerfiles.get(i).getNombrePerfil()));
		}
		//Mods
		for (int i = 0; i < listaMods.size(); i++) {
			modelMods.addElement(new JCheckBox(listaMods.get(i).getNombreMod()));
		}
		//Mods Opcionales
		for (int i = 0; i < listaMods.size(); i++) {
			modelModsOpcionales.addElement(new JCheckBox(listaMods.get(i).getNombreMod()));
		}
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		listMisiones.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				deseleccionarCheckBoxModsPerfil();
				if (!listMisiones.isSelectionEmpty()) {
					String nombreMision = listMisiones.getSelectedValue().getText();
					Mision mision = Interfaz.buscarMisionPorNombre(nombreMision);
					ArrayList<String> modulosMision = mision.getModulosNecesarios();
					ArrayList<Mod> modsNecesarios = new ArrayList<Mod>();
					boolean moduloEncontrado = false;
					Mod mod = null;
					for (String moduloMision : modulosMision) {
						moduloEncontrado = false;
						//MODULOS DE LA MISION
						for (int i = 0; i < modelMods.getSize(); i++) { //MODS EN LA LISTA
							JCheckBox ckbmod = modelMods.getElementAt(i);
							mod = Interfaz.buscarModPorNombre(ckbmod.getText());
							ArrayList<Modulo> modulosMod = mod.getModulos();
							for (Modulo moduloMod : modulosMod) { //MODULOS DEL MOD
								if (lock_match(moduloMision.toLowerCase(),moduloMod.getNombre().toLowerCase()) > 90) {
									for (String string : moduloMod.getdModulos()) {
										for (Mod mod2 : Interfaz.mods) {
											for (Modulo string2 : mod2.getModulos()) {
												if(lock_match(string2.getNombre().toLowerCase(),string.toLowerCase()) > 90) {
													modsNecesarios.add(mod2);
													break;
												}
											}
										}
									}
									ckbmod.setSelected(true);
									listMods.repaint();
									moduloEncontrado = true;
									continue;
								}
							}

						}
						if (!moduloEncontrado) {
							//String nombreModNoEncontrado = moduloMision.substring(0, moduloMision.indexOf("_")).toUpperCase();
							System.out.println("Modulo "+moduloMision+ " no encontrado");
						}
						
						for (Mod mod3 : modsNecesarios) {
							for (int i = 0; i < modelMods.getSize(); i++) {
								if(mod3.getNombreMod().equals(modelMods.get(i).getText())) {
									modelMods.get(i).setSelected(true);
									listMods.repaint();
								}
							}
						}
					}
				}
			}
		});
		frame = new JFrame();
		frame.setBounds(100, 100, 997, 655);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(new TitledBorder(null, "Servidor", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		String[] arrayServidores = new String[listaServidores.size()];
		for(int i = 0; i < arrayServidores.length; i++) {
			arrayServidores[i] = listaServidores.get(i).toString();
		}

		ComboBoxModel<Object> s = new DefaultComboBoxModel<Object>(arrayServidores);

		JComboBox<Object> comboBox = new JComboBox<Object>(s);
		comboBox.setBounds(10, 45, 572, 31);
		panel.add(comboBox);

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Perfiles", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGap(0, 248, Short.MAX_VALUE)
				.addComponent(scrollPanePerfilesPerfil, GroupLayout.DEFAULT_SIZE, 236, Short.MAX_VALUE)
				);
		gl_panel_1.setVerticalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGap(0, 139, Short.MAX_VALUE)
				.addComponent(scrollPanePerfilesPerfil, GroupLayout.DEFAULT_SIZE, 116, Short.MAX_VALUE)
				);
		panel_1.setLayout(gl_panel_1);

		JButton button = new JButton("Iniciar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ArrayList<Mod> mods = new ArrayList<Mod>();
				ArrayList<Mod> modsOpcionales = new ArrayList<Mod>();
				Servidor sv;
				Mision m = null;
				for (int i = 0; i < modelMods.size(); i++) {
					if (modelMods.getElementAt(i).isSelected()) {
						Mod mod = Interfaz.buscarModPorNombre(modelMods.getElementAt(i).getText());
						System.out.println(modelMods.getElementAt(i).getText());
						mods.add(mod);
					}
				}
				
				for (int i = 0; i < modelModsOpcionales.size(); i++) {
					if (modelModsOpcionales.getElementAt(i).isSelected()) {
						Mod mod = Interfaz.buscarModPorNombre(modelModsOpcionales.getElementAt(i).getText());
						System.out.println(modelModsOpcionales.getElementAt(i).getText());
						modsOpcionales.add(mod);
					}
				}

				String s2 = (String) comboBox.getSelectedItem();
				System.out.println(s2);
				sv = Interfaz.buscarServidorPorNombre(s2);

				for (int i = 0; i < modelMisiones.size(); i++) {
					if (modelMisiones.getElementAt(i).isSelected()) {
						System.out.println(modelMisiones.getElementAt(i).getText());
						m = Interfaz.buscarMisionPorNombre(modelMisiones.getElementAt(i).getText());
					}
				}
				Interfaz.execArma3Server(mods,modsOpcionales, sv, m);
			}
		});

		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Mods Obligatorios", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));

		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
				gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGap(0, 348, Short.MAX_VALUE)
				.addComponent(scrollPaneModsPerfil, GroupLayout.DEFAULT_SIZE, 336, Short.MAX_VALUE)
				);
		gl_panel_2.setVerticalGroup(
				gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGap(0, 301, Short.MAX_VALUE)
				.addComponent(scrollPaneModsPerfil, GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE)
				);
		panel_2.setLayout(gl_panel_2);

		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(null, "Misiones", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		GroupLayout gl_panel_3 = new GroupLayout(panel_3);
		gl_panel_3.setHorizontalGroup(
				gl_panel_3.createParallelGroup(Alignment.TRAILING)
				.addGap(0, 367, Short.MAX_VALUE)
				.addComponent(scrollPaneMisionesPerfil, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 355, Short.MAX_VALUE)
				);
		gl_panel_3.setVerticalGroup(
				gl_panel_3.createParallelGroup(Alignment.TRAILING)
				.addGap(0, 301, Short.MAX_VALUE)
				.addComponent(scrollPaneMisionesPerfil, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE)
				);
		panel_3.setLayout(gl_panel_3);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Mods Opcionales", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		GroupLayout gl_panel_4 = new GroupLayout(panel_4);
		gl_panel_4.setHorizontalGroup(
			gl_panel_4.createParallelGroup(Alignment.LEADING)
				.addGap(0, 288, Short.MAX_VALUE)
				.addComponent(scrollPaneModsPerfilOpcionales, GroupLayout.DEFAULT_SIZE, 336, Short.MAX_VALUE)
		);
		gl_panel_4.setVerticalGroup(
			gl_panel_4.createParallelGroup(Alignment.LEADING)
				.addGap(0, 455, Short.MAX_VALUE)
				.addComponent(scrollPaneModsPerfilOpcionales, GroupLayout.DEFAULT_SIZE, 257, Short.MAX_VALUE)
		);
		
		scrollPaneModsPerfilOpcionales.setViewportView(listModsOpcionales);
		panel_4.setLayout(gl_panel_4);
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
							.addComponent(panel, GroupLayout.DEFAULT_SIZE, 592, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 248, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(button, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
						.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
							.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 288, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(panel_4, GroupLayout.DEFAULT_SIZE, 292, Short.MAX_VALUE)
							.addGap(18)
							.addComponent(panel_3, GroupLayout.PREFERRED_SIZE, 367, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
							.addComponent(panel_1, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 139, GroupLayout.PREFERRED_SIZE)
							.addGroup(groupLayout.createSequentialGroup()
								.addContainerGap()
								.addComponent(button, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE)))
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, 139, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(panel_3, GroupLayout.DEFAULT_SIZE, 455, Short.MAX_VALUE)
						.addComponent(panel_2, 0, 0, Short.MAX_VALUE)
						.addComponent(panel_4, GroupLayout.PREFERRED_SIZE, 455, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		frame.getContentPane().setLayout(groupLayout);


	}
	
	 public static int lock_match(String s, String t) {



	        int totalw = word_count(s);
	        int total = 100;
	        int perw = total / totalw;
	        int gotperw = 0;

	        if (!s.equals(t)) {

	            for (int i = 1; i <= totalw; i++) {
	                if (simple_match(split_string(s, i), t) == 1) {
	                    gotperw = ((perw * (total - 10)) / total) + gotperw;
	                } else if (front_full_match(split_string(s, i), t) == 1) {
	                    gotperw = ((perw * (total - 20)) / total) + gotperw;
	                } else if (anywhere_match(split_string(s, i), t) == 1) {
	                    gotperw = ((perw * (total - 30)) / total) + gotperw;
	                } else {
	                    gotperw = ((perw * smart_match(split_string(s, i), t)) / total) + gotperw;
	                }
	            }
	        } else {
	            gotperw = 100;
	        }
	        return gotperw;
	    }

	    public static int anywhere_match(String s, String t) {
	        int x = 0;
	        if (t.contains(s)) {
	            x = 1;
	        }
	        return x;
	    }

	    public static int front_full_match(String s, String t) {
	        int x = 0;
	        String tempt;
	        int len = s.length();

	        //----------Work Body----------//
	        for (int i = 1; i <= word_count(t); i++) {
	            tempt = split_string(t, i);
	            if (tempt.length() >= s.length()) {
	                tempt = tempt.substring(0, len);
	                if (s.contains(tempt)) {
	                    x = 1;
	                    break;
	                }
	            }
	        }
	        //---------END---------------//
	        if (len == 0) {
	            x = 0;
	        }
	        return x;
	    }

	    public static int simple_match(String s, String t) {
	        int x = 0;
	        String tempt;
	        int len = s.length();


	        //----------Work Body----------//
	        for (int i = 1; i <= word_count(t); i++) {
	            tempt = split_string(t, i);
	            if (tempt.length() == s.length()) {
	                if (s.contains(tempt)) {
	                    x = 1;
	                    break;
	                }
	            }
	        }
	        //---------END---------------//
	        if (len == 0) {
	            x = 0;
	        }
	        return x;
	    }

	    public static int smart_match(String ts, String tt) {

	        char[] s = new char[ts.length()];
	        s = ts.toCharArray();
	        char[] t = new char[tt.length()];
	        t = tt.toCharArray();


	        int slen = s.length;
	        //number of 3 combinations per word//
	        int combs = (slen - 3) + 1;
	        //percentage per combination of 3 characters//
	        int ppc = 0;
	        if (slen >= 3) {
	            ppc = 100 / combs;
	        }
	        //initialising an integer to store the total % this class genrate//
	        int x = 0;
	        //declaring a temporary new source char array
	        char[] ns = new char[3];
	        //check if source char array has more then 3 characters//
	        if (slen < 3) {
	        } else {
	            for (int i = 0; i < combs; i++) {
	                for (int j = 0; j < 3; j++) {
	                    ns[j] = s[j + i];
	                }
	                if (cross_full_match(ns, t) == 1) {
	                    x = x + 1;
	                }
	            }
	        }
	        x = ppc * x;
	        return x;
	    }

	    /**
	     *
	     * @param s
	     * @param t
	     * @return
	     */
	    public static int  cross_full_match(char[] s, char[] t) {
	        int z = t.length - s.length;
	        int x = 0;
	        if (s.length > t.length) {
	            return x;
	        } else {
	            for (int i = 0; i <= z; i++) {
	                for (int j = 0; j <= (s.length - 1); j++) {
	                    if (s[j] == t[j + i]) {
	                        // x=1 if any charecer matches
	                        x = 1;
	                    } else {
	                        // if x=0 mean an character do not matches and loop break out
	                        x = 0;
	                        break;
	                    }
	                }
	                if (x == 1) {
	                    break;
	                }
	            }
	        }
	        return x;
	    }

	    public static String split_string(String s, int n) {

	        int index;
	        String temp;
	        temp = s;
	        String temp2 = null;

	        int temp3 = 0;

	        for (int i = 0; i < n; i++) {
	            int strlen = temp.length();
	            index = temp.indexOf(" ");
	            if (index < 0) {
	                index = strlen;
	            }
	            temp2 = temp.substring(temp3, index);
	            temp = temp.substring(index, strlen);
	            temp = temp.trim();

	        }
	        return temp2;
	    }

	    public static int word_count(String s) {
	        int x = 1;
	        int c;
	        s = s.trim();
	        if (s.isEmpty()) {
	            x = 0;
	        } else {
	            if (s.contains(" ")) {
	                for (;;) {
	                    x++;
	                    c = s.indexOf(" ");
	                    s = s.substring(c);
	                    s = s.trim();
	                    if (s.contains(" ")) {
	                    } else {
	                        break;
	                    }
	                }
	            }
	        }
	        return x;
	    }

	private void deseleccionarCheckBoxModsPerfil() {
		for (int i = 0; i < modelMods.size(); i++) {
			modelMods.getElementAt(i).setSelected(false);
		}
		listMods.repaint();

	}
}
