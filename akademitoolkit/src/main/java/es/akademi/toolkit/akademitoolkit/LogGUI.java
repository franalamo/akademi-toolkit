package es.akademi.toolkit.akademitoolkit;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JScrollPane;
import java.awt.SystemColor;
import java.awt.BorderLayout;
import java.awt.Color;

public class LogGUI {

	static JFrame frmLog;
	JTextPane consoleLog;
	JScrollPane scrollPane;

	/**
	 * Create the application.
	 */
	public LogGUI() {
		initialize();
	}
	
	public class CustomOutputStreamError extends OutputStream {
		private JTextPane textArea;
		private boolean firstChar;

		public CustomOutputStreamError(JTextPane textArea) {
			this.textArea = textArea;
			this.firstChar = true;
		}

		@Override
		public void write(int b) throws IOException {
			// redirects data to the text area
			if (firstChar) {
				SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss:SSS");
				appendToPane(textArea,format.format(new Date()),Color.red);
				appendToPane(textArea,"\n",Color.red);
				this.firstChar = false;
				Thread t = new Thread (new Runnable() {
					
					@Override
					public void run() {
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						firstChar = true;
					}
				});
				t.start();
			}
			appendToPane(textArea,String.valueOf((char)b),Color.red);
			// scrolls the text area to the end of data
			textArea.setCaretPosition(textArea.getDocument().getLength());
		}
		
		private void appendToPane(JTextPane tp, String msg, Color c)
	    {
	        StyleContext sc = StyleContext.getDefaultStyleContext();
	        AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);

	        aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
	        aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);

	        int len = tp.getDocument().getLength();
	        tp.setCaretPosition(len);
	        tp.setCharacterAttributes(aset, false);
	        tp.replaceSelection(msg);
	    }
	}

	public class CustomOutputStream extends OutputStream {
		private JTextPane textArea;
		private boolean firstChar;

		public CustomOutputStream(JTextPane textArea) {
			this.textArea = textArea;
			this.firstChar = true;
		}

		@Override
		public void write(int b) throws IOException {
			// redirects data to the text area
			if (firstChar) {
				SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
				appendToPane(textArea,"["+format.format(new Date())+"] ",Color.black);
				this.firstChar = false;
				Thread t = new Thread (new Runnable() {
					
					@Override
					public void run() {
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						firstChar = true;
					}
				});
				t.start();
			}
			appendToPane(textArea,String.valueOf((char)b),Color.black);
			// scrolls the text area to the end of data
			textArea.setCaretPosition(textArea.getDocument().getLength());

		}
		
		private void appendToPane(JTextPane tp, String msg, Color c)
	    {
	        StyleContext sc = StyleContext.getDefaultStyleContext();
	        AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);

	        aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
	        aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);

	        int len = tp.getDocument().getLength();
	        tp.setCaretPosition(len);
	        tp.setCharacterAttributes(aset, false);
	        tp.replaceSelection(msg);
	    }
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmLog = new JFrame();
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e3) {
			e3.printStackTrace();
		}

		List<Image> icons = new ArrayList<Image>();
		icons.add(Toolkit.getDefaultToolkit().getImage(Interfaz.class.getResource("/images/icon.gif")));
		icons.add(Toolkit.getDefaultToolkit().getImage(Interfaz.class.getResource("/images/icon16x16.gif")));
		icons.add(Toolkit.getDefaultToolkit().getImage(Interfaz.class.getResource("/images/icon256x256.gif")));
		frmLog.setIconImages(icons);
		frmLog.setTitle("Log");
		frmLog.setBounds(1200, 100, 589, 443);

		scrollPane = new JScrollPane();
		frmLog.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

		consoleLog = new JTextPane();
		consoleLog.setBackground(SystemColor.control);
		EmptyBorder eb = new EmptyBorder(new Insets(10, 10, 10, 10));
		consoleLog.setBorder(eb);
		consoleLog.setMargin(new Insets(5, 5, 5, 5));
		consoleLog.addFocusListener(new FocusListener() {

	        @Override
	        public void focusLost(FocusEvent e) {
	        	consoleLog.setEditable(true);

	        }

	        @Override
	        public void focusGained(FocusEvent e) {
	        	consoleLog.setEditable(false);

	        }
	    });
		//consoleLog.setEditable(false);
		frmLog.getContentPane().setLayout(new BorderLayout(0, 0));
		scrollPane.setViewportView(consoleLog);
		frmLog.getContentPane().add(scrollPane);

		//QUITAR PARA VER ERRORES EN LOG
		
		PrintStream printStream = new PrintStream(new CustomOutputStream(consoleLog));
		System.setOut(printStream);
		PrintStream printStream2 = new PrintStream(new CustomOutputStreamError(consoleLog));
		System.setErr(printStream2);

	}
	
	public static void update() {
		frmLog.update(frmLog.getGraphics());
	}
}
