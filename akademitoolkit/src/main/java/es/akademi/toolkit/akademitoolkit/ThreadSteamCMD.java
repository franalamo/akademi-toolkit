package es.akademi.toolkit.akademitoolkit;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class ThreadSteamCMD implements Runnable { 
	private String[] commands;
	private String tipo;

	public ThreadSteamCMD (String[] commands,String tipo) {
		this.commands = commands;
		this.tipo = tipo;
	}
	@Override
	public void run() { 
		if (Interfaz.txtSteamcmdStatus.getText().equals("STEAMCMD ENCONTRADO")) {
			String fullCommands = "";
			for (int i = 0; i < commands.length; i++) {
				fullCommands = fullCommands + "+" + commands[i]+" ";
			}
			try {
				Interfaz.downloadLabel.setText("Ejecutando SteamCMD...");
				if (Interfaz.isWindows()) {
					Process p = Runtime.getRuntime().exec("cmd /c start /wait \"SteamCMD\" \""+ Interfaz.steamCMD + "\\steamcmd.exe\" "+fullCommands+"+quit");
		            p.waitFor();
					p.destroy();
					Interfaz.mensajeInformacion("STEAMCMD Finalizado", "Proceso de Steamcmd finalizado");
					Interfaz.downloadLabel.setText("");
					Interfaz.downloadPB.setValue(0);
					Interfaz.downloadPB.setStringPainted(false);
				}
				if (Interfaz.isLinux()) {
					try {
						Process s = Runtime.getRuntime().exec("sudo chmod +x "+Interfaz.steamCMD + "/steamcmd.sh");
						s = Runtime.getRuntime().exec("sudo chmod +x "+Interfaz.steamCMD + "/linux32/steamcmd");
						try {
							s.waitFor();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
					Runtime.getRuntime().exec("sudo " + Interfaz.steamCMD + "/steamcmd.sh "+fullCommands+" +quit");	
				}
			}catch (Exception e) {
				System.out.println("Error al ejecutar steamcmd");
			}
		}else {
			Interfaz.mensajeError("ERROR", "STEAMCMD NO ENCONTRADO. Visita el apartado SteamCMD para configurar el SteamCMD");
		} 
		switch (tipo) {
		case "servidor":
			File f = new File(Interfaz.serverDir.getText());
			File[] matchingFiles = f.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return (name.startsWith("arma3server") && name.endsWith(".exe")) || (name.startsWith("arma3server_x64") && name.endsWith(".exe"));
				}
			});
			if (matchingFiles != null && matchingFiles.length > 0) {
				//Existe el archivo
				if (!Interfaz.serverName.getText().equals("")) {
					Interfaz.btnAgregarServidor.setEnabled(true);
				}
				Interfaz.mensajeInformacion("Servidor Encontrado en la ruta: " + Interfaz.serverDir.getText());
				Interfaz.btnDescargarServidor.setEnabled(false);
			}else {
				Interfaz.btnAgregarServidor.setEnabled(false);
				if (!Interfaz.serverName.getText().equals("")) {
					Interfaz.btnDescargarServidor.setEnabled(true);
				}else {
					Interfaz.btnDescargarServidor.setEnabled(false);
				}
				Interfaz.mensajeInformacion("Servidor no Encontrado en la ruta: " + Interfaz.serverDir.getText());
			}

			break;
		case "mod":
			//Mover mod
			Interfaz.downloadLabel.setText("Moviendo Mods");
			Process p2 = null;
			try {
				if (Interfaz.isWindows()) {
					p2 = Runtime.getRuntime().exec("cmd /c start /wait xcopy \""+Interfaz.steamCMD+"\\steamapps\\workshop\\content\\107410\\"+Interfaz.inputIdMod.getText()+"\" \""+Interfaz.inputDirMod.getText()+"\" /s/h/e/k/f/c/i/y");	
				}
				if (Interfaz.isLinux()) {
					p2 = Runtime.getRuntime().exec("sudo cp \""+Interfaz.steamCMD+"\\steamapps\\workshop\\content\\107410\\"+Interfaz.inputIdMod.getText()+"\" \""+Interfaz.inputDirMod.getText()+"\"");	
				}
			} catch (IOException e1) {
				Interfaz.mensajeError("ERROR", "STEAMCMD NO ENCONTRADO. Visita el apartado SteamCMD para configurar el SteamCMD");
			}
			try {
				p2.waitFor();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			p2.destroy();
			File fmod = new File(Interfaz.inputDirMod.getText());
			File[] matchingMod = fmod.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.startsWith("meta") && name.endsWith(".cpp");
				}
			});
			ArrayList<String> bikeys = new ArrayList<String>();
			Interfaz.searchBikeyLastModify(bikeys,fmod);
			Interfaz.dirKeyMod.setText(Arrays.toString(bikeys.toArray()).substring(1, Arrays.toString(bikeys.toArray()).length()-1));
			if (matchingMod != null && matchingMod.length > 0) {
				//Existe el archivo
				if (!Interfaz.inputNombreMod.getText().equals("")) {
					Interfaz.btnAgregarMod.setEnabled(true);
				}
				Interfaz.btnDescargarMod.setEnabled(false);
			}else {
				Interfaz.btnAgregarMod.setEnabled(false);
				if (!Interfaz.inputNombreMod.getText().equals("")) {
					Interfaz.btnDescargarMod.setEnabled(true);
				}else {
					Interfaz.btnDescargarMod.setEnabled(false);
				}
			}
			p2.destroy();

			break;

		default:
			System.out.println("Error en la seleccion de tipo de descarga");
			break;
		}
		Interfaz.downloadLabel.setText("");


	}
} 