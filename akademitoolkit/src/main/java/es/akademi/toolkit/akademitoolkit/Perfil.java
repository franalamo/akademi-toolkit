package es.akademi.toolkit.akademitoolkit;
import java.util.ArrayList;

public class Perfil {
	private String nombrePerfil;
	private Servidor sv;
	private ArrayList<Mod> listaMods;
	private Mision mision;
	
	public Perfil(String nombrePerfil, Servidor sv, ArrayList<Mod> listaMods, Mision mision) {
		super();
		this.nombrePerfil = nombrePerfil;
		this.sv = sv;
		this.listaMods = listaMods;
		this.mision = mision;
	}

	public String getNombrePerfil() {
		return nombrePerfil;
	}

	public Servidor getSv() {
		return sv;
	}

	public ArrayList<Mod> getListaMods() {
		return listaMods;
	}

	public Mision getMision() {
		return mision;
	}

	@Override
	public String toString() {
		return nombrePerfil;
	}
	
	
	
	
	
	
	

}
