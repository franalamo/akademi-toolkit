package es.akademi.toolkit.akademitoolkit;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.TitledBorder;

import javax.swing.JFileChooser;

import javax.swing.ImageIcon;
import javax.swing.UIManager;
import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JList;
import javax.swing.JScrollPane;

public class ModificarModGUI {

	public JFrame frame;

	private int pos;
	private Mod mod;
	private static JTextField inputNombreMod;
	private static JTextField inputIdMod;
	private JTextField inputDirMod;
	private JTextField inputDirKeyMod;
	private static ArrayList<File> bikeys = new ArrayList<File>();

	/**
	 * Create the application.
	 */
	public ModificarModGUI(Mod mod,int pos) {
		this.pos = pos;
		this.mod = mod;
		initialize();
	}

	private boolean buscarMod (String file) {
		File f = new File(file);
		File[] matchingFiles = f.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.startsWith("meta") && name.endsWith(".cpp");
			}
		});
		if (matchingFiles != null && matchingFiles.length > 0) {
			return true;
		}else {
			return false;
		}
	}

	public static void añadirIdMod(int id,String nombreMod) {
		inputIdMod.setText(""+id);
		inputNombreMod.setText(nombreMod);
	}

	private static ArrayList<File> searchBikeyLastModify(File file) {
		if (file.isDirectory()) {
			File[] arr = file.listFiles();
			for (File f : arr) {
				searchBikeyLastModify(f);
			}
		} else {
			if (file.getName().endsWith(".bikey")) {
				bikeys.add(file);
			}
		}
		return bikeys;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame(mod.getNombreMod());
		frame.setBounds(100, 100, 850, 699);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Modificar Mod", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		frame.getContentPane().add(panel, BorderLayout.CENTER);

		final JButton btnModificarMod = new JButton("Modificar Mod");
		btnModificarMod.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ArrayList<String> bikeys = null;
				try {
					bikeys = new ArrayList<String>(Arrays.asList(inputDirKeyMod.getText().substring(0, inputDirKeyMod.getText().length()).split(",")));
				}catch (Exception e2) {
					bikeys = new ArrayList<String>();
				}
				mod.setDirKey(bikeys);
				mod.setNombreMod(inputNombreMod.getText());
				mod.setDirMod(inputDirMod.getText());
				mod.setId(Integer.parseInt(inputIdMod.getText()));
				frame.setTitle(mod.getNombreMod());
			}
		});

		btnModificarMod.setEnabled(false);

		JButton button_2 = new JButton("");

		button_2.setIcon(new ImageIcon(ModificarServidorGUI.class.getResource("/images/folder-icon.gif")));

		JButton btnEliminarMod = new JButton("Eliminar Mod");
		btnEliminarMod.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Interfaz.eliminarMod(mod,pos);
				frame.dispose();
			}
		});

		JLabel label = new JLabel("Nombre:");

		inputNombreMod = new JTextField(mod.getNombreMod());
		inputNombreMod.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				if (buscarMod(inputDirMod.getText()) && !inputNombreMod.getText().equals("") && !inputDirMod.getText().equals("") && !inputIdMod.getText().equals("")) {
					btnModificarMod.setEnabled(true);
				} else {
					btnModificarMod.setEnabled(false);
				}
			}
		});
		inputNombreMod.setColumns(10);

		JLabel label_1 = new JLabel("Id:");

		inputIdMod = new JTextField(String.valueOf(mod.getId()));
		inputIdMod.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (buscarMod(inputDirMod.getText()) && !inputNombreMod.getText().equals("") && !inputDirMod.getText().equals("") && !inputIdMod.getText().equals("")) {
					btnModificarMod.setEnabled(true);
				} else {
					btnModificarMod.setEnabled(false);
				}
			}
		});
		inputIdMod.setColumns(10);

		JLabel label_2 = new JLabel("Direcci\u00F3n:");

		inputDirMod = new JTextField(mod.getDirMod());
		inputDirMod.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (buscarMod(inputDirMod.getText()) && !inputNombreMod.getText().equals("") && !inputDirMod.getText().equals("") && !inputIdMod.getText().equals("")) {
					btnModificarMod.setEnabled(true);
				} else {
					btnModificarMod.setEnabled(false);
				}
			}
		});
		inputDirMod.setColumns(10);

		JLabel label_3 = new JLabel("Dir. Key:");
		try {
			inputDirKeyMod = new JTextField(Arrays.toString(mod.getDirKeys().toArray()).substring(1, Arrays.toString(mod.getDirKeys().toArray()).length()-1));
		}catch (Exception e) {
			inputDirKeyMod = new JTextField();
		}
		
		inputDirKeyMod.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (buscarMod(inputDirMod.getText()) && !inputNombreMod.getText().equals("") && !inputDirMod.getText().equals("") && !inputIdMod.getText().equals("")) {
					btnModificarMod.setEnabled(true);
				} else {
					btnModificarMod.setEnabled(false);
				}
			}
		});
		inputDirKeyMod.setColumns(10);

		JButton button = new JButton("");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new java.io.File("."));
				chooser.setDialogTitle("Directorio Mod");
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				chooser.setAcceptAllFileFilterUsed(false);

				if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					inputDirMod.setText(chooser.getSelectedFile().toString());
					//STATUS STEAM
					// your directory
					File f = new File(chooser.getSelectedFile().toString());
					File[] matchingFiles = f.listFiles(new FilenameFilter() {
						public boolean accept(File dir, String name) {
							return name.startsWith("meta") && name.endsWith(".cpp");
						}
					});
					if (matchingFiles != null && matchingFiles.length > 0) {
						//Existe el archivo
						if (!inputNombreMod.getText().equals("") && !inputNombreMod.getText().equals("") && !inputDirMod.getText().equals("") && !inputIdMod.getText().equals("")) {
							btnModificarMod.setEnabled(true);
						}else {
							btnModificarMod.setEnabled(false);
						}
						System.out.println("Mod Encontrado en la ruta: " + chooser.getSelectedFile().toString());
						ArrayList<File> bikeys = searchBikeyLastModify(new File(chooser.getSelectedFile().toString()));
						File bikeyARetornar = null;
						long num = Long.MIN_VALUE;
						for (int i = 0; i < bikeys.size(); i++) {
							if (bikeys.get(i).lastModified() > num) {
								bikeyARetornar = bikeys.get(i);
								num = bikeys.get(i).lastModified();
							}
						}
						if (bikeyARetornar == null) {
							inputDirKeyMod.setText("NO BIKEY");
						}else {
							inputDirKeyMod.setText(bikeyARetornar.getAbsolutePath());
						}

					}else {
						System.out.println("Mod no Encontrado en la ruta: " + chooser.getSelectedFile().toString());
						btnModificarMod.setEnabled(false);
					}
				}
			}
		});
		button.setIcon(new ImageIcon(ModificarModGUI.class.getResource("/images/folder-icon.gif")));

		JButton button_1 = new JButton("");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new SearchMod(inputNombreMod.getText(),false);
			}
		});
		button_1.setIcon(new ImageIcon(ModificarModGUI.class.getResource("/images/search-icon.gif")));
		DefaultListModel<String> model = new DefaultListModel<>();
		for (Modulo modulo : mod.getModulos()) {
			model.add(model.getSize(), modulo.getNombre());
		}

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Modulos", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel.createSequentialGroup()
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(button_2, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_panel.createSequentialGroup()
										.addGap(51)
										.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
												.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
														.addGroup(gl_panel.createSequentialGroup()
																.addComponent(label_3, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
																.addGap(23))
														.addGroup(gl_panel.createSequentialGroup()
																.addComponent(label_2)
																.addGap(18)))
												.addComponent(label_1, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
												.addComponent(label, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE))
										.addGap(36)
										.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
												.addComponent(inputDirKeyMod, GroupLayout.DEFAULT_SIZE, 473, Short.MAX_VALUE)
												.addGroup(gl_panel.createSequentialGroup()
														.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING, false)
																.addComponent(inputNombreMod, Alignment.LEADING)
																.addComponent(inputDirMod, Alignment.LEADING)
																.addComponent(inputIdMod, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 418, Short.MAX_VALUE))
														.addPreferredGap(ComponentPlacement.RELATED)
														.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
																.addComponent(button, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
																.addComponent(button_1, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE))))
										.addPreferredGap(ComponentPlacement.RELATED, 197, Short.MAX_VALUE))
								.addGroup(gl_panel.createSequentialGroup()
										.addGap(35)
										.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
												.addComponent(btnModificarMod, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 787, Short.MAX_VALUE)
												.addComponent(panel_1, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addComponent(btnEliminarMod, GroupLayout.DEFAULT_SIZE, 787, Short.MAX_VALUE))))
						.addContainerGap())
				);
		gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
						.addGap(11)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(button_2, GroupLayout.PREFERRED_SIZE, 0, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_panel.createSequentialGroup()
										.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
												.addComponent(inputNombreMod, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(label))
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
												.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
														.addComponent(inputIdMod, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
														.addComponent(label_1))
												.addComponent(button_1, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
												.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
														.addComponent(inputDirMod, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
														.addComponent(label_2))
												.addComponent(button, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
										.addGap(11)
										.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
												.addComponent(inputDirKeyMod, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(label_3))))
						.addGap(31)
						.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 331, Short.MAX_VALUE)
						.addGap(18)
						.addComponent(btnModificarMod, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(btnEliminarMod, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)
						.addContainerGap())
				);

		JScrollPane scrollPane = new JScrollPane();
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
						.addGap(2)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 773, Short.MAX_VALUE))
				);
		gl_panel_1.setVerticalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addComponent(scrollPane, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 308, Short.MAX_VALUE)
				);
		JList<String> list = new JList<String>(model);
		scrollPane.setViewportView(list);
		panel_1.setLayout(gl_panel_1);
		panel.setLayout(gl_panel);
	}
}
