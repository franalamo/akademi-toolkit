package es.akademi.toolkit.akademitoolkit;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import javafx.application.Platform;
import javafx.concurrent.Worker;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

public class SearchMod {

	JFrame frame;
	private String busqueda;
	private boolean tipo;
	private boolean finalizado = false;



	/**
	 * Create the application.
	 */
	public SearchMod(String busqueda,boolean tipo) {
		this.busqueda = busqueda;
		this.tipo = tipo;
		initialize();

		
	}
	

	public boolean isFinalizado() {
		return finalizado;
	}


	/**
	 * Initialize the contents of the frame.
	 * @throws SteamApiException 
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1400, 1200);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		
		JFXPanel jfxPanel = new JFXPanel();
		frame.getContentPane().add(jfxPanel);
		frame.setVisible(true);

		Platform.runLater(() -> {
		    WebView webView = new WebView();
		    WebEngine engine = webView.getEngine();
		    jfxPanel.setScene(new Scene(webView));
		    webView.getEngine().load("https://steamcommunity.com/workshop/browse/?appid=107410&searchtext="+busqueda+"&childpublishedfileid=0&browsesort=trend&section=items&requiredtags%5B%5D=Mod");
		    engine.getLoadWorker().stateProperty().addListener((observable, oldValue, newValue) -> {
	            if (Worker.State.SUCCEEDED.equals(newValue)) {
	            	String url = engine.getLocation();
	            	if (url.contains(("https://steamcommunity.com/sharedfiles/filedetails/?"))) {
	            		Thread t = new Thread(new Runnable() {
							@Override
							public void run() {
								int response = JOptionPane.showConfirmDialog(null, "Confirmar Mod", "Confirmar", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			                    if (response == JOptionPane.YES_OPTION) {
			                    	if (tipo) {
			                    		Interfaz.añadirIdMod(Integer.parseInt(url.substring(url.indexOf("?")+4,url.indexOf("&"))),obtenerNombreModPorId(Integer.parseInt(url.substring(url.indexOf("?")+4,url.indexOf("&")))));
			                    		
									}else {
										ModificarModGUI.añadirIdMod(Integer.parseInt(url.substring(url.indexOf("?")+4,url.indexOf("&"))),obtenerNombreModPorId(Integer.parseInt(url.substring(url.indexOf("?")+4,url.indexOf("&")))));
									}
			                    	Platform.exit();
			                        frame.setVisible(false);
			                        Interfaz.tabbedPane.setSelectedIndex(1);
			                        finalizado = true;
			                    } else {
			                    	Platform.exit();
			                        frame.setVisible(false);
			                        Interfaz.tabbedPane.setSelectedIndex(1);
			                        finalizado = true;
			                    }
								
							}
						});
	            		t.start();
	            		
	            	}
	            }
	        });
		});
	}
	

	public static String obtenerNombreModPorId (int idMod) {
		HttpClient httpclient = HttpClients.createDefault();
		HttpPost httppost = new HttpPost("https://api.steampowered.com/ISteamRemoteStorage/GetPublishedFileDetails/v1/");
		// Request parameters and other properties.
		List<NameValuePair> params = new ArrayList<NameValuePair>(2);
		params.add(new BasicNameValuePair("key", "7D03BDF3CA93ACC07F53E386B9FBF8CE"));
		params.add(new BasicNameValuePair("itemcount", "1"));
		params.add(new BasicNameValuePair("publishedfileids[0]", ""+idMod));

		try {
			httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		//Execute and get the response.
		HttpResponse response = null;
		try {
			response = httpclient.execute(httppost);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		HttpEntity entity = response.getEntity();
		String nombreMod = "";
		if (entity != null) {
			InputStream instream = null;
			try {
				instream = entity.getContent();
			} catch (UnsupportedOperationException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(instream));
				String temp;
				try {
					while ((temp = in.readLine()) != null){
						nombreMod = temp.substring(temp.indexOf("title")+8, temp.indexOf(",",temp.indexOf("title"))-1);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			} finally {
				try {
					instream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return nombreMod;

	}

}
